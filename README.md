## Libraries used
* [glad](https://github.com/Dav1dde/glad) - For OpenGL
* [GLFW](https://github.com/glfw/glfw) - For Window and Input
* [libpng](http://www.libpng.org/pub/png/libpng.html) - For Textures
* [openal](https://www.openal.org/) - Audio support
* [vorbis](https://xiph.org/vorbis/) - Ogg file support
* [ffmpeg](https://ffmpeg.org/) - multiple other codecs
