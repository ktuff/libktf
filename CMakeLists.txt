cmake_minimum_required(VERSION 3.1)

project(ktf VERSION 0.0.2 LANGUAGES CXX C)

include_directories(./src/)
add_subdirectory(./src/ktf)
