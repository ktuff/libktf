#pragma once

#include <ktf/container/st_index_map.hpp>
#include <ktf/texture/font.hpp>
#include <ktf/texture/png.hpp>

#include "camera.hpp"
#include "framebuffer.hpp"
#include "mesh.hpp"

#define KTF_TEXTURE_BIT 0b00000001
#define KTF_COLOR_BIT   0b00000010
#define KTF_NORMAL_BIT  0b00000100

#define KTF_VERTEX_SHADER 0
#define KTF_FRAGMENT_SHADER 1

namespace ktf {

class buffer;
class buffer_texture;
class shader;
class texture;


struct buffer_data {
	size_t size;
	void* data;
	size_t draw_hint;
};


struct buffer_attrib {
	size_t data_index;
	size_t attrib_index;
	size_t num_elements;
	size_t data_type;
	bool normalize;
	size_t size_element;
	size_t offset;
	size_t divisor;
};


enum fb_attachment_t {
	e_fb_none,
	e_fb_color,
	e_fb_depth,
	e_fb_depth_stencil
};


class composer {

	public:
		static bool const bool_true;
		static bool const bool_false;
		static size_t const data_type_byte;
		static size_t const data_type_ubyte;
		static size_t const data_type_short;
		static size_t const data_type_ushort;
		static size_t const data_type_int;
		static size_t const data_type_uint;
		static size_t const data_type_float;
		static size_t const data_type_double;
		static size_t const draw_hint_static;
		static size_t const draw_hint_dynamic;
		static size_t const shader_type_vertex;
		static size_t const shader_type_fragment;
		static size_t const shader_type_geometry;
		static size_t const shader_type_compute;


	private:
		// storage
		ktf::st_index_map<std::shared_ptr<ktf::shader>> shaders;
		ktf::st_index_map<std::shared_ptr<ktf::texture>> textures;
		ktf::st_index_map<std::shared_ptr<ktf::buffer>> meshes;
		ktf::st_index_map<std::shared_ptr<ktf::buffer_texture>> buffer_textures;
		ktf::st_index_map<std::unique_ptr<ktf::framebuffer>> m_framebuffers;
		ktf::font font;
		ktf::camera camera;
		// active shader
		std::shared_ptr<ktf::shader> used_shader;


	public:
		composer();
		~composer();


	public:
		void free();

		void register_shader(size_t* id, const std::string& vertex_path, const std::string& fragment_path);
		void deregister_shader(size_t* id);
		void bind_shader(size_t id);

		void register_texture(size_t* id, const std::string& path);
		void register_texture(size_t* id, int width, int height, int channels, uint8_t* data, size_t data_type, fb_attachment_t attachment);
		void deregister_texture(size_t* id);
		void bind_texture(size_t id, int target = 0) const;

		void register_cubemap(size_t* id, std::array<ktf::png, 6> const& textures);

		void register_buffer_texture(size_t* id, size_t size);
		void fill_buffer_texture(size_t id, char const* data) const;
		void bind_buffer_texture(size_t id) const;
		void deregister_buffer_texture(size_t* id);

		void register_framebuffer(size_t* id);
		void bind_framebuffer(size_t id);
		void copy_framebuffer(size_t src, size_t dest, fb_attachment_t type, int src_w, int src_h, int dest_w, int dest_h);
		void bind_framebuffer_texture(size_t fb_id, size_t tex_id, fb_attachment_t type);
		void unbind_framebuffer_texture(size_t fb_id, fb_attachment_t type);
		void read_framebuffer(size_t id, std::vector<uint8_t>& data, int& width, int& height, int& channels);
		void deregister_framebuffer(size_t* id);

		void register_mesh(size_t* id, const std::vector<buffer_data>& data, const std::vector<buffer_attrib>& attribs, ktf::vector<unsigned int> const& indices);
		void deregister_mesh(size_t* id);
		void update_mesh_buffer(size_t id, buffer_data const& data, size_t index);
		void render_mesh(size_t id, const ktf::matrix<float>& model) const;
		void render_mesh(size_t id, ktf::matrix<float> const * const models, unsigned int count) const;

		ktf::font const* get_font() const;
		void load_font(const std::string& path, const ktf::vec2<uint32_t>& size, const std::string& temp_folder, const std::vector<uint32_t>& chars);
		void unload_font();

		void begin_frame();
		void end_frame();

		void use_orthogonal_projection();
		void use_perspective_projection();

		ktf::camera* get_camera();
		void generate_texture(const std::string& path, int width, int height, int channels, size_t shader_id, size_t texture_id, size_t mesh_id, const ktf::matrix<float>& model);

		void set_uniform_1b(const char* id, bool value);
		void set_uniform_1i(const char* id, int value);
		void set_uniform_1f(const char* id, float value);
		void set_uniform_1d(const char* id, double value);

		void set_uniform_2i(const char* id, const ktf::vec2<int>& value);
		void set_uniform_2f(const char* id, const ktf::vec2<float>& value);
		void set_uniform_2d(const char* id, const ktf::vec2<double>& value);

		void set_uniform_3i(const char* id, const ktf::vec3<int>& value);
		void set_uniform_3f(const char* id, const ktf::vec3<float>& value);
		void set_uniform_3d(const char* id, const ktf::vec3<double>& value);

		void set_uniform_4i(const char* id, const ktf::vec4<int>& value);
		void set_uniform_4f(const char* id, const ktf::vec4<float>& value);
		void set_uniform_4d(const char* id, const ktf::vec4<double>& value);

		void set_uniform_4x4f(const char* id, const ktf::matrix<float>& value);
		void set_uniform_4x4d(const char* id, const ktf::matrix<double>& value);

	private:
		void check_error();
		static unsigned int get_color_enum(int channels);
};

} // namespace ktf
