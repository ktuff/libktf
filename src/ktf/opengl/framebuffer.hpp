#pragma once

#include <vector>


namespace ktf {

class framebuffer {

	private:
		unsigned int id;


	public:
		framebuffer();
		framebuffer(unsigned int id);
		framebuffer(framebuffer&& buffer);
		framebuffer(const framebuffer& buffer) = delete;
		~framebuffer();
		framebuffer& operator=(framebuffer&& buffer);
		framebuffer& operator=(const framebuffer& buffer) = delete;


	public:
		void bind() const;
		void bind_read() const;
		void bind_draw() const;
		void unbind() const;
		void read(std::vector<uint8_t>& data, int& width, int& height, int& channels) const;

	private:
		inline void free() const;

};

} // namespace ktf
