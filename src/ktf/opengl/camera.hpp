#pragma once

#include "../math/frustum.hpp"

namespace ktf {

class camera {

	private:
		ktf::vec3<double> pos;
		ktf::vec3<double> rot;
		ktf::vec3<double> dir;
		ktf::matrix<double> ortho;
		ktf::matrix<double> persp;
		ktf::frustum frustum;
		double fov;
		double width, height, oznear, ozfar, pznear, pzfar;
		bool relative;


	public:
		camera();
		~camera();


	public:
		void set_position(const ktf::vec3<double>& pos);
		void set_rotation(const ktf::vec3<double>& rot);
		void set_fov(double fov);
		void set_relative(bool relative);
		ktf::vec3<double> get_position() const;
		ktf::vec3<double> get_rotation() const;
		ktf::vec3<double> get_direction() const;
		double get_fov() const;

		void set_viewport(double width, double height, double oznear, double ozfar, double pznear, double pzfar);
		void get_viewport(double* width, double* height, double* oznear, double* ozfar, double* pznear, double* pzfar) const;
		ktf::matrix<double> look_at() const;
		ktf::matrix<double> orthogonal() const;
		ktf::matrix<double> perspective() const;

		bool point_in_view(const ktf::vec3<double>& p) const;
		bool sphere_in_view(const ktf::vec3<double>& center, float radius) const;
		bool cube_in_view(const ktf::vec3<double>& p0, int size) const;
		void update_matrices();

	private:
		void calc_ortho();
		void calc_persp();
};

} // namespace ktf
