#pragma once

#include <stddef.h>

namespace ktf {

class buffer_texture {

	private:
		unsigned int m_id;
		unsigned int m_buffer_id;
		size_t m_size;


	public:
		buffer_texture();
		buffer_texture(size_t size);
		buffer_texture(buffer_texture&& b);
		buffer_texture(buffer_texture const& b) = delete;
		~buffer_texture();

		buffer_texture& operator=(buffer_texture&& b);
		buffer_texture& operator=(buffer_texture const& b) = delete;


	public:
		void fill(char const* data) const;
		void bind() const;
};

} // namespace ktf
