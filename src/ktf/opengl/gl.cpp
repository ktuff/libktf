#include <ktf/glad/glad.h>

#include "gl.hpp"


namespace ktf {

GLenum get_blend_factor(gl::blend_factor_t v) {
	switch (v) {
	case gl::e_zero:
		return GL_ZERO;
	case gl::e_one:
		return GL_ONE;
	case gl::e_src_color:
		return GL_SRC_COLOR;
	case gl::e_one_minus_src_color:
		return GL_ONE_MINUS_SRC_COLOR;
	case gl::e_src_alpha:
		return GL_SRC_ALPHA;
	case gl::e_one_minus_src_alpha:
		return GL_ONE_MINUS_SRC_ALPHA;
	case gl::e_dst_color:
		return GL_DST_COLOR;
	case gl::e_one_minus_dst_color:
		return GL_ONE_MINUS_DST_COLOR;
	case gl::e_dst_alpha:
		return GL_DST_ALPHA;
	case gl::e_one_minus_dst_alpha:
		return GL_ONE_MINUS_DST_ALPHA;
	case gl::e_constant_color:
		return GL_CONSTANT_COLOR;
	case gl::e_one_minus_constant_color:
		return GL_ONE_MINUS_CONSTANT_COLOR;
	case gl::e_constant_alpha:
		return GL_CONSTANT_ALPHA;
	case gl::e_one_minus_constant_alpha:
		return GL_ONE_MINUS_CONSTANT_ALPHA;
	default:
		return GL_ZERO;
	}
}

} // namespace ktf


void ktf::gl::set_viewport_size(int width, int height)
{
	::glViewport(0, 0, width, height);
}

void ktf::gl::get_viewport_size(int* width, int* height)
{
	GLint dims[4];
	::glGetIntegerv(GL_VIEWPORT, dims);
	*width = dims[2];
	*height = dims[3];
}

void ktf::gl::clear()
{
	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void ktf::gl::clear_color()
{
	::glClear(GL_COLOR_BUFFER_BIT);
}

void ktf::gl::clear_depth()
{
	::glClear(GL_DEPTH_BUFFER_BIT);
}

void ktf::gl::set_clear_color(float r, float g, float b, float a)
{
    ::glClearColor(r, g, b, a);
}

void ktf::gl::get_clear_color(float* r, float* g, float* b, float* a)
{
    float color[4];
    ::glGetFloatv(GL_COLOR_CLEAR_VALUE, color);
    *r = color[0];
    *g = color[1];
    *b = color[2];
    *a = color[3];
}

void ktf::gl::enable_culling()
{
	::glEnable(GL_CULL_FACE);
}

void ktf::gl::disable_culling()
{
	::glDisable(GL_CULL_FACE);
}

void ktf::gl::enable_blending()
{
	::glEnable(GL_BLEND);
}

void ktf::gl::disable_blending()
{
    ::glDisable(GL_BLEND);
}

void ktf::gl::set_blending_factors(ktf::gl::blend_factor_t sf, ktf::gl::blend_factor_t df)
{
	::glBlendFunc(ktf::get_blend_factor(sf), ktf::get_blend_factor(df));
}

void ktf::gl::set_blend_color(float r, float g, float b, float a)
{
	::glBlendColor(r, g, b, a);
}

void ktf::gl::set_blend_equation(ktf::gl::blend_eq_t f)
{
	switch (f) {
	case e_eq_add:
		::glBlendEquation(GL_FUNC_ADD);
		break;
	case e_eq_sub:
		::glBlendEquation(GL_FUNC_SUBTRACT);
		break;
	case e_eq_rsub:
		::glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
		break;
	case e_eq_min:
		::glBlendEquation(GL_MIN);
		break;
	case e_eq_max:
		::glBlendEquation(GL_MAX);
		break;
	}
}

void ktf::gl::enable_depth_testing()
{
    ::glEnable(GL_DEPTH_TEST);
	::glDepthFunc(GL_LEQUAL);
}

void ktf::gl::disable_depth_testing()
{
    ::glDisable(GL_DEPTH_TEST);
}

void ktf::gl::enable_depth_mask()
{
    ::glDepthMask(GL_TRUE);
}

void ktf::gl::disable_depth_mask()
{
    ::glDepthMask(GL_FALSE);
}

void ktf::gl::use_polygon_fill()
{
    ::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void ktf::gl::use_polygon_lines()
{
    ::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}
