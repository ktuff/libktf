#pragma once

#include <stdint.h>
#include <string>
#include <vector>

namespace ktf {

class window;

enum event_type : uint8_t {
	event_size = 0,
	event_focus = 1,
	event_drop = 2,
	event_button = 3,
	event_position = 4,
	event_scroll = 5,
	event_key = 6,
	event_char = 7
};

struct event {
	ktf::window* pwindow;
	event_type type;
	bool handled;
	int action;
	int mods;

	event(ktf::window* pwindow, event_type type, int action, int mods);
	~event() = default;
};


struct window_event : public event {
	int width, height;
	bool focused;

	window_event(ktf::window* pwindow, int width, int height);
	window_event(ktf::window* pwindow, bool focused);
};


struct drop_event : public event {
	std::vector<std::string> paths;

	drop_event(ktf::window* pwindow, std::vector<std::string> const& paths);
};


struct mouse_event : public event {
	int x, y;
	int button;
	int offset_x, offset_y;

	mouse_event(ktf::window* pwindow, int x, int y);
	mouse_event(ktf::window* pwindow, int x, int y, int button, int action, int mods);
	mouse_event(ktf::window* pwindow, int x, int y, int diffx, int diffy);
};


struct key_event : public event {
	uint32_t codepoint;
	int key;
	int scancode;

	key_event(ktf::window* pwindow, int codepoint);
	key_event(ktf::window* pwindow, int key, int scancode, int action, int mods);
};

} // namespace ktf

