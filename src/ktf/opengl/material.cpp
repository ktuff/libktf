#include "material.hpp"

#include "composer.hpp"

ktf::material::material()
: material("")
{
}

ktf::material::material(std::string const& name)
: m_name(name)
, m_specular_strength(1.0)
, m_specular_exponent(0.0)
, m_opacity(1.0)
, m_texture_id((size_t)-1)
{
}

ktf::material& ktf::material::operator=(ktf::material&& m)
{
	this->m_name = m.m_name;
	this->m_diffuse_color = m.m_diffuse_color;
	this->m_specular_color = m.m_specular_color;
	this->m_specular_strength = m.m_specular_strength;
	this->m_specular_exponent = m.m_specular_exponent;
	this->m_opacity = m.m_opacity;
	this->m_texture_id = m.m_texture_id;
	return *this;
}

ktf::material& ktf::material::operator=(ktf::material const& m)
{
	this->m_name = m.m_name;
	this->m_diffuse_color = m.m_diffuse_color;
	this->m_specular_color = m.m_specular_color;
	this->m_specular_strength = m.m_specular_strength;
	this->m_specular_exponent = m.m_specular_exponent;
	this->m_opacity = m.m_opacity;
	this->m_texture_id = m.m_texture_id;
	return *this;
}

void ktf::material::apply(ktf::composer* comp) const
{
	comp->set_uniform_3f("diffuse_color", m_diffuse_color);
	comp->set_uniform_3f("specular_color", m_specular_color);
	comp->set_uniform_1f("specular_strength", m_specular_strength);
	comp->set_uniform_1f("specular_exponent", m_specular_exponent);
	comp->set_uniform_1f("opacity", m_opacity);
	comp->bind_texture(m_texture_id);
}
