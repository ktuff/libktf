#include <cmath>

#include <GLFW/glfw3.h>

#include <ktf/glad/glad.h>
#include <ktf/texture/png.hpp>

#include "window.hpp"

void APIENTRY gl_debug_output(
	GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char *message,
	const void *user_param
);


void ktf::window::error_callback(int error, const char* description)
{
	::fprintf(stderr, "%d %s", error, description);
}

void ktf::window::framebuffer_size_callback(GLFWwindow* glfw_window, int width, int height)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_size_change(width, height);
}

void ktf::window::window_focus_callback(GLFWwindow* glfw_window, int focused)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_focus(focused);
}

void ktf::window::drop_callback(GLFWwindow* glfw_window, int num, const char** paths)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_drop_event(num, paths);
}

void ktf::window::key_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_key_event(key, scancode, action, mods);
}

void ktf::window::character_callback(GLFWwindow* glfw_window, unsigned int codepoint)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_char_event(codepoint);
}

void ktf::window::mouse_button_callback(GLFWwindow* glfw_window, int button, int action, int mods)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_mouse_button_event(button, action, mods);
}

void ktf::window::mouse_position_callback(GLFWwindow* glfw_window, double x_pos, double y_pos)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_mouse_position_event(x_pos, y_pos);
}

void ktf::window::mouse_scroll_callback(GLFWwindow* glfw_window, double x_offset, double y_offset)
{
	ktf::window* window = reinterpret_cast<ktf::window*>(::glfwGetWindowUserPointer(glfw_window));
	window->on_mouse_scroll_event(x_offset, y_offset);
}


ktf::window::window()
: glfw_window(nullptr)
{
}

ktf::window::~window()
{
	this->destroy();
}


bool ktf::window::create(int width, int height, bool fullscreen, int version_major, int version_minor, const char* title)
{
	if (glfw_window) {
		return false;
	}
	if (!init_glfw(width, height, fullscreen, version_major, version_minor)) {
		return false;
	}
	if (!init_opengl()) {
		this->terminate_glfw();
		return false;
	}

	::glfwSetWindowUserPointer(glfw_window, this);

	// register callbacks
	::glfwSetWindowSizeCallback(glfw_window, framebuffer_size_callback);
	::glfwSetWindowFocusCallback(glfw_window, window_focus_callback);
	::glfwSetDropCallback(glfw_window, drop_callback);
	::glfwSetKeyCallback(glfw_window, key_callback);
	::glfwSetCharCallback(glfw_window, character_callback);
	::glfwSetMouseButtonCallback(glfw_window, mouse_button_callback);
	::glfwSetCursorPosCallback(glfw_window, mouse_position_callback);
	::glfwSetScrollCallback(glfw_window, mouse_scroll_callback);

	this->framebuffer_size_callback(glfw_window, width, height);

	this->set_title(title);

	return true;
}

void ktf::window::destroy()
{
	if (!glfw_window) {
		return;
	}
	this->composer.free();
	this->terminate_glfw();
}

void ktf::window::sync_display(bool value) const
{
	::glfwSwapInterval(value);
}

void ktf::window::set_framerate(int rate)
{
	GLFWmonitor *monitor = ::glfwGetPrimaryMonitor();
	const GLFWvidmode *mode = ::glfwGetVideoMode(monitor);
	this->framerate = std::min(mode->refreshRate, rate);
}

void ktf::window::toggle_fullscreen()
{
	this->fullscreen = !fullscreen;
	if (fullscreen) {
		::glfwGetWindowSize(glfw_window, &size_x, &size_y);
		::glfwGetWindowPos(glfw_window, &position_x, &position_y);
		GLFWmonitor *monitor = ::glfwGetPrimaryMonitor();
		const GLFWvidmode *mode = ::glfwGetVideoMode(monitor);
		::glfwSetWindowMonitor(glfw_window, monitor, 0, 0, mode->width, mode->height, framerate);
	} else {
		::glfwSetWindowMonitor(glfw_window, nullptr, position_x, position_y, size_x, size_y, framerate);
	}
}

void ktf::window::poll() const
{
	::glfwPollEvents();
}

bool ktf::window::should_close() const
{
	return ::glfwWindowShouldClose(glfw_window);
}

void ktf::window::close() const
{
	::glfwSetWindowShouldClose(glfw_window, GLFW_TRUE);
}

void ktf::window::swap_buffers() const
{
	::glfwSwapBuffers(glfw_window);
}

void ktf::window::enable_cursor() const
{
	::glfwSetInputMode(glfw_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void ktf::window::disable_cursor() const
{
	::glfwSetInputMode(glfw_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void ktf::window::reset_cursor()
{
	int width, height;
	::glfwGetWindowSize(glfw_window, &width, &height);
	::glfwSetCursorPos(glfw_window, width/2, height/2);
	this->m_cursor_pos.x = 0;
	this->m_cursor_pos.y = 0;
}

void ktf::window::set_title(const char* title)
{
	if (title) {
		::glfwSetWindowTitle(glfw_window, title);
	}
}

void ktf::window::set_icon(const char* icon_path)
{
	if (icon_path) {
		ktf::png png;
		png.load(icon_path);
		if (png.valid()) {
			GLFWimage image;
			image.width = png.get_width();
			image.height = png.get_height();
			image.pixels = png.get_data();
			::glfwSetWindowIcon(glfw_window, 1, &image);
		}
	}
}

std::string ktf::window::copy_from_clipboard() const
{
	if (char const * content = ::glfwGetClipboardString(NULL)) {
		return std::string(content);
	} else {
		return std::string();
	}
}

void ktf::window::copy_to_clipboard(std::string const& content) const
{
	::glfwSetClipboardString(NULL, content.c_str());
}

ktf::composer * ktf::window::get_composer()
{
	return &composer;
}

GLFWwindow * ktf::window::get_glfw_window()
{
	return glfw_window;
}


bool ktf::window::init_glfw(int width, int height, bool fullscreen, int version_major, int version_minor)
{
	::glfwSetErrorCallback(error_callback);
	::glfwInit();
	::glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	::glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, version_major);
	::glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, version_minor);
#ifndef NDEBUG
	if (version_major >= 4 && version_minor >= 3) {
		::glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	}
#endif

	this->fullscreen = fullscreen;
	GLFWmonitor *monitor = ::glfwGetPrimaryMonitor();
	const GLFWvidmode *mode = ::glfwGetVideoMode(monitor);
	this->framerate = std::min(mode->refreshRate, 60);
	if (fullscreen) {
		this->glfw_window = ::glfwCreateWindow(mode->width, mode->height, "ktf::window", monitor, NULL);
	} else {
		this->glfw_window = ::glfwCreateWindow(width, height, "ktf::window", NULL, NULL);
	}

	if (glfw_window == NULL) {
		::glfwTerminate();
		return false;
	}

	::glfwMakeContextCurrent(glfw_window);
	::glfwSetFramebufferSizeCallback(glfw_window, framebuffer_size_callback);

	return true;
}

bool ktf::window::window::init_opengl() const
{
	if(!::gladLoadGL()) {
		return false;
	} else {
#ifndef NDEBUG
		GLint ContextFlags;
		::glGetIntegerv(GL_CONTEXT_FLAGS, &ContextFlags);

		if (ContextFlags & GL_CONTEXT_FLAG_DEBUG_BIT) {
			::glEnable(GL_DEBUG_OUTPUT);
			::glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			::glDebugMessageCallback(gl_debug_output, 0);
		}
#endif
		return true;
	}
}

void ktf::window::terminate_glfw()
{
	if (!glfw_window) {
		return;
	}
	::glfwMakeContextCurrent(NULL);
	::glfwDestroyWindow(glfw_window);
	::glfwTerminate();
	this->glfw_window = nullptr;
}

void ktf::window::on_size_change(int width, int height)
{
	this->m_window_size = ktf::vec2<int>(width, height);
	ktf::window_event e(this, width, height);
	if (m_callbacks[event_size]) {
		this->m_callbacks[event_size](&e);
	}
}

void ktf::window::on_focus(int focused)
{
    ktf::window_event e(this, focused != 0);
	if (m_callbacks[event_focus]) {
		this->m_callbacks[event_focus](&e);
	}
}

void ktf::window::on_drop_event(int num, const char** paths)
{
    std::vector<std::string> path_list;
	for (int i=0; i<num; i++) {
		path_list.push_back(paths[i]);
	}
	ktf::drop_event e(this, path_list);
	if (m_callbacks[event_drop]) {
		this->m_callbacks[event_drop](&e);
	}
}

void ktf::window::on_key_event(int key, int scancode, int action, int mods)
{
	ktf::key_event e(this, key, scancode, action, mods);
	if (m_callbacks[event_key]) {
		this->m_callbacks[event_key](&e);
	}
}

void ktf::window::on_char_event(unsigned int codepoint)
{
	ktf::key_event e(this, codepoint);
	if (m_callbacks[event_char]) {
		this->m_callbacks[event_char](&e);
	}
}

void ktf::window::on_mouse_button_event(int button, int action, int mods)
{
	ktf::mouse_event e(this, m_cursor_pos.x, m_cursor_pos.y, button, action, mods);
	if (m_callbacks[event_button]) {
		this->m_callbacks[event_button](&e);
	}
}

void ktf::window::on_mouse_position_event(double x_pos, double y_pos)
{
	double x = x_pos - m_window_size.x/2.0;
	double y = y_pos - m_window_size.y/2.0;
	this->m_cursor_pos.x = x;
	this->m_cursor_pos.y = y;

	ktf::mouse_event e(this, m_cursor_pos.x, m_cursor_pos.y);
	if (m_callbacks[event_position]) {
		this->m_callbacks[event_position](&e);
	}
}

void ktf::window::on_mouse_scroll_event(double x_offset, double y_offset)
{
	ktf::mouse_event e(this, m_cursor_pos.x, m_cursor_pos.y, x_offset, y_offset);
	if (m_callbacks[event_scroll]) {
		this->m_callbacks[event_scroll](&e);
	}
}

#ifndef NDEBUG

#include <sstream>

void APIENTRY gl_debug_output(
	GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei, // length
	const char *message,
	const void * // user_param
)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) {
		//return;
	}

	std::stringstream s;
	s << "---------------" << std::endl;
	s << "Debug message (" << id << "): " <<  message << std::endl;

	// print log source
	s << "Source: ";
	switch (source) {
	case GL_DEBUG_SOURCE_API:
		s << "API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		s << "Window System";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		s << "Shader Compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		s << "Third Party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		s << "Application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		s << "Other";
		break;
	}
	s << std::endl;

	// print log type
	s << "Type: ";
    switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		s << "Error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		s << "Deprecated Behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		s << "Undefined Behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		s << "Portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		s << "Performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		s << "Marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		s << "Push Group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		s << "Pop Group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		s << "Other";
		break;
	}
	s << std::endl;

	// print log severity
	s << "Severity: ";
	switch (severity) {
	case GL_DEBUG_SEVERITY_HIGH:
		s << "high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		s << "medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		s << "low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		s << "notification";
		break;
	}
	s << std::endl;

	::printf("%s", s.str().c_str());
}
#endif
