#pragma once

#include "../math/matrix.hpp"

namespace ktf {

class buffer {

	private:
		unsigned int vao;
		unsigned int *vbos;
		unsigned int ebo;
		unsigned int size;
		unsigned int buffer_count;


	public:
		buffer();
		buffer(unsigned int vao, unsigned int* vbo, unsigned int ebo, unsigned int size, unsigned int buffer_count);
		buffer(buffer&& b);
		buffer(const buffer& b) = delete;
		~buffer();

		buffer& operator=(buffer&& b);
		buffer& operator=(const buffer& b) = delete;


	public:
		void bind() const;
		void unbind() const;
		void draw() const;
		void render() const;
		void render_instanced(ktf::matrix<float> const * const models, unsigned int count) const;
		unsigned int get_vbo(unsigned int index);

	private:
		void free_buffers();

};

} // namespace ktf
