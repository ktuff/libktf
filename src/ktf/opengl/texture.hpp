#pragma once

namespace ktf {

class texture {

	private:
		unsigned int m_type;
		unsigned int m_id;


	public:
		texture();
		texture(unsigned int type);
		texture(unsigned int type, unsigned int tex_id);
		texture(texture&& tex);
		texture(const texture& tex) = delete;
		~texture();
		texture& operator=(texture&& tex);
		texture& operator=(const texture& tex) = delete;


	public:
		unsigned int id() const;
		void bind(int target) const;
		void unbind(int target) const;

	private:
		inline void free() const;
};

} // namespace ktf
