#include <string.h>

#include <ktf/glad/glad.h>

#include "framebuffer.hpp"
#include "gl.hpp"


ktf::framebuffer::framebuffer()
{
    ::glGenFramebuffers(1, &id);
}

ktf::framebuffer::framebuffer(unsigned int id)
: id(id)
{
}

ktf::framebuffer::framebuffer(ktf::framebuffer && buffer)
: id(buffer.id)
{
    buffer.id = 0;
}

ktf::framebuffer::~framebuffer()
{
    this->free();
}

ktf::framebuffer& ktf::framebuffer::operator=(framebuffer&& buffer)
{
    this->free();
    
    this->id = buffer.id;
    buffer.id = 0;
    
    return *this;
}


void ktf::framebuffer::bind() const
{
    ::glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void ktf::framebuffer::bind_read() const
{
	::glBindFramebuffer(GL_READ_FRAMEBUFFER, id);
}

void ktf::framebuffer::bind_draw() const
{
	::glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id);
}

void ktf::framebuffer::unbind() const
{
	::glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ktf::framebuffer::read(std::vector<uint8_t>& data, int& width, int& height, int& channels) const
{
	ktf::gl::get_viewport_size(&width, &height);
	channels = 4;

	size_t row_size = width * channels;
	size_t num_bytes = height * row_size;
	uint8_t* pixels = new uint8_t[num_bytes];
	::glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	// invert image
	data.resize(num_bytes);
	for (long h=0; h<height; ++h) {
		::memcpy(&data[row_size * h], &pixels[(height - h - 1) * row_size], row_size);
	}
	delete[] pixels;
}

void ktf::framebuffer::free() const
{
	if (id) {
		::glDeleteFramebuffers(1, &id);
	}
}
