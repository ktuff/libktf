#pragma once

namespace ktf {

class gl {

	public:
		enum blend_factor_t {
			e_zero,
			e_one,
			e_src_color,
			e_one_minus_src_color,
			e_src_alpha,
			e_one_minus_src_alpha,
			e_dst_color,
			e_one_minus_dst_color,
			e_dst_alpha,
			e_one_minus_dst_alpha,
			e_constant_color,
			e_one_minus_constant_color,
			e_constant_alpha,
			e_one_minus_constant_alpha
		};
		enum blend_eq_t {
			e_eq_add,
			e_eq_sub,
			e_eq_rsub,
			e_eq_min,
			e_eq_max
		};


	public:
		static void set_viewport_size(int width, int height);
		static void get_viewport_size(int* width, int* height);
		static void clear();
		static void clear_color();
		static void clear_depth();
		static void set_clear_color(float r, float g, float b, float a);
		static void get_clear_color(float* r, float *g, float *b, float* a);
		static void enable_culling();
		static void disable_culling();
		static void enable_blending();
		static void disable_blending();
		static void set_blending_factors(blend_factor_t sf, blend_factor_t df);
		static void set_blend_color(float r, float g, float b, float a);
		static void set_blend_equation(blend_eq_t f);
		static void enable_depth_testing();
		static void disable_depth_testing();
		static void enable_depth_mask();
		static void disable_depth_mask();
		static void use_polygon_fill();
		static void use_polygon_lines();
};

} // namespace ktf
