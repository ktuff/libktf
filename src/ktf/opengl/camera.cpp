#include "../math/radians.hpp"
#include "camera.hpp"

ktf::camera::camera()
: fov(70.0)
, width(1.0)
, height(1.0)
, oznear(-1.0)
, ozfar(1.0)
, pznear(0.0)
, pzfar(1000.0)
, relative(false)
{
}

ktf::camera::~camera()
{
}


void ktf::camera::set_position(const ktf::vec3<double>& pos)
{
    this->pos = pos;
}

void ktf::camera::set_rotation(const ktf::vec3<double>& rot)
{
    double pitch = rot.x, yaw = rot.y, roll = rot.z;
    // limit values to [-180, 180] (yaw) and [-90, 90] (pitch)
    if (pitch >= 90) {
        pitch = 89.9;
    } else if (pitch <= -90) {
        pitch = -89.9;
    }
    while (yaw >= 180) {
        yaw -= 360;
    }
    while (yaw < -180) {
        yaw += 360;
    }
    while (roll >= 180) {
        roll -= 360;
    }
    while (roll < -180) {
        roll += 360;
    }
    this->rot = ktf::vec3<double>(pitch, yaw, roll);
    
    double x = std::cos(ktf::radians(pitch)) * std::cos(ktf::radians(yaw));
    double y = std::sin(ktf::radians(pitch));
    double z = std::cos(ktf::radians(pitch)) * std::sin(ktf::radians(yaw));
    this->dir = ktf::vec3<double>::normalize(ktf::vec3<double>(x, y, z));
}

void ktf::camera::set_fov(double fov)
{
    this->fov = fov;
    this->calc_persp();
}

void ktf::camera::set_relative(bool relative)
{
    this->relative = relative;
}

ktf::vec3<double> ktf::camera::get_position() const
{
    return pos;
}

ktf::vec3<double> ktf::camera::get_rotation() const
{
    return rot;
}

ktf::vec3<double> ktf::camera::get_direction() const
{
    return dir;
}

double ktf::camera::get_fov() const
{
    return fov;
}

void ktf::camera::set_viewport(double width, double height, double oznear, double ozfar, double pznear, double pzfar)
{
    this->width = width;
    this->height = height;
    this->oznear = oznear;
    this->ozfar = ozfar;
    this->pznear = pznear;
    this->pzfar = pzfar;
    
    this->update_matrices();
}

void ktf::camera::get_viewport(double* width, double* height, double* oznear, double* ozfar, double* pznear, double* pzfar) const
{
    *width = camera::width;
    *height = camera::height;
    *oznear = camera::oznear;
    *ozfar = camera::ozfar;
    *pznear = camera::pznear;
    *pzfar = camera::pzfar;
}

ktf::matrix<double> ktf::camera::look_at() const
{
    const ktf::vec3<double> up(0.0, 1.0, 0.0);
    const ktf::vec3<double>& f = dir; // is already normalized in set_rotation(...)
    const ktf::vec3<double> s(ktf::vec3<double>::normalize(ktf::vec3<double>::cross(f, up)));
    const ktf::vec3<double> u(ktf::vec3<double>::cross(s, f));
    
    ktf::vec3<double> p(0.0);
    if (!relative) {
        p = pos;
    }
    
    ktf::matrix<double> view;
    view[0] = {s.x, u.x, -f.x, 0.0};
    view[1] = {s.y, u.y, -f.y, 0.0};
    view[2] = {s.z, u.z, -f.z, 0.0};
    view[3] = {-(s*p), -(u*p), (f*p), 1.0};
    return view;
}

ktf::matrix<double> ktf::camera::orthogonal() const
{
    return ortho;
}

ktf::matrix<double> ktf::camera::perspective() const
{
    return persp;
}

void ktf::camera::calc_ortho()
{
    double left = -0.5 * width;
    double right = 0.5 * width;
    double bottom = -0.5 * height;
    double top = 0.5 * height;
    this->ortho = ktf::matrix<double>(1.0);
    this->ortho[0][0] = 2.0 / (right - left);
    this->ortho[1][1] = -2.0 / (top - bottom);
    this->ortho[2][2] = -2.0 / (ozfar - oznear);
    this->ortho[3][0] = -(right + left) / (right - left);
    this->ortho[3][1] = -(top + bottom) / (top - bottom);
    this->ortho[3][2] = -(ozfar + oznear) / (ozfar - oznear);
}

void ktf::camera::calc_persp()
{
    double aspect = width / height;
    double tan_fov = std::tan(ktf::radians(fov) * 0.5);
    ktf::matrix<double> proj(0.0);
    proj[0][0] = 1.0 / (aspect * tan_fov);
    proj[1][1] = 1.0 / (tan_fov);
    proj[2][2] = -(pzfar + pznear) / (pzfar - pznear);
    proj[2][3] = -1.0;
    proj[3][2] = -(2.0 * pzfar * pznear) / (pzfar - pznear);
    
    // combine projection and view matrix
    const ktf::matrix<double>& view = look_at();
    this->persp = proj * view;
    this->frustum.calculate(persp);
}

bool ktf::camera::point_in_view(const ktf::vec3<double>& p) const
{
    return frustum.point_in_frustum(p);
}

bool ktf::camera::sphere_in_view(const ktf::vec3<double>& center, float radius) const
{
    return frustum.sphere_in_frustum(center, radius);
}

bool ktf::camera::cube_in_view(const ktf::vec3<double>& p0, int size) const
{
    return frustum.cube_in_frustum(p0, size);
}

void ktf::camera::update_matrices()
{
	this->calc_ortho();
    this->calc_persp();
}
