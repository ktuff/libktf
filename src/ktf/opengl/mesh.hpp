#pragma once

#include "../container/vector.hpp"
#include "../math/vec.hpp"

namespace ktf {

struct mesh2d {
	ktf::vector<ktf::vec4<float>> pos;
	ktf::vector<ktf::vec4<float>> clr;
	ktf::vector<unsigned int> indices;
};

struct mesh3d {
	ktf::vector<ktf::vec3<float>> pos;
	ktf::vector<ktf::vec2<float>> tex;
	ktf::vector<ktf::vec4<float>> clr;
	ktf::vector<ktf::vec3<float>> nrm;
	ktf::vector<unsigned int> offsets;
	ktf::vector<unsigned int> indices;
};

} // namespace ktf
