#include <fstream>

#include <ktf/glad/glad.h>
#include "shader.hpp"

ktf::shader::shader(const std::vector<std::pair<std::string, unsigned int>>& paths)
: program_id(::glCreateProgram())
{
	std::vector<GLuint> shader_ids;
	for (auto const& s : paths) {
		GLuint shader_id = compile_shader(s.first, s.second);
		if (shader_id) {
			shader_ids.push_back(shader_id);
			::glAttachShader(program_id, shader_id);
		}
	}
	::glLinkProgram(program_id);
	int success;
	::glGetProgramiv(program_id, GL_LINK_STATUS, &success);
	if (!success) {
		GLint log_size;
		::glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_size);
		char* log = new char[log_size+1];
		::glGetProgramInfoLog(program_id, log_size, NULL, log);
		log[log_size] = '\0';
		::fprintf(stderr, "%s", log);
		::glDeleteProgram(program_id);
		this->program_id = 0;
	}
	for (auto const& shader_id : shader_ids) {
		::glDeleteShader(shader_id);
	}
}

ktf::shader::shader(ktf::shader && s)
: program_id(s.program_id)
{
    s.program_id = 0;
}

ktf::shader::~shader()
{
    if (program_id) {
        ::glDeleteProgram(program_id);
    }
}

ktf::shader & ktf::shader::operator=(ktf::shader && s)
{
    if (program_id) {
        ::glDeleteProgram(program_id);
    }
    this->program_id = s.program_id;
    s.program_id = 0;
    return *this;
}


bool ktf::shader::is_valid() const
{
    return program_id;
}

void ktf::shader::use() const
{
    if (program_id) {
        ::glUseProgram(program_id);
    }
}

void ktf::shader::set_uniform_1b(const char* uid, bool val) {
    ::glUniform1i(::glGetUniformLocation(program_id, uid), (int) val);
}

void ktf::shader::set_uniform_1i(const char* uid, int val) {
    ::glUniform1i(::glGetUniformLocation(program_id, uid), val);
}

void ktf::shader::set_uniform_1f(const char* uid, float val) {
    ::glUniform1f(::glGetUniformLocation(program_id, uid), val);
}

void ktf::shader::set_uniform_1d(const char* uid, double val) {
    ::glUniform1d(::glGetUniformLocation(program_id, uid), val);
}

void ktf::shader::set_uniform_2i(const char* uid, const ktf::vec2<int>& val) {
    ::glUniform2iv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_2f(const char* uid, const ktf::vec2<float>& val) {
    ::glUniform2fv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_2d(const char* uid, const ktf::vec2<double>& val) {
    ::glUniform2dv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_3i(const char* uid, const ktf::vec3<int>& val) {
    ::glUniform3iv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_3f(const char* uid, const ktf::vec3<float>& val) {
    ::glUniform3fv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_3d(const char* uid, const ktf::vec3<double>& val) {
    ::glUniform3dv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_4i(const char* uid, const ktf::vec4<int>& val) {
    ::glUniform4iv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_4f(const char* uid, const ktf::vec4<float>& val) {
    ::glUniform4fv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_4d(const char* uid, const ktf::vec4<double>& val) {
    ::glUniform4dv(::glGetUniformLocation(program_id, uid), 1, &val[0]);
}

void ktf::shader::set_uniform_4x4f(const char* uid, const ktf::matrix<float>& val) {
    ::glUniformMatrix4fv(::glGetUniformLocation(program_id, uid), 1, GL_FALSE,  &val[0][0]);
}

void ktf::shader::set_uniform_4x4d(const char* uid, const ktf::matrix<double>& val) {
    ::glUniformMatrix4dv(::glGetUniformLocation(program_id, uid), 1, GL_FALSE,  &val[0][0]);
}

unsigned int ktf::shader::compile_shader(const std::string& path, unsigned int type)
{
    std::string shader_code;
    std::ifstream file;
    try {
        file.open(path);
        if (!file.is_open()) {
            return 0;
        } else {
            std::string shader_code((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
            file.close();
            
            GLuint shader_id = ::glCreateShader(type);
            const char * src_ptr = shader_code.c_str();
            ::glShaderSource(shader_id, 1, &src_ptr, NULL);
            ::glCompileShader(shader_id);
            int success;
            ::glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);

            if(!success) {
                char log[512];
                ::glGetShaderInfoLog(shader_id, 512, NULL, log);
            }
            return shader_id;
        }
    } catch(const std::fstream::failure& f) {
        return 0;
    }
}
