#include "event.hpp"

ktf::event::event(ktf::window* pwindow, ktf::event_type type, int action, int mods)
: pwindow(pwindow)
, type(type)
, handled(false)
, action(action)
, mods(mods)
{
}


ktf::window_event::window_event(ktf::window* pwindow, int width, int height)
: event(pwindow, event_size, 0, 0)
, width(width)
, height(height)
, focused(false)
{
}

ktf::window_event::window_event(ktf::window* pwindow, bool focused)
: event(pwindow, event_focus, 0, 0)
, width(0)
, height(0)
, focused(focused)
{
}


ktf::drop_event::drop_event(ktf::window* pwindow, const std::vector<std::string>& paths)
: event(pwindow, event_drop, 0, 0)
, paths(paths)
{
}


ktf::key_event::key_event(ktf::window* pwindow, int codepoint)
: event(pwindow, event_char, 0, 0)
, codepoint(codepoint)
{
}

ktf::key_event::key_event(ktf::window* pwindow, int key, int scancode, int action, int mods)
: event(pwindow, event_key, action, mods)
, key(key)
, scancode(scancode)
{
}


ktf::mouse_event::mouse_event(ktf::window* pwindow, int x, int y)
: event(pwindow, event_position, 0, 0)
, x(x)
, y(y)
, button(0)
, offset_x(0)
, offset_y(0)
{
}

ktf::mouse_event::mouse_event(ktf::window* pwindow, int x, int y, int button, int action, int mods)
: event(pwindow, event_button, action, mods)
, x(x)
, y(y)
, button(button)
, offset_x(0)
, offset_y(0)
{
}

ktf::mouse_event::mouse_event(ktf::window* pwindow, int x, int y, int diffx, int diffy)
: event(pwindow, event_scroll, 0, 0)
, x(x)
, y(y)
, button(0)
, offset_x(diffx)
, offset_y(diffy)
{
}
