#pragma once

#include <string>
#include <vector>
#include "../math/vec.hpp"
#include "../math/matrix.hpp"

namespace ktf {

class shader {
    
    private:
        unsigned int program_id;
        
        
    public:
        shader(const std::vector<std::pair<std::string, unsigned int>>& paths);
        shader(shader&& s);
        shader(const shader& s) = delete;
        ~shader();
        shader& operator=(shader&& s);
        shader& operator=(const shader& s) = delete;
        
        
    public:
        bool is_valid() const;
        void use() const;
        void set_uniform_1b(const char* uid, bool val);
        void set_uniform_1i(const char* uid, int val);
        void set_uniform_1f(const char* uid, float val);
        void set_uniform_1d(const char* uid, double val);
        void set_uniform_2i(const char* uid, const ktf::vec2<int>& val);
        void set_uniform_2f(const char* uid, const ktf::vec2<float>& val);
        void set_uniform_2d(const char* uid, const ktf::vec2<double>& val);
        void set_uniform_3i(const char* uid, const ktf::vec3<int>& val);
        void set_uniform_3f(const char* uid, const ktf::vec3<float>& val);
        void set_uniform_3d(const char* uid, const ktf::vec3<double>& val);
        void set_uniform_4i(const char* uid, const ktf::vec4<int>& val);
        void set_uniform_4f(const char* uid, const ktf::vec4<float>& val);
        void set_uniform_4d(const char* uid, const ktf::vec4<double>& val);
        void set_uniform_4x4f(const char* uid, const ktf::matrix<float>& val);
        void set_uniform_4x4d(const char* uid, const ktf::matrix<double>& val);
        
    private:
        unsigned int compile_shader(const std::string& path, unsigned int type);
};

} // namespace ktf
