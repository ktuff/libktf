find_package(glfw3 3.3 REQUIRED)
include_directories(${glfw3_INCLUDE_DIRS})
find_package(OpenGL REQUIRED)
include_directories(${OpenGL_INCLUDE_DIRS})

add_library(ktfgl STATIC
    buffer.cpp
    buffer_texture.cpp
    camera.cpp
    composer.cpp
    event.cpp
    framebuffer.cpp
    gl.cpp
    material.cpp
    shader.cpp
    texture.cpp
    window.cpp
)

target_link_libraries(ktfgl ${OPENGL_gl_LIBRARY})
target_link_libraries(ktfgl ${OPENGL_glu_LIBRARY})
target_link_libraries(ktfgl OpenGL::GL ${CMAKE_DL_LIBS})
target_link_libraries(ktfgl glad glfw ktfmath ktftexture)
