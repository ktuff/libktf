#include "../glad/glad.h"
#include "../texture/png.hpp"
#include "buffer.hpp"
#include "buffer_texture.hpp"
#include "composer.hpp"
#include "framebuffer.hpp"
#include "gl.hpp"
#include "shader.hpp"
#include "texture.hpp"


bool const ktf::composer::bool_true = GL_TRUE;
bool const ktf::composer::bool_false = GL_FALSE;
size_t const ktf::composer::data_type_byte = GL_BYTE;
size_t const ktf::composer::data_type_ubyte = GL_UNSIGNED_BYTE;
size_t const ktf::composer::data_type_short = GL_SHORT;
size_t const ktf::composer::data_type_ushort = GL_UNSIGNED_SHORT;
size_t const ktf::composer::data_type_int = GL_INT;
size_t const ktf::composer::data_type_uint = GL_UNSIGNED_INT;
size_t const ktf::composer::data_type_float = GL_FLOAT;
size_t const ktf::composer::data_type_double = GL_DOUBLE;
size_t const ktf::composer::draw_hint_static = GL_STATIC_DRAW;
size_t const ktf::composer::draw_hint_dynamic = GL_DYNAMIC_DRAW;
size_t const ktf::composer::shader_type_vertex = GL_VERTEX_SHADER;
size_t const ktf::composer::shader_type_fragment = GL_FRAGMENT_SHADER;
size_t const ktf::composer::shader_type_geometry = GL_GEOMETRY_SHADER;
size_t const ktf::composer::shader_type_compute = GL_COMPUTE_SHADER;


ktf::composer::composer()
{
	this->free();
}

ktf::composer::~composer()
{
}


void ktf::composer::free()
{
	this->used_shader = nullptr;

	this->shaders.clear();
	this->textures.clear();
	this->meshes.clear();
	this->font.free();

	this->m_framebuffers.push(std::make_unique<ktf::framebuffer>(0));
}

void ktf::composer::register_shader(size_t* id, const std::string& vertex_path, const std::string& fragment_path)
{
	std::vector<std::pair<std::string, unsigned int>> paths = {
		{vertex_path, GL_VERTEX_SHADER},
		{fragment_path, GL_FRAGMENT_SHADER}
	};
	std::shared_ptr<ktf::shader> shader = std::make_shared<ktf::shader>(paths);
	if (shader->is_valid()) {
		*id = shaders.push(shader);
	} else {
		*id = (size_t)-1;
	}
}

void ktf::composer::deregister_shader(size_t* id)
{
	if (shaders.valid(*id)) {
		std::shared_ptr<ktf::shader> shader = shaders.pop(*id);
		if (shader == used_shader) {
			this->used_shader = nullptr;
		}
	}
}

void ktf::composer::bind_shader(size_t id)
{
	if (shaders.valid(id)) {
		this->used_shader = shaders.get(id);
		this->used_shader->use();
	}
}

void ktf::composer::register_texture(size_t* id, const std::string& path)
{
	ktf::png image;
	image.load(path);
	if (image.valid()) {
		unsigned int tex_id;
		GLenum color = get_color_enum(image.get_channels());
		::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		::glGenTextures(1, &tex_id);
		::glBindTexture(GL_TEXTURE_2D, tex_id);
		::glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.get_width(), image.get_height(), 0, color, GL_UNSIGNED_BYTE, image.get_data());
		::glGenerateMipmap(GL_TEXTURE_2D);

		::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		::glBindTexture(GL_TEXTURE_2D, 0);
		std::shared_ptr<ktf::texture> texture = std::make_shared<ktf::texture>(GL_TEXTURE_2D, tex_id);
		*id = textures.push(texture);
	} else {
		*id = (size_t)-1;
	}
}

void ktf::composer::register_texture(size_t* id, int width, int height, int channels, uint8_t* data, size_t data_type, fb_attachment_t attachment)
{
	GLenum colors;
	switch (attachment) {
	case e_fb_none:
	case e_fb_color:
		colors = get_color_enum(channels);
		break;
	case e_fb_depth:
		colors = GL_DEPTH_COMPONENT;
		break;
	case e_fb_depth_stencil:
		colors = GL_DEPTH_STENCIL;
		break;
	default:
		return;
	}

	unsigned int tex_id;
	::glGenTextures(1, &tex_id);
	std::shared_ptr<ktf::texture> texture = std::make_shared<ktf::texture>(GL_TEXTURE_2D, tex_id);
	texture->bind(0);
	::glTexImage2D(GL_TEXTURE_2D, 0, colors, width, height, 0, colors, data_type, data);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	texture->unbind(0);

	*id = textures.push(texture);
}

void ktf::composer::deregister_texture(size_t* id)
{
	if (textures.valid(*id)) {
		this->textures.pop(*id);
	}
}

void ktf::composer::bind_texture(size_t id, int target) const
{
	if (textures.valid(id)) {
		this->textures.get(id)->bind(target);
	}
}

void ktf::composer::register_cubemap(size_t* id, const std::array<ktf::png, 6>& texture_data)
{
	int gl_ids[6] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};

	unsigned int tex_id;
	::glGenTextures(1, &tex_id);
	std::shared_ptr<ktf::texture> texture = std::make_shared<ktf::texture>(GL_TEXTURE_CUBE_MAP, tex_id);
	texture->bind(0);
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    for(int i=0; i<6; i++) {
		if (texture_data[i].valid()) {
			GLenum color = get_color_enum(texture_data[i].get_channels());
			::glTexImage2D(gl_ids[i], 0, GL_RGBA, texture_data[i].get_width(), texture_data[i].get_height(), 0, color, GL_UNSIGNED_BYTE, texture_data[i].get_data());
		}
	}
	::glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	::glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	::glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	::glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	::glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	::glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	texture->unbind(0);

	*id = textures.push(texture);
}

void ktf::composer::register_buffer_texture(size_t* id, size_t size)
{
	std::shared_ptr<ktf::buffer_texture> tex = std::make_shared<ktf::buffer_texture>(size);
	*id = buffer_textures.push(tex);
}

void ktf::composer::fill_buffer_texture(size_t id, const char* data) const
{
	if (buffer_textures.valid(id)) {
		buffer_textures.get(id)->fill(data);
	}
}

void ktf::composer::bind_buffer_texture(size_t id) const
{
	if (buffer_textures.valid(id)) {
		buffer_textures.get(id)->bind();
	}
}

void ktf::composer::deregister_buffer_texture(size_t* id)
{
	if (buffer_textures.valid(*id)) {
		this->buffer_textures.pop(*id);
		*id = (size_t)-1;
	}
}

void ktf::composer::register_framebuffer(size_t* id)
{
	*id = m_framebuffers.push(std::make_unique<ktf::framebuffer>());
}

void ktf::composer::bind_framebuffer(size_t id)
{
	if (m_framebuffers.valid(id)) {
		this->m_framebuffers.get(id)->bind();
	}
}

void ktf::composer::copy_framebuffer(size_t src, size_t dest, ktf::fb_attachment_t type, int src_w, int src_h, int dest_w, int dest_h)
{
	if (!m_framebuffers.valid(src) || !m_framebuffers.valid(dest)) {
		return;
	}

	GLbitfield attachment_bit;
	switch (type) {
	case e_fb_color:
		attachment_bit = GL_COLOR_BUFFER_BIT;
		break;
	case e_fb_depth:
		attachment_bit = GL_DEPTH_BUFFER_BIT;
		break;
	default:
		return;
	}

	GLenum filter = GL_NEAREST;
	this->m_framebuffers.get(src)->bind_read();
	this->m_framebuffers.get(dest)->bind_draw();
	::glBlitFramebuffer(0, 0, src_w, src_h, 0, 0, dest_w, dest_h, attachment_bit, filter);
	this->m_framebuffers.get(0)->bind();
}

void ktf::composer::bind_framebuffer_texture(size_t fb_id, size_t tex_id, ktf::fb_attachment_t type)
{
	if (!m_framebuffers.valid(fb_id) || !textures.valid(tex_id)) {
		return;
	}

	GLenum attachment;
	switch (type) {
	case e_fb_color:
		attachment = GL_COLOR_ATTACHMENT0;
		break;
	case e_fb_depth:
		attachment = GL_DEPTH_ATTACHMENT;
		break;
	default:
		return;
	}
	auto const& fb = m_framebuffers.get(fb_id);
	auto const& tex = textures.get(tex_id);
	fb->bind();
	::glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, tex->id(), 0);
	fb->unbind();
}

void ktf::composer::unbind_framebuffer_texture(size_t fb_id, ktf::fb_attachment_t type)
{
	if (!m_framebuffers.valid(fb_id)) {
		return;
	}
	GLenum attachment;
	switch (type) {
	case e_fb_color:
		attachment = GL_COLOR_ATTACHMENT0;
		break;
	case e_fb_depth:
		attachment = GL_DEPTH_ATTACHMENT;
		break;
	default:
		return;
	}
	auto const& fb = m_framebuffers.get(fb_id);
	fb->bind();
	::glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, 0, 0);
	fb->unbind();
}

void ktf::composer::read_framebuffer(size_t id, std::vector<uint8_t>& data, int& width, int& height, int& channels)
{
	if (m_framebuffers.valid(id)) {
		this->m_framebuffers.get(id)->read(data, width, height, channels);
	}
}

void ktf::composer::deregister_framebuffer(size_t* id)
{
	if (m_framebuffers.valid(*id)) {
		this->m_framebuffers.pop(*id);
	}
}

void ktf::composer::register_mesh(size_t* id, const std::vector<buffer_data>& data, const std::vector<buffer_attrib>& attribs, const ktf::vector<unsigned int>& indices)
{
	if (data.empty() || indices.size() == 0) {
		*id = -1;
		return;
	}

	GLuint vao;
	GLuint ebo;
	::glGenVertexArrays(1, &vao);
	::glBindVertexArray(vao);

	size_t const n = data.size();
	GLuint* vbos = new GLuint[n];
	::glGenBuffers(n, vbos);
	for (size_t i=0; i<n; ++i) {
		::glBindBuffer(GL_ARRAY_BUFFER, vbos[i]);
		::glBufferData(GL_ARRAY_BUFFER, data[i].size, data[i].data, data[i].draw_hint);
	}
	for (size_t i=0; i<attribs.size(); ++i) {
		::glBindBuffer(GL_ARRAY_BUFFER, vbos[attribs[i].data_index]);
		::glEnableVertexAttribArray(attribs[i].attrib_index);
		GLenum const& type = attribs[i].data_type;
		if (type == GL_BYTE || type == GL_UNSIGNED_BYTE || type == GL_UNSIGNED_SHORT || type == GL_SHORT || type == GL_UNSIGNED_INT || type == GL_INT) {
			::glVertexAttribIPointer(attribs[i].attrib_index, attribs[i].num_elements, type, attribs[i].size_element, (void*)attribs[i].offset);
		} else if (type == GL_DOUBLE) {
			::glVertexAttribLPointer(attribs[i].attrib_index, attribs[i].num_elements, type, attribs[i].size_element, (void*)attribs[i].offset);
		} else {
			::glVertexAttribPointer(attribs[i].attrib_index, attribs[i].num_elements, type, attribs[i].normalize, attribs[i].size_element, (void*)attribs[i].offset);
		}
		::glVertexAttribDivisor(attribs[i].attrib_index, attribs[i].divisor);
	}

	::glGenBuffers(1, &ebo);
	::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	::glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

	::glBindVertexArray(0);

	std::shared_ptr<ktf::buffer> buffer = std::make_shared<ktf::buffer>(vao, vbos, ebo, indices.size(), n);
	delete[] vbos;
	*id = meshes.push(buffer);
}

void ktf::composer::deregister_mesh(size_t* id)
{
	if (meshes.valid(*id)) {
		this->meshes.pop(*id);
	}
}

void ktf::composer::update_mesh_buffer(size_t id, buffer_data const& data, size_t index)
{
	if (!meshes.valid(id)) {
		return;
	}

	auto buffer = meshes.get(id);
	buffer->bind();
	::glBindBuffer(GL_ARRAY_BUFFER, buffer->get_vbo(index));
	::glBufferSubData(GL_ARRAY_BUFFER, 0, data.size, data.data);
	buffer->unbind();
}

void ktf::composer::render_mesh(size_t id, const ktf::matrix<float>& model) const
{
	if (meshes.valid(id) && used_shader) {
		this->used_shader->set_uniform_4x4f("model", model);
		this->meshes.get(id)->render();
	}
}

void ktf::composer::render_mesh(size_t id, const ktf::matrix<float> *const models, unsigned int count) const
{
	if (meshes.valid(id) && used_shader) {
		this->meshes.get(id)->render_instanced(models, count);
	}
}

const ktf::font * ktf::composer::get_font() const
{
	return &font;
}

void ktf::composer::load_font(const std::string& path, const ktf::vec2<uint32_t>& size, const std::string& temp_folder, const std::vector<uint32_t>& chars)
{
	this->font.load(path, size.x, size.y, temp_folder, chars);
}

void ktf::composer::unload_font()
{
	this->font.free();
}

void ktf::composer::begin_frame()
{
	ktf::gl::clear();
	int width, height;
	double oznear = -1.0, ozfar = 1.0;
	double pznear = 0.001, pzfar = 1000.0;
	ktf::gl::get_viewport_size(&width, &height);
	this->camera.set_viewport(width, height, oznear, ozfar, pznear, pzfar);
}

void ktf::composer::end_frame()
{
	this->check_error();
}

void ktf::composer::use_orthogonal_projection()
{
	if (used_shader) {
		ktf::matrix<float> mat = camera.orthogonal();
		this->used_shader->set_uniform_4x4f("projection", mat);
	}
}

void ktf::composer::use_perspective_projection()
{
	if (used_shader) {
		ktf::matrix<float> mat = camera.perspective();
		this->used_shader->set_uniform_4x4f("projection", mat);
	}
}

ktf::camera * ktf::composer::get_camera()
{
	return &camera;
}

void ktf::composer::generate_texture(const std::string& path, int width, int height, int channels, size_t shader_id, size_t texture_id, size_t mesh_id, const ktf::matrix<float>& model)
{
	ktf::framebuffer framebuffer;
	framebuffer.bind();
	unsigned int tex_id;
	::glGenTextures(1, &tex_id);
	ktf::texture texture(GL_TEXTURE_2D, tex_id);
	texture.bind(0);

	GLenum colors = get_color_enum(channels);
	::glTexImage2D(GL_TEXTURE_2D, 0, colors, width, height, 0, colors, GL_UNSIGNED_BYTE, NULL);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	::glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_id, 0);

	framebuffer.unbind();
	framebuffer.bind();

	if (::glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
		// store old values
		int vpwidth, vpheight;
		ktf::vec4<float> color;
		ktf::gl::get_viewport_size(&vpwidth, &vpheight);
		ktf::gl::get_clear_color(&color.x, &color.y, &color.z, &color.w);
		ktf::gl::set_clear_color(0.0f, 0.0f, 0.0f, 0.0f);

		// setup
		ktf::gl::set_viewport_size(width, height);
		this->camera.set_viewport(width, height, -1000.0, 1000.0, 0.0, 1000.0);
		ktf::gl::enable_depth_testing();
		ktf::gl::enable_blending();
		ktf::gl::enable_culling();
		ktf::gl::clear();

		// render
		this->bind_shader(shader_id);
		this->bind_texture(texture_id);
		this->use_orthogonal_projection();
		this->render_mesh(mesh_id, model);

		// save rendered image
		int w, h, c;
		std::vector<uint8_t> pixel_data;
		framebuffer.read(pixel_data, w, h, c);
		ktf::png texture(w, h, c, pixel_data.data());
		texture.write(path);

		// load old values
		ktf::gl::set_viewport_size(vpwidth, vpheight);
		ktf::gl::set_clear_color(color.x, color.y, color.z, color.w);

		ktf::gl::clear();
	}

	texture.unbind(0);
	framebuffer.unbind();
}

void ktf::composer::set_uniform_1b(const char* id, bool value)
{
	if (used_shader) {
		this->used_shader->set_uniform_1b(id, value);
	}
}

void ktf::composer::set_uniform_1i(const char* id, int value)
{
	if (used_shader) {
		this->used_shader->set_uniform_1i(id, value);
	}
}

void ktf::composer::set_uniform_1f(const char* id, float value)
{
	if (used_shader) {
		this->used_shader->set_uniform_1f(id, value);
	}
}

void ktf::composer::set_uniform_1d(const char* id, double value)
{
	if (used_shader) {
		this->used_shader->set_uniform_1d(id, value);
	}
}

void ktf::composer::set_uniform_2i(const char* id, const ktf::vec2<int>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_2i(id, value);
	}
}

void ktf::composer::set_uniform_2f(const char* id, const ktf::vec2<float>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_2f(id, value);
	}
}

void ktf::composer::set_uniform_2d(const char* id, const ktf::vec2<double>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_2d(id, value);
	}
}

void ktf::composer::set_uniform_3i(const char* id, const ktf::vec3<int>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_3i(id, value);
	}
}

void ktf::composer::set_uniform_3f(const char* id, const ktf::vec3<float>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_3f(id, value);
	}
}

void ktf::composer::set_uniform_3d(const char* id, const ktf::vec3<double>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_3d(id, value);
	}
}

void ktf::composer::set_uniform_4i(const char* id, const ktf::vec4<int>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_4i(id, value);
	}
}

void ktf::composer::set_uniform_4f(const char* id, const ktf::vec4<float>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_4f(id, value);
	}
}

void ktf::composer::set_uniform_4d(const char* id, const ktf::vec4<double>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_4d(id, value);
	}
}

void ktf::composer::set_uniform_4x4f(const char* id, const ktf::matrix<float>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_4x4f(id, value);
	}
}

void ktf::composer::set_uniform_4x4d(const char* id, const ktf::matrix<double>& value)
{
	if (used_shader) {
		this->used_shader->set_uniform_4x4d(id, value);
	}
}

void ktf::composer::check_error()
{
	GLenum errorCode;
	while ((errorCode = ::glGetError()) != GL_NO_ERROR) {
		std::string error("OpenGL: ");
		switch (errorCode) {
		case GL_INVALID_ENUM:
			error += "INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			error += "INVALID_VALUE";
			break;
		case GL_INVALID_OPERATION:
			error += "INVALID_OPERATION";
			break;
		case GL_STACK_OVERFLOW:
			error += "STACK_OVERFLOW";
			break;
		case GL_STACK_UNDERFLOW:
			error += "STACK_UNDERFLOW";
			break;
		case GL_OUT_OF_MEMORY:
			error += "OUT_OF_MEMORY";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			error += "INVALID_FRAMEBUFFER_OPERATION";
			break;
		default:
			error += "UNKNOWN ERROR";
			break;
		}
		::fprintf(stderr, "%s\n", error.c_str());
	}
}

unsigned int ktf::composer::get_color_enum(int channels)
{
	switch(channels) {
	case 1:
		return GL_RED;
	case 2:
		return GL_RG;
	case 3:
		return GL_RGB;
	case 4:
		return GL_RGBA;
	default:
		return GL_NONE;
	}
}
