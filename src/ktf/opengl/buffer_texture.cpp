#include "../glad/glad.h"

#include "buffer_texture.hpp"

ktf::buffer_texture::buffer_texture()
: m_id(0)
, m_buffer_id(0)
, m_size(0)
{
}

ktf::buffer_texture::buffer_texture(size_t size)
: m_size(size)
{
	if (size > GL_MAX_TEXTURE_BUFFER_SIZE) {
		size = GL_MAX_TEXTURE_BUFFER_SIZE;
	}

	::glGenBuffers(1, &m_buffer_id);
	::glBindBuffer(GL_TEXTURE_BUFFER, m_buffer_id);
	::glBufferData(GL_TEXTURE_BUFFER, size, NULL, GL_DYNAMIC_DRAW);

	::glGenTextures(1, &m_id);
	::glBindTexture(GL_TEXTURE_BUFFER, m_id);
	::glTexBuffer(GL_TEXTURE_BUFFER, GL_R32F, m_buffer_id);
	::glBindBuffer(GL_TEXTURE_BUFFER, 0);
	::glBindTexture(GL_TEXTURE_BUFFER, 0);
}

ktf::buffer_texture::buffer_texture(ktf::buffer_texture && b)
: m_id(b.m_id)
, m_buffer_id(b.m_buffer_id)
, m_size(b.m_size)
{
	b.m_id = 0;
	b.m_buffer_id = 0;
	b.m_size = 0;
}

ktf::buffer_texture::~buffer_texture()
{
	::glDeleteTextures(1, &m_id);
	::glDeleteBuffers(1, &m_buffer_id);
}


ktf::buffer_texture & ktf::buffer_texture::operator=(ktf::buffer_texture && b)
{
	this->m_id = b.m_id;
	this->m_buffer_id = b.m_buffer_id;
	this->m_size = b.m_size;
	b.m_id = 0;
	b.m_buffer_id = 0;
	b.m_size = 0;
	return *this;
}


void ktf::buffer_texture::fill(char const* data) const
{
	::glBindBuffer(GL_TEXTURE_BUFFER, m_buffer_id);
	::glBufferSubData(GL_TEXTURE_BUFFER, 0, m_size, data);
	::glBindBuffer(GL_TEXTURE_BUFFER, 0);
}

void ktf::buffer_texture::bind() const
{
	::glBindTexture(GL_TEXTURE_BUFFER, m_id);
}
