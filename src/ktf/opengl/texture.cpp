#include <ktf/glad/glad.h>
#include "texture.hpp"

ktf::texture::texture()
: m_type(0)
, m_id(0)
{
}

ktf::texture::texture(unsigned int type)
: m_type(type)
{
	::glGenTextures(1, &m_id);
}

ktf::texture::texture(unsigned int type, unsigned int tex_id)
: m_type(type)
, m_id(tex_id)
{
}

ktf::texture::texture(ktf::texture && tex)
: m_type(tex.m_type)
, m_id(tex.m_id)
{
	tex.m_type = 0;
	tex.m_id = 0;
}

ktf::texture::~texture()
{
	this->free();
}

ktf::texture & ktf::texture::operator=(ktf::texture && tex)
{
	this->free();

	this->m_type = tex.m_type;
	this->m_id = tex.m_id;
	tex.m_type = 0;
	tex.m_id = 0;
	return *this;
}

unsigned int ktf::texture::id() const
{
	return m_id;
}

void ktf::texture::bind(int target) const
{
	::glActiveTexture(GL_TEXTURE0 + target);
	::glBindTexture(m_type, m_id);
}

void ktf::texture::unbind(int target) const
{
	::glActiveTexture(GL_TEXTURE0 + target);
	::glBindTexture(m_type, 0);
}

inline void ktf::texture::free() const
{
	if (m_id) {
		::glDeleteTextures(1, &m_id);
	}
}
