#pragma once

#include "composer.hpp"
#include "event.hpp"

struct GLFWwindow;

namespace ktf {

class window {

	public:
		static void error_callback(int error, const char* description);
		static void framebuffer_size_callback(GLFWwindow* glfw_window, int width, int height);
		static void window_focus_callback(GLFWwindow* window, int focused);
		static void drop_callback(GLFWwindow* window, int num, const char** paths);
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void character_callback(GLFWwindow* window, unsigned int codepoint);
		static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
		static void mouse_position_callback(GLFWwindow* window, double x_pos, double y_pos);
		static void mouse_scroll_callback(GLFWwindow* window, double x_offset, double y_offset);


	private:
		GLFWwindow* glfw_window;
		int position_x, position_y;
		int size_x, size_y;
		bool fullscreen;
		int framerate;

		ktf::composer composer;
		ktf::vec2<double> m_cursor_pos;
		ktf::vec2<int> m_window_size;
		std::function<void(ktf::event* event)> m_callbacks[8];


	public:
		window();
		~window();


	public:
		bool create(int width, int height, bool fullscreen, int version_major, int version_minor, const char* title = nullptr);
		void destroy();
		void sync_display(bool value) const;
		void set_framerate(int rate);
		void toggle_fullscreen();
		void poll() const;
		bool should_close() const;
		void close() const;
		void swap_buffers() const;
		void enable_cursor() const;
		void disable_cursor() const;
		void reset_cursor();
		void set_title(char const *title);
		void set_icon(char const *icon_path);
		ktf::composer* get_composer();
		GLFWwindow* get_glfw_window();
		std::string copy_from_clipboard() const;
		void copy_to_clipboard(std::string const& content) const;
		template<event_type T> void on(std::function<void(ktf::event* event)> callback);

	private:
		bool init_glfw(int width, int height, bool fullscreen, int version_major, int version_minor);
		bool init_opengl() const;
		void terminate_glfw();

	// events
	private:
		void on_size_change(int width, int height);
		void on_focus(int focused);
		void on_drop_event(int num, const char** paths);
		void on_key_event(int key, int scancode, int action, int mods);
		void on_char_event(unsigned int codepoint);
		void on_mouse_button_event(int button, int action, int mods);
		void on_mouse_position_event(double x_pos, double y_pos);
		void on_mouse_scroll_event(double x_offset, double y_offset);
};

} // namespace ktf


/*
 * Template functions
 */
template<ktf::event_type T>
void ktf::window::on(std::function<void (ktf::event *)> callback)
{
	this->m_callbacks[T] = callback;
}
