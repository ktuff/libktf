#include "../glad/glad.h"
#include "buffer.hpp"

ktf::buffer::buffer()
: vao(0)
, vbos(nullptr)
, ebo(0)
, size(0)
, buffer_count(0)
{
}

ktf::buffer::buffer(uint vao, uint* vbo, uint ebo, uint size, uint buffer_count)
: vao(vao)
, ebo(ebo)
, size(size)
, buffer_count(buffer_count)
{
	this->vbos = new uint[buffer_count];
	for (uint i=0; i<buffer_count; ++i) {
		this->vbos[i] = vbo[i];
	}
}

ktf::buffer::buffer(ktf::buffer && b)
: vao(b.vao)
, vbos(b.vbos)
, ebo(b.ebo)
, size(b.size)
, buffer_count(b.buffer_count)
{
	b.vao = 0;
	b.vbos = nullptr;
	b.ebo = 0;
	b.size = 0;
	b.buffer_count = 0;
}

ktf::buffer::~buffer()
{
	this->free_buffers();
}

ktf::buffer & ktf::buffer::operator=(ktf::buffer && b)
{
	this->free_buffers();

	this->vao = b.vao;
	this->vbos = b.vbos;
	this->ebo = b.ebo;
	this->size = b.size;
	this->buffer_count = b.buffer_count;
	b.vao = 0;
	b.vbos = 0;
	b.ebo = 0;
	b.size = 0;
	b.buffer_count = 0;
	return *this;
}


void ktf::buffer::bind() const
{
	::glBindVertexArray(vao);
}

void ktf::buffer::unbind() const
{
	::glBindVertexArray(0);
}

void ktf::buffer::draw() const
{
	::glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);
}

void ktf::buffer::render() const
{
	this->bind();
	this->draw();
#ifndef KTF_NO_BUFFER_UNBIND
	this->unbind();
#endif
}

void ktf::buffer::render_instanced(const ktf::matrix<float> *const models, unsigned int count) const
{
	this->bind();

	// create temporary buffer
	GLuint matrices;
	::glGenBuffers(1, &matrices);
	::glBindBuffer(GL_ARRAY_BUFFER, matrices);
	::glBufferData(GL_ARRAY_BUFFER, count * sizeof(ktf::matrix<float>), &models[0], GL_STATIC_DRAW);
	::glEnableVertexAttribArray(4);
	::glEnableVertexAttribArray(5);
	::glEnableVertexAttribArray(6);
	::glEnableVertexAttribArray(7);
	::glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(ktf::matrix<float>), (void*) (0 * sizeof(ktf::vec4<float>)));
	::glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(ktf::matrix<float>), (void*) (1 * sizeof(ktf::vec4<float>)));
	::glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(ktf::matrix<float>), (void*) (2 * sizeof(ktf::vec4<float>)));
	::glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(ktf::matrix<float>), (void*) (3 * sizeof(ktf::vec4<float>)));
	::glVertexAttribDivisor(4, 1);
	::glVertexAttribDivisor(5, 1);
	::glVertexAttribDivisor(6, 1);
	::glVertexAttribDivisor(7, 1);
	::glBindBuffer(GL_ARRAY_BUFFER, 0);
	// draw
	::glDrawElementsInstanced(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0, count);
	// clean up
	::glDeleteBuffers(1, &matrices);

	this->unbind();
}

unsigned int ktf::buffer::get_vbo(unsigned int index)
{
	if (index < buffer_count) {
		return vbos[index];
	} else {
		return 0;
	}
}

void ktf::buffer::free_buffers()
{
	if (ebo) {
		::glDeleteBuffers(1, &ebo);
	}
	if (vbos) {
		for (uint i=0; i<buffer_count; ++i) {
			if (vbos[i] != 0) {
				::glDeleteBuffers(1, &vbos[i]);
			}
		}
		delete[] vbos;
	}
	::glDeleteVertexArrays(1, &vao);
}
