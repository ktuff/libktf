#pragma once

#include <string>
#include <ktf/math/vec3.hpp>

namespace ktf {

class composer;

class material {

	public:
		std::string m_name;
		ktf::vec3<float> m_diffuse_color;
		ktf::vec3<float> m_specular_color;
		float m_specular_strength;
		float m_specular_exponent;
		float m_opacity;
		size_t m_texture_id;


	public:
		material();
		material(std::string const& name);
		material(material && m) = default;
		material(material const& m) = default;
		~material() = default;


	public:
		material& operator=(material && m);
		material& operator=(material const& m);
		void apply(ktf::composer * sh) const;
};

} // namespace ktf
