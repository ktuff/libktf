#pragma once

#include <set>
#include <string>
#include <unordered_map>

#include "../opengl/mesh.hpp"
#include "../texture/font.hpp"
#include "../texture/texture_atlas.hpp"

namespace ktf {
    
class composer;
class material;
    
class assets_index {

	public:
		static ktf::vec4<float> const sprite_vertices[4];
		static unsigned int const sprite_indices[6];

	public:
		struct sprite {
			size_t id;
			float ratio;
		};

	protected:
		// config
		std::string folder;
		ktf::composer* composer;

		// data
		std::unordered_map<std::string, size_t> shader_map;
		std::unordered_map<std::string, size_t> texture_map;
		std::unordered_map<std::string, size_t> mesh_map;
		std::unordered_map<std::string, ktf::texture_atlas> atlas_map;
		std::unordered_map<std::string, std::shared_ptr<ktf::material>> material_map;
		std::unordered_map<std::string, ktf::assets_index::sprite> sprites;
		std::set<size_t> string_set;


	public:
		assets_index(const std::string& assets_folder, ktf::composer* composer);
		~assets_index();


	public:
		void clear();

		void load_shader(const std::string& name, const std::string& path_vertex, const std::string& path_fragment);
		void load_texture(const std::string& name, const std::string& path);
		void load_cube_texture(std::string const& name, std::array<ktf::png, 6> const& textures);
		void load_cube_texture(const std::string& name, const std::array<std::string, 6>& path);
		void load_mesh(const std::string& path);
		void load_mesh(const std::string& name, const std::string& path);
		void load_mesh(const std::string& name, ktf::mesh2d const& mesh);
		void load_mesh(const std::string& name, ktf::mesh3d const& mesh);
		void load_material(const std::string& path);
		void remove_shader(const std::string& name);
		void remove_texture(const std::string& name);
		void remove_mesh(const std::string& name);
		void remove_material(const std::string& name);
		size_t get_shader(const std::string& name) const;
		size_t get_texture(const std::string& name) const;
		size_t get_mesh(const std::string& name) const;
		std::shared_ptr<ktf::material> get_material(const std::string& name) const;
		void use_shader(const std::string& name) const;
		void use_texture(const std::string& name) const;
		void use_material(const std::string& name) const;

		void load_font(const std::string& path, const std::string& output, const std::vector<uint32_t>& chars);
		void unload_font();
		size_t create_string(const std::string& string, ktf::vec2<float>& size, size_t& num_lines, const std::vector<ktf::vec4<float>>& letter_colors);
		void remove_string(size_t* id);

		void load_atlas(const std::string& name, const std::string& path);
		void remove_atlas(const std::string& name);
		ktf::entry const * get_atlas_entry(const std::string& name, const std::string& id);

		void load_sprite(const std::string& id, const std::string& tex_group);
		sprite get_sprite(const std::string& id) const;
		void remove_sprite(const std::string& id);
};
    
} // namespace ktf
