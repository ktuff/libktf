#pragma once

#include <string>
#include <vector>

#include "../container/vector.hpp"
#include "../math/vec.hpp"

namespace ktf {

struct obj_object {
	std::string name;
	std::string mtl;
	ktf::vector<ktf::vec3<float>> pos;
	ktf::vector<ktf::vec2<float>> tex;
	ktf::vector<ktf::vec3<float>> nrm;
	ktf::vector<unsigned int> indices;
	bool smooth;
};

class obj {

	private:
		std::vector<ktf::obj_object> objects;


	public:
		obj(const std::string& path);
		~obj();


	public:
		size_t num_objects() const;
		ktf::obj_object& get_object(size_t index);
		ktf::obj_object* get_object(std::string const& name);

	private:
		void parse(const std::string& path);
};

} // namespace ktf
