#include <fstream>

#include "mtl_loader.hpp"

ktf::material_loader::material_loader(const std::string& path)
{
	this->parse(path);
}


size_t ktf::material_loader::get_num_materials() const
{
	return m_materials.size();
}

ktf::material const& ktf::material_loader::get_material(size_t index) const
{
	return m_materials[index];
}

void ktf::material_loader::parse(const std::string& path)
{
	// parse file to tokens
	ktf::vector<std::string> tokens;
	try {
		std::ifstream file;
		file.open(path);
		if (!file.is_open()) {
			return;
		}

		std::string content;
		file.seekg(0, std::ios::end);
		content.reserve(file.tellg());
		file.seekg(0, std::ios::beg);
		content.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
		file.close();

		for (size_t c=0; c<content.size(); ++c) {
			if (content[c] == '#') {
				// skip entire comment line
				while (c < content.size() && content[c] != '\n') {
					++c;
				}
			} else {
				std::string s;
				while (c < content.size() && content[c] != ' ' && content[c] != '\n') {
					s += content[c];
					++c;
				}
				tokens.push(s);
			}
		}
	} catch (std::exception& e) {
		return;
	}

	// create materials
	try {
		ktf::material dummy;
		ktf::material * mat = &dummy;
		for (unsigned int t=0; t<tokens.size();) {
			// TODO t in bounds
			const std::string& token = tokens[t];
			t += 1;
			if (token == "newmtl") {
				m_materials.push(ktf::material(tokens[t]));
				mat = &m_materials[m_materials.size()-1];
				t += 1;
			} else if (token == "Ns") {
				float exp = std::stod(tokens[t]);
				mat->m_specular_exponent = exp;
				t += 1;
			} else if (token == "Es") {
				float strength = std::stod(tokens[t]);
				mat->m_specular_strength = strength;
				t += 1;
			} else if (token == "Ka") {
				t += 3;
			} else if (token == "Kd") {
				float r = std::stod(tokens[t+0]), g = std::stod(tokens[t+1]), b = std::stod(tokens[t+2]);
				mat->m_diffuse_color = ktf::vec3<float>(r, g, b);
				t += 3;
			} else if (token == "Ks") {
				float r = std::stod(tokens[t+0]), g = std::stod(tokens[t+1]), b = std::stod(tokens[t+2]);
				mat->m_specular_color = ktf::vec3<float>(r, g, b);
				t += 3;
			} else if (token == "Ke") {
				t += 3;
			} else if (token == "Ni") {
				t += 1;
			} else if (token == "d") {
				float opacity = std::stod(tokens[t]);
				mat->m_opacity = opacity;
				t += 1;
			} else if (token == "Tr") {
				float transparency = std::stod(tokens[t]);
				mat->m_opacity = 1.0f - transparency;
				t += 1;
			} else if (token == "Tf") {
				t += 3;
			} else if (token == "illum") {
				t += 1;
			} else {
				continue;
			}
		}
	} catch (std::exception& e) {
		return;
	}
}
