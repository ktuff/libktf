#pragma once

#include "../container/vector.hpp"
#include "../opengl/material.hpp"

namespace ktf {

class material_loader {

	private:
		ktf::vector<ktf::material> m_materials;


	public:
		material_loader(const std::string& path);
		~material_loader() = default;


	public:
		size_t get_num_materials() const;
		ktf::material const& get_material(size_t index) const;

	private:
		void parse(const std::string& path);

};

} // namespace ktf
