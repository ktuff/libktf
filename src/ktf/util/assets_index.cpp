#include <assert.h>

#include "../opengl/composer.hpp"
#include "../opengl/material.hpp"
#include "assets_index.hpp"
#include "mtl_loader.hpp"
#include "obj_loader.hpp"

ktf::vec4<float> const ktf::assets_index::sprite_vertices[4] = {
    {-0.5f,  0.5f, 0.0f, 1.0f},
    { 0.5f, -0.5f, 1.0f, 0.0f},
    {-0.5f, -0.5f, 0.0f, 0.0f},
    { 0.5f,  0.5f, 1.0f, 1.0f}
};

unsigned int const ktf::assets_index::sprite_indices[6] = {
    0, 1, 2, 0, 3, 1
};

ktf::buffer_attrib const sprite_attrib = {
	0,
	0,
	4,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec4<float>),
	0,
	0
};
ktf::buffer_attrib const pos_attrib = {
	0,
	0,
	3,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec3<float>),
	0,
	0
};
ktf::buffer_attrib const tex_attrib = {
	1,
	1,
	2,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec2<float>),
	0,
	0
};
ktf::buffer_attrib const clr_attrib = {
	2,
	2,
	4,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec4<float>),
	0,
	0
};
ktf::buffer_attrib const nrm_attrib = {
	3,
	3,
	3,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec3<float>),
	0,
	0
};


ktf::assets_index::assets_index(const std::string& assets_folder, ktf::composer* composer)
: folder(assets_folder)
, composer(composer)
{
	if (folder == "") {
		this->folder = "./";
	} else if (assets_folder[assets_folder.size()-1] != '/') {
		this->folder = folder + "/";
	}
}

ktf::assets_index::~assets_index()
{
	this->clear();
}


void ktf::assets_index::clear()
{
	assert(composer != nullptr);

	for (auto& s : shader_map) {
		this->composer->deregister_shader(&s.second);
	}
	this->shader_map.clear();
	for (auto& t : texture_map) {
		this->composer->deregister_texture(&t.second);
	}
	this->texture_map.clear();
	for (auto& m : mesh_map) {
		this->composer->deregister_mesh(&m.second);
	}
	this->mesh_map.clear();
	this->material_map.clear();
	this->atlas_map.clear();
	this->sprites.clear();
	this->unload_font();
}

void ktf::assets_index::load_shader(const std::string& name, const std::string& path_vertex, const std::string& path_fragment)
{
    if (shader_map.find(name) != shader_map.end()) {
        return;
    }
    
    size_t id;
    this->composer->register_shader(&id, folder + path_vertex, folder + path_fragment);
    if (id == (size_t)-1) {
        // error
    } else {
        this->shader_map.emplace(name, id);
    }
}

void ktf::assets_index::load_texture(const std::string& name, const std::string& path)
{
    if (texture_map.find(name) != texture_map.end()) {
        return;
    }
    
    size_t id;
    this->composer->register_texture(&id, folder + path);
    if (id == (size_t)-1) {
        // error
    } else {
        this->texture_map.emplace(name, id);
    }
}

void ktf::assets_index::load_cube_texture(const std::string& name, const std::array<ktf::png, 6>& textures)
{
	size_t id;
	this->composer->register_cubemap(&id, textures);
	if (id == (size_t)-1) {
		// error
	} else {
		this->texture_map.emplace(name, id);
	}
}

void ktf::assets_index::load_cube_texture(const std::string& name, const std::array<std::string, 6>& paths)
{
	size_t id;
	std::array<ktf::png, 6> textures;
	for (size_t i=0; i<textures.size(); i++) {
		textures[i].load(folder + paths[i]);
	}
	this->composer->register_cubemap(&id, textures);
	if (id == (size_t)-1) {
		// error
	} else {
		this->texture_map.emplace(name, id);
	}
}

void ktf::assets_index::load_mesh(const std::string& path)
{
	ktf::obj obj(folder + path);
	if (obj.num_objects() < 1) {
		return;
	}

	size_t id;
	ktf::mesh3d mesh;
	for (size_t i=0; i<obj.num_objects(); i++) {
		ktf::obj_object& o = obj.get_object(i);
		mesh.pos = std::move(o.pos);
		mesh.tex = std::move(o.tex);
		mesh.nrm = std::move(o.nrm);
		mesh.indices = std::move(o.indices);
		std::vector<ktf::buffer_data> data = {
			{
				mesh.pos.size() * sizeof(ktf::vec4<float>),
				mesh.pos.data(),
				ktf::composer::draw_hint_static
			},
			{
				mesh.tex.size() * sizeof(ktf::vec2<float>),
				mesh.tex.data(),
				ktf::composer::draw_hint_static
			}
		};
		std::vector<ktf::buffer_attrib> attribs = {
			pos_attrib,
			tex_attrib
		};
		this->composer->register_mesh(&id, data, attribs, mesh.indices);
		if ((mesh_map.find(o.name) != mesh_map.end()) || (id == (size_t)-1)) {
			// error
		} else {
			this->mesh_map.emplace(o.name, id);
		}
	}
}

void ktf::assets_index::load_mesh(const std::string& name, const std::string& path)
{
	if (mesh_map.find(name) != mesh_map.end()) {
		return;
	}

	ktf::obj obj(folder + path);
	if (obj.num_objects() < 1) {
		return;
	}

	size_t id;
	const ktf::obj_object& o = obj.get_object(0);
	ktf::mesh3d mesh;
	mesh.pos = std::move(o.pos);
	mesh.tex = std::move(o.tex);
	mesh.nrm = std::move(o.nrm);
	mesh.indices = std::move(o.indices);
	std::vector<ktf::buffer_data> data = {
		{
			mesh.pos.size() * sizeof(ktf::vec4<float>),
			mesh.pos.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.tex.size() * sizeof(ktf::vec2<float>),
			mesh.tex.data(),
			ktf::composer::draw_hint_static
		}
	};
	std::vector<ktf::buffer_attrib> attribs = {
		pos_attrib,
		tex_attrib
	};
	this->composer->register_mesh(&id, data, attribs, mesh.indices);
	if (id == (size_t)-1) {
		// error
	} else {
		this->mesh_map.emplace(name, id);
	}
}

void ktf::assets_index::load_mesh(const std::string& name, const ktf::mesh2d& mesh)
{
	if (mesh_map.find(name) != mesh_map.end()) {
		return;
	}

	std::vector<ktf::buffer_data> data = {
		{
			mesh.pos.size() * sizeof(ktf::vec4<float>),
			mesh.pos.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.clr.size() * sizeof(ktf::vec4<float>),
			mesh.clr.data(),
			ktf::composer::draw_hint_static
		}
	};
	auto catt = clr_attrib;
	catt.attrib_index = catt.data_index = 1;
	std::vector<ktf::buffer_attrib> attribs = {
		sprite_attrib,
		catt
	};

	size_t id;
	this->composer->register_mesh(&id, data, attribs, mesh.indices);
	if (id == (size_t)-1) {
		// error
	} else {
		this->mesh_map.emplace(name, id);
	}
}

void ktf::assets_index::load_mesh(const std::string& name, const ktf::mesh3d& mesh)
{
	if (mesh_map.find(name) != mesh_map.end()) {
		return;
	}
	size_t id;

	std::vector<ktf::buffer_data> data = {
		{
			mesh.pos.size() * sizeof(ktf::vec4<float>),
			mesh.pos.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.tex.size() * sizeof(ktf::vec2<float>),
			mesh.tex.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.clr.size() * sizeof(ktf::vec4<float>),
			mesh.clr.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.nrm.size() * sizeof(ktf::vec3<float>),
			mesh.nrm.data(),
			ktf::composer::draw_hint_static
		}
	};
	std::vector<ktf::buffer_attrib> attribs = {
		pos_attrib,
		tex_attrib,
		clr_attrib,
		nrm_attrib
	};
    this->composer->register_mesh(&id, data, attribs, mesh.indices);

    if (id == (size_t)-1) {
        // error
    } else {
        this->mesh_map.emplace(name, id);
    }
}

void ktf::assets_index::load_material(const std::string& path)
{
	ktf::material_loader loader(folder + path);
	size_t num = loader.get_num_materials();
	for (size_t i=0; i<num; i++) {
		auto material = std::make_shared<ktf::material>(loader.get_material(i));
		if (material_map.find(material->m_name) == material_map.end()) {
			this->material_map.emplace(material->m_name, material);
		}
	}
}

void ktf::assets_index::remove_shader(const std::string& name)
{
    auto it = shader_map.find(name);
    if (it != shader_map.end()) {
        this->composer->deregister_shader(&it->second);
        this->shader_map.erase(it);
    } else {
        // error
    }
}

void ktf::assets_index::remove_texture(const std::string& name)
{
    auto it = texture_map.find(name);
    if (it != texture_map.end()) {
        this->composer->deregister_texture(&it->second);
        this->texture_map.erase(it);
    } else {
        // error
    }
}

void ktf::assets_index::remove_mesh(const std::string& name)
{
    auto it = mesh_map.find(name);
    if (it != mesh_map.end()) {
        this->composer->deregister_mesh(&it->second);
        this->mesh_map.erase(it);
    } else {
        // error
    }
}

void ktf::assets_index::remove_material(const std::string& name)
{
	auto it = material_map.find(name);
	if (it != material_map.end()) {
		this->material_map.erase(it);
	} else {
		// error
	}
}

size_t ktf::assets_index::get_shader(const std::string& name) const
{
    auto it = shader_map.find(name);
    if (it != shader_map.end()) {
        return it->second;
    } else {
        return (size_t)-1;
    }
}

size_t ktf::assets_index::get_texture(const std::string& name) const
{
    auto it = texture_map.find(name);
    if (it != texture_map.end()) {
        return it->second;
    } else {
        return (size_t)-1;
    }
}

size_t ktf::assets_index::get_mesh(const std::string& name) const
{
    auto it = mesh_map.find(name);
    if (it != mesh_map.end()) {
        return it->second;
    } else {
        return (size_t)-1;
    }
}

std::shared_ptr<ktf::material> ktf::assets_index::get_material(const std::string& name) const
{
	auto it = material_map.find(name);
    if (it != material_map.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

void ktf::assets_index::use_shader(const std::string& name) const
{
    auto it = shader_map.find(name);
    if (it != shader_map.end()) {
        this->composer->bind_shader(it->second);
    } else {
        // error
    }
}

void ktf::assets_index::use_texture(const std::string& name) const
{
    auto it = texture_map.find(name);
    if (it != texture_map.end()) {
        this->composer->bind_texture(it->second);
    } else {
        // error
    }
}

void ktf::assets_index::use_material(const std::string& name) const
{
	auto it = material_map.find(name);
	if (it != material_map.end()) {
		it->second->apply(this->composer);
	} else {
		// error
	}
}

void ktf::assets_index::load_font(const std::string& path, const std::string& output, const std::vector<uint32_t>& chars)
{
	this->composer->load_font(folder + path, ktf::vec2<uint32_t>(0, 96), output, chars);
}

void ktf::assets_index::unload_font()
{
	for (size_t id : string_set) {
		this->composer->deregister_mesh(&id);
	}
	this->string_set.clear();
	this->composer->unload_font();
}

size_t ktf::assets_index::create_string(const std::string& string, ktf::vec2<float>& size, size_t& num_lines, const std::vector<ktf::vec4<float>>& letter_colors)
{
	ktf::vector<ktf::vec4<float>> vertices;
	ktf::vector<uint32_t> indices;
	ktf::font const* font = composer->get_font();
	num_lines = font->generate(string, vertices, indices, size);
	size_t id;
	ktf::mesh2d mesh;
	mesh.pos = vertices;
	size_t min_letters = std::min(letter_colors.size(), vertices.size() / 4);
	for (size_t i=0; i<min_letters; i++) {
		mesh.clr.push(letter_colors[i]);
		mesh.clr.push(letter_colors[i]);
		mesh.clr.push(letter_colors[i]);
		mesh.clr.push(letter_colors[i]);
	}
	for (size_t i=min_letters*4; i<vertices.size(); i++) {
		mesh.clr.push(ktf::vec4<float>(1.0f));
	}
	mesh.indices = indices;

	std::vector<ktf::buffer_data> data = {
		{
			mesh.pos.size() * sizeof(ktf::vec4<float>),
			mesh.pos.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.clr.size() * sizeof(ktf::vec4<float>),
			mesh.clr.data(),
			ktf::composer::draw_hint_static
		}
	};
	auto catt = clr_attrib;
	catt.attrib_index = catt.data_index = 1;
	std::vector<ktf::buffer_attrib> attribs = {
		sprite_attrib,
		catt
	};
	this->composer->register_mesh(&id, data, attribs, mesh.indices);

	this->string_set.emplace(id);
	return id;
}

void ktf::assets_index::remove_string(size_t* id)
{
    if (string_set.find(*id) != string_set.end()) {
        this->string_set.erase(*id);
    }
    *id = (size_t)-1;
}

void ktf::assets_index::load_atlas(const std::string& name, const std::string& path)
{
    if (atlas_map.find(name) != atlas_map.end()) {
        return;
    }
    
    ktf::texture_atlas atlas;
    atlas.load(folder + path);
    this->atlas_map.emplace(name, std::move(atlas));
}

void ktf::assets_index::remove_atlas(const std::string& name)
{
    if (atlas_map.find(name) != atlas_map.end()) {
        atlas_map.erase(name);
    }
}

ktf::entry const * ktf::assets_index::get_atlas_entry(const std::string& name, const std::string& id)
{
    if (atlas_map.find(name) != atlas_map.end()) {
        const ktf::texture_atlas& atlas = atlas_map.at(name);
        if (atlas.contains(id)) {
            return atlas.get_entry(id);
        } else {
            return nullptr;
        }
    } else {
        return nullptr;
    }
}

void ktf::assets_index::load_sprite(const std::string& id, const std::string& tex_group)
{
	if (sprites.find(id) != sprites.end() && mesh_map.find(id) != mesh_map.end()) {
		return;
	}
	size_t id_ = (size_t)-1;
	float ratio = 1;
	ktf::mesh2d mesh;
	for (const auto& i : sprite_indices) {
		mesh.indices.push(i);
	}
	const auto& tex = atlas_map.find(tex_group);
	if (tex != atlas_map.end() && tex->second.contains(id)) {
		const ktf::entry* e = tex->second.get_entry(id);
		mesh.pos.push({sprite_vertices[0].x, sprite_vertices[0].y, float(e->uv[0].x), float(e->uv[1].y)});
		mesh.pos.push({sprite_vertices[1].x, sprite_vertices[1].y, float(e->uv[1].x), float(e->uv[0].y)});
		mesh.pos.push({sprite_vertices[2].x, sprite_vertices[2].y, float(e->uv[0].x), float(e->uv[0].y)});
		mesh.pos.push({sprite_vertices[3].x, sprite_vertices[3].y, float(e->uv[1].x), float(e->uv[1].y)});
		ratio = (double)e->dim[0]/(double)e->dim[1];
	} else {
		for (const auto& v : sprite_vertices) {
			mesh.pos.push(v);
		}
		ratio = 1;
	}
	for (size_t i=0; i<mesh.pos.size(); i++) {
		mesh.clr.push(ktf::vec4<float>(1.0f));
	}

	std::vector<ktf::buffer_data> data = {
		{
			mesh.pos.size() * sizeof(ktf::vec4<float>),
			mesh.pos.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh.clr.size() * sizeof(ktf::vec4<float>),
			mesh.clr.data(),
			ktf::composer::draw_hint_static
		}
	};
	auto catt = clr_attrib;
	catt.attrib_index = catt.data_index = 1;
	std::vector<ktf::buffer_attrib> attribs = {
		sprite_attrib,
		catt
	};
	this->composer->register_mesh(&id_, data, attribs, mesh.indices);

	this->mesh_map.emplace(id, id_);
	sprite sp = {id_, ratio};
	this->sprites.emplace(id, sp);
}

ktf::assets_index::sprite ktf::assets_index::get_sprite(const std::string& id) const
{
	auto it = sprites.find(id);
	if(it != sprites.end()) {
		return it->second;
	} else {
		return {(size_t)-1, 1.0};
	}
}

void ktf::assets_index::remove_sprite(const std::string& id)
{
	if (mesh_map.find(id) != mesh_map.end()) {
		size_t& s = mesh_map.at(id);
		if (s != (size_t)-1) {
			composer->deregister_mesh(&s);
		}
		mesh_map.erase(id);
	}
	if (sprites.find(id) != sprites.end()) {
		sprites.erase(id);
	}
}
