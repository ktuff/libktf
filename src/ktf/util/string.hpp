#pragma once

#include <string>
#include <vector>

namespace ktf {
	void split(std::vector<std::string>& cont, const std::string& str, const std::string& div);
	std::vector<uint32_t> to_utf32(const std::string& string);
	std::vector<char> to_utf8(const std::vector<uint32_t>& codepoints);
};
