#include <iostream>
#include <fstream>
#include <mutex>

#include "logger.hpp"

std::ofstream file = ([]() {
    return std::ofstream();
})();
std::mutex mutex;


void ktf::logger::open(const std::string& path)
{
	file.open(path);
	if (!file.is_open()) {
		ktf::logger::error("Could not open log file!");
	}
}

void ktf::logger::close()
{
	if (file.is_open()) {
		file.close();
	}
}

void ktf::logger::log(const std::string& string)
{
	std::string message = "[LOG]: " + string;
	std::cout << message << std::endl;
	std::lock_guard<std::mutex> guard(mutex);
	if (file.is_open()) {
		file << message << std::endl;
	}
}

void ktf::logger::info(const std::string& string)
{
	std::string message = "[INFO]: " + string;
	std::cout << message << std::endl;
	std::lock_guard<std::mutex> guard(mutex);
	if (file.is_open()) {
		file << message << std::endl;
	}
}

void ktf::logger::error(const std::string& string)
{
	std::string message = "[ERROR]: " + string;
	std::cerr << message << std::endl;
	std::lock_guard<std::mutex> guard(mutex);
	if (file.is_open()) {
		file << message << std::endl;
	}
}

void ktf::logger::debug(const std::string& string)
{
	std::string message = "[DEBUG]: " + string;
	std::cout << message << std::endl;
	std::lock_guard<std::mutex> guard(mutex);
	if (file.is_open()) {
		file << message << std::endl;
	}
}
