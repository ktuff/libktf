#pragma once

#include <sys/times.h>
#include <atomic>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

namespace ktf {

class proc_info {

	private:
		clock_t last_cpu_time;
		clock_t last_cpu_sys;
		clock_t last_cpu_usr;
		int num_cpus;
		double cpu_fraction;
		long long total_virt_mem;
		long long total_phys_mem;
		long long free_virt_mem;
		long long free_phys_mem;
		long long virt_mem;
		long long phys_mem;
		double cpu_usage;
		std::thread thread;
		std::atomic<bool> up;
		std::mutex m_mutex;


	public:
		proc_info();
		~proc_info();


	public:
		long long get_total_virt_mem() const;
		long long get_total_phys_mem() const;
		long long get_free_virt_mem();
		long long get_free_phys_mem();
		long long get_proc_virt_mem();
		long long get_proc_phys_mem();
		double get_cpu_usage();
		int get_cpu_count() const;

	private:
		long long parse_line(const std::string& line) const;
		long long read_memory_attrib(const std::string& attrib) const;
		void update();
};

} // namespace ktf
