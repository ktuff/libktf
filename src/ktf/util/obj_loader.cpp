#include <fstream>
#include <unordered_map>

#include "obj_loader.hpp"
#include "string.hpp"


ktf::obj::obj(const std::string& path)
{
	this->parse(path);
}

ktf::obj::~obj()
{
}


size_t ktf::obj::num_objects() const
{
	return objects.size();
}

ktf::obj_object & ktf::obj::get_object(size_t index)
{
	return objects.at(index);
}

ktf::obj_object * ktf::obj::get_object(const std::string& name)
{
	for (obj_object& object : objects) {
		if (object.name == name) {
			return &object;
		}
	}
	return nullptr;
}

void ktf::obj::parse(const std::string& path)
{
    // parse file to tokens
    ktf::vector<std::string> tokens;
    try {
        std::ifstream file;
        file.open(path);
        if (!file.is_open()) {
            return;
        }
        
        std::string content;
        file.seekg(0, std::ios::end);   
        content.reserve(file.tellg());
        file.seekg(0, std::ios::beg);
        content.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
        file.close();
        
        for (size_t c=0; c<content.size(); ++c) {
            if (content[c] == '#') {
                // skip entire comment line
                while (c < content.size() && content[c] != '\n') {
                    ++c;
                }
            } else {
                std::string s;
                while (c < content.size() && content[c] != ' ' && content[c] != '\n') {
                    s += content[c];
                    ++c;
                }
                tokens.push(s);
            }
        }
    } catch (std::exception& e) {
        return;
    }
    
    // sort tokens
    ktf::vector<std::string> mtllibs;
    ktf::vector<std::string> usemtls;
    ktf::vector<std::string> object_names;
    ktf::vector<bool> smooths;
    ktf::vector<vec3<double>> pos;
    ktf::vector<vec2<double>> tex;
    ktf::vector<vec3<double>> nrm;
    ktf::vector<std::string> faces;
    ktf::vector<unsigned int> face_offsets;
    try {
        for (unsigned int t=0; t<tokens.size();) {
            // TODO t in bounds
            const std::string& token = tokens[t++];
            if (token == "mtllib") {
                mtllibs.push(tokens[t++]);
            } else if (token == "o") {
                object_names.push(tokens[t++]);
                face_offsets.push(faces.size());
            } else if (token == "v") {
                float x = std::stod(tokens[t++]), y = std::stod(tokens[t++]), z = std::stod(tokens[t++]);
                pos.push({x, y, z});
            } else if (token == "vt") {
                float x = std::stod(tokens[t++]), y = std::stod(tokens[t++]);
                tex.push({x, y});
            } else if (token == "vn") {
                float x = std::stod(tokens[t++]), y = std::stod(tokens[t++]), z = std::stod(tokens[t++]);
                nrm.push({x, y, z});
            } else if (token == "f") {
                unsigned int vpf = 0;
                while (t<tokens.size() && tokens[t].find("/") != tokens[t].npos) {
                    faces.push(tokens[t++]);
                    vpf++;
                }
                if (vpf != 3) {
                    return;
                }
            } else if (token == "usemtl") {
                usemtls.push(tokens[t++]);
            } else if (token == "s") {
                smooths.push(tokens[t++] == "off" ? false : true);
            } else {
                return;
            }
        }
        face_offsets.push(faces.size());
        
        if (object_names.size() != smooths.size() && object_names.size() != usemtls.size()) {
            return;
        }
    } catch (std::exception& e) {
        return;
    }
    // not needed anymore
    tokens.clear();
    
    // create objects
    try {
        objects.clear();
        objects.resize(object_names.size());
        for (size_t i=0; i<object_names.size(); i++) {
            objects[i].name = object_names[i];
            objects[i].mtl = usemtls[i];
            objects[i].smooth = smooths[i];
            unsigned int next_index = 0;
            std::unordered_map<std::string, unsigned int> index_order;
            ktf::vector<std::string> st_verts;
            for (unsigned int f=face_offsets[i]; f<face_offsets[i+1]; f++) {
                const std::string& face = faces[f];
                if (index_order.find(face) == index_order.end()) {
                    index_order.emplace(face, next_index++);
                    st_verts.push(face);
                }
                objects[i].indices.push(index_order.at(face));
            }
            std::vector<std::string> splits;
            for (size_t f=0; f<st_verts.size(); f++) {
                // create vertices from strings
                ktf::split(splits, st_verts[f], "/");
                objects[i].pos.push(pos[std::stoi(splits[0]) - 1]);
                objects[i].tex.push(tex[std::stoi(splits[1]) - 1]);
                objects[i].nrm.push(nrm[std::stoi(splits[2]) - 1]);
                // TODO assert index in bounds
            }
        }
    } catch (std::exception& e) {
        objects.clear();
    }
}
