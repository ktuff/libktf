#pragma once

#include <string>

namespace ktf {
    
class logger {
        
    public:
        static void open(const std::string& path);
        static void close();
        
        static void log(const std::string& string);
        static void info(const std::string& string);
        static void error(const std::string& string);
        static void debug(const std::string& string);
        
};

} // namespace ktf
