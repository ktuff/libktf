#include <iconv.h>
#include <string.h>
#include <memory>

#include "string.hpp"

// NOTE
// improvement: handling of 0xfeff and 0xfffe
unsigned long const bom_size = sizeof(uint32_t);


void ktf::split(std::vector<std::string>& cont, const std::string& str, const std::string& div)
{
	std::string line = str;
	unsigned int lt = 0;
	std::string::size_type x = 0;
	cont.clear();
	while (x != std::string::npos) {
		x = line.find(div, lt);
		if (x != std::string::npos) {
			cont.push_back(line.substr(lt, x-lt));
			lt = x+div.size();
		}
	}
	cont.push_back(line.substr(lt, line.size()-lt));
}

size_t convert_charset(char** input, unsigned long* plenin, char** output, unsigned long* plenout, char const* in, char const* out)
{
	iconv_t cd = ::iconv_open(out, in);
	if (cd == (iconv_t)-1) {
		return (size_t)-1;
	} else {
		size_t rc = ::iconv(cd, input, plenin, output, plenout);
		::iconv_close(cd);
		return rc;
	}
}

std::vector<uint32_t> ktf::to_utf32(const std::string& string)
{
	if (string.empty()) {
		return std::vector<uint32_t>();
	}

	unsigned long input_size = string.size();
	unsigned long output_size = input_size * 4 + bom_size; // UTF-8 => UTF-32 : max. factor 4 + buffer for BOM
	unsigned long buffer_out = output_size;
	std::unique_ptr<char[]> input = std::make_unique<char[]>(input_size);
	std::unique_ptr<char[]> output = std::make_unique<char[]>(output_size);
	char* pin = input.get();
	char* pout = output.get();
	::memcpy(pin, string.data(), string.size());
	::memset(pout, '\0', output_size);
	size_t chars = ::convert_charset(&pin, &input_size, &pout, &output_size, "UTF-8", "UTF-32");
	// return empty vector on error
	if (chars == (size_t)-1) {
		return std::vector<uint32_t>();
	}

	std::vector<uint32_t> unicode_chars;
	unicode_chars.resize((buffer_out - output_size - bom_size) / sizeof(uint32_t));
	::memcpy(unicode_chars.data(), output.get() + bom_size, buffer_out - output_size - bom_size);

	return unicode_chars;
}

std::vector<char> ktf::to_utf8(const std::vector<uint32_t>& codepoints)
{
	if (codepoints.empty()) {
		return std::vector<char>();
	}

	// no BOM generated
	unsigned long input_size = codepoints.size() * sizeof(uint32_t);
	unsigned long output_size = input_size;
	unsigned long buffer_size = output_size;
	std::unique_ptr<char[]> input = std::make_unique<char[]>(input_size);
	std::unique_ptr<char[]> output = std::make_unique<char[]>(output_size);
	char* pin = input.get();
	char* pout = output.get();
	::memcpy(pin, codepoints.data(), input_size);
	::memset(pout, '\0', output_size);
	size_t chars = ::convert_charset(&pin, &input_size, &pout, &output_size, "UTF-32", "UTF-8");

	// skip on error
	std::vector<char> utf8;
	if (chars != (size_t)-1) {
		utf8.resize(buffer_size - output_size);
		::memcpy(utf8.data(), output.get(), buffer_size - output_size);
	}
	return utf8;
}
