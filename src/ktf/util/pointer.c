#include "pointer.h"

const void* cptr(const void* p, long offset)
{
    return &((const char*)p)[offset];
}

void* ptr(void* p, long offset)
{
    return &((char*)p)[offset];
}
