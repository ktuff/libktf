#ifndef KTF_POINTER_H
#define KTF_POINTER_H

#ifdef __cplusplus
extern "C" {
#endif

const void* cptr(const void* p, long offset);
void* ptr(void* p, long offset);

#ifdef __cplusplus
}
#endif

#endif
