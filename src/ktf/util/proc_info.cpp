#include <sys/sysinfo.h>
#include <fstream>

#include "proc_info.hpp"

namespace ktf {

proc_info::proc_info()
: free_virt_mem(-1)
, free_phys_mem(-1)
, virt_mem(-1)
, phys_mem(-1)
, cpu_usage(0.0f)
, up(true)
{
	std::ifstream cpuinfo("/proc/cpuinfo");
	int num = 0;
	if (cpuinfo.is_open()) {
		std::string line;
		while (std::getline(cpuinfo, line)) {
			if (line.find("processor") != line.npos) {
				num++;
			}
		}
		cpuinfo.close();
	}
	this->num_cpus = std::max(num, 1);
	this->cpu_fraction = 1.0f / num_cpus;

	struct tms cpu_time_info;
	this->last_cpu_time = times(&cpu_time_info);
	this->last_cpu_sys = cpu_time_info.tms_stime;
	this->last_cpu_usr = cpu_time_info.tms_utime;

	struct sysinfo mem_info;
	sysinfo(&mem_info);
	this->total_virt_mem = (mem_info.totalram + mem_info.totalswap) * mem_info.mem_unit;
	this->total_phys_mem = (mem_info.totalram) * mem_info.mem_unit;

	this->thread = std::thread([=] {
		while (up) {
			this->update();
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	});
}

proc_info::~proc_info()
{
	this->up = false;
	this->thread.join();
}


long long proc_info::get_total_virt_mem() const
{
	return total_virt_mem;
}

long long proc_info::get_total_phys_mem() const
{
	return total_phys_mem;
}

long long proc_info::get_free_virt_mem()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return free_virt_mem;
}

long long proc_info::get_free_phys_mem()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return free_phys_mem;
}

long long proc_info::get_proc_virt_mem()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return virt_mem;
}

long long proc_info::get_proc_phys_mem()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return phys_mem;
}

double proc_info::get_cpu_usage()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return cpu_usage;
}

int proc_info::get_cpu_count() const
{
	return num_cpus;
}

long long proc_info::parse_line(const std::string& line) const
{
	const std::string div(" ");
	std::vector<std::string> substrings;
	unsigned int lt = 0;
	std::string::size_type x = 0;
	while (x != std::string::npos) {
		x = line.find(div, lt);
		if (x != std::string::npos) {
			substrings.push_back(line.substr(lt, x - lt));
			lt = x + div.size();
		}
	}
	substrings.push_back(line.substr(lt, line.size() - lt));

	long long mem = -1;
	for (const std::string& s : substrings) {
		if (s.empty()) {
			continue;
		}

		bool is_number = true;
		for (char c : s) {
			if (!std::isdigit(c)) {
				is_number = false;
				break;
			}
		}
		if (is_number) {
			mem = std::stoll(s);
			break;
		}
	}

	return mem;
}

long long proc_info::read_memory_attrib(const std::string& attrib) const
{
	long long mem = -1;
	std::ifstream status("/proc/self/status");
	if (status.is_open()) {
		std::string line;
		while (getline(status, line)) {
			if (line.find(attrib) != line.npos && line.find("kB") != line.npos) {
				mem = parse_line(line);
				break;
			}
		}
		status.close();
	}
	return mem;
}

void proc_info::update()
{
	std::lock_guard<std::mutex> guard(m_mutex);

	// MEMORY
	struct sysinfo mem_info;
	::sysinfo(&mem_info);
	this->free_virt_mem = (mem_info.freeram + mem_info.freeswap) * mem_info.mem_unit;
	this->free_phys_mem = (mem_info.freeram) * mem_info.mem_unit;
	this->virt_mem = read_memory_attrib("VmSize:");
	this->phys_mem = read_memory_attrib("VmRSS:");

	// CPU
	struct tms cpu_time_info;
	clock_t cpu_time = times(&cpu_time_info);
	clock_t cpu_sys = cpu_time_info.tms_stime;
	clock_t cpu_usr = cpu_time_info.tms_utime;

	double usage = -1.0f;
	if ((cpu_time > last_cpu_time) && (cpu_sys >= last_cpu_sys) && (cpu_usr >= last_cpu_usr)) {
		usage = ((cpu_sys - last_cpu_sys) + (cpu_usr - last_cpu_usr)) / (double)(cpu_time - last_cpu_time);
		usage *= cpu_fraction;
		usage *= 100.0f;
		this->cpu_usage = usage;
	}
	this->last_cpu_time = cpu_time;
	this->last_cpu_sys = cpu_sys;
	this->last_cpu_usr = cpu_usr;
}

} // namespace ktf
