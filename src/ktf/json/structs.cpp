#include "structs.hpp"


json::basic_json::basic_json(json::basic_json::type t)
: m_type(t)
{
}

json::basic_json::~basic_json()
{
}

std::string json::basic_json::dump() const
{
	return "null";
}

bool json::basic_json::to_bool() const
{
	return false;
}

int json::basic_json::to_int() const
{
	return 0;
}

float json::basic_json::to_float() const
{
	return 0.0;
}

std::string json::basic_json::to_string() const
{
	return std::string();
}

json::array* json::basic_json::to_array()
{
	return nullptr;
}

json::object* json::basic_json::to_object()
{
	return nullptr;
}

bool json::basic_json::is_null() const
{
	return m_type == null_t;
}

bool json::basic_json::is_boolean() const
{
	return m_type == boolean_t;
}

bool json::basic_json::is_float() const
{
	return m_type == float_t;
}

bool json::basic_json::is_int() const
{
	return m_type == integer_t;
}

bool json::basic_json::is_number() const
{
	return is_int() || is_float();
}

bool json::basic_json::is_string() const
{
	return m_type == string_t;
}

bool json::basic_json::is_object() const
{
	return m_type == object_t;
}

bool json::basic_json::is_array() const
{
	return m_type == array_t;
}


json::boolean::boolean(bool value)
: basic_json(boolean_t)
, value(value)
{
}

std::string json::boolean::dump() const
{
	return std::to_string(value);
}

bool json::boolean::to_bool() const
{
	return value;
}


json::number::number(type t)
: basic_json(t)
{
}


json::number_int::number_int(int value)
: number(integer_t)
, value(value)
{
}

std::string json::number_int::dump() const
{
	return std::to_string(value);
}

int json::number_int::to_int() const
{
	return value;
}


json::number_float::number_float(double value)
: number(float_t)
, value(value)
{
}

std::string json::number_float::dump() const
{
	return std::to_string(value);
}

float json::number_float::to_float() const
{
	return value;
}


json::string::string(std::string const& s)
: basic_json(string_t)
, value(s)
{
}

std::string json::string::dump() const
{
	return "\"" + value + "\"";
}

std::string json::string::to_string() const
{
	return value;
}


json::array::array()
: basic_json(array_t)
{
}

std::string json::array::dump() const
{
	std::string value;
	value += "[";
	size_t i=0;
	for (auto& e : *this) {
		value += e->dump();
		if (i<size()-1) {
			value += ",";
		}
		i++;
	}
	value += "]";
	return value;
}

json::array* json::array::to_array()
{
	return this;
}


json::object::object()
: basic_json(object_t)
{
}

std::string json::object::dump() const
{
	std::string value;
	value += "{";
	size_t i = 0;
	for (auto& e : *this) {
		value += "\"" + e.first + "\":" + e.second->dump();
		if (i<size()-1) {
			value += ",";
		}
		i++;
	}
	value += "}";
	return value;
}

json::object* json::object::to_object()
{
	return this;
}
