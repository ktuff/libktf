#pragma once

#include "structs.hpp"

namespace json {

json::json_ptr parse(std::string const& path) noexcept;

} // namespace json
