#pragma once


#include <memory>
#include <string>
#include <unordered_map>
#include <vector>


namespace json {

struct array;
struct basic_json;
struct object;

typedef std::shared_ptr<json::basic_json> json_ptr;


struct basic_json {

	public:
		enum type {
			array_t,
			object_t,
			string_t,
			boolean_t,
			integer_t,
			float_t,
			null_t,
		} const m_type;


	public:
		basic_json(type t = null_t);
		virtual ~basic_json();


	public:
		virtual std::string dump() const;

		virtual bool to_bool() const;
		virtual int to_int() const;
		virtual float to_float() const;
		virtual std::string to_string() const;
		virtual array* to_array();
		virtual object* to_object();

		bool is_null() const;
		bool is_array() const;
		bool is_object() const;
		bool is_number() const;
		bool is_float() const;
		bool is_int() const;
		bool is_string() const;
		bool is_boolean() const;
};

struct boolean : public basic_json {
	bool value;

	boolean(bool value = false);

	std::string dump() const override;
	bool to_bool() const override;
};

struct number : public basic_json {
	number(type t);
};

struct number_int : public number {
	int value;

	number_int(int value);

	std::string dump() const override;
	int to_int() const override;
};

struct number_float : public number {
	double value;

	number_float(double value);

	std::string dump() const override;
	float to_float() const override;
};

struct string : public basic_json {
	std::string value;

	string(std::string const& s = "");

	std::string dump() const override;
	std::string to_string() const override;
};

struct array : public basic_json, std::vector<json_ptr> {
	array();

	std::string dump() const override;
	array* to_array() override;
};

struct object : public basic_json, std::unordered_map<std::string, json_ptr> {
	object();

	std::string dump() const override;
	object* to_object() override;
};

} // namespace json
