#include <cmath>
#include <fstream>

#include "parse.hpp"


namespace json {

struct token {

	enum token_t {
		COLON,
		COMMA,
		LBRACE,
		RBRACE,
		LBRACKET,
		RBRACKET,
		INTEGER,
		FRACTION,
		EXPONENT,
		STRING,
		JSON_TRUE,
		JSON_FALSE,
		JSON_NULL
	} m_type;
	std::string m_text;

	token(token_t type, std::string const& text);
};


json::json_ptr parse_object(std::vector<token> const& tokens, size_t& index) noexcept;
json::json_ptr parse_array(std::vector<token> const& tokens, size_t& index) noexcept;
std::pair<std::string, json::json_ptr> parse_member(std::vector<token> const& tokens, size_t& index) noexcept;
json::json_ptr parse_value(std::vector<token> const& tokens, size_t& index) noexcept;

} // namespace json



json::token::token(json::token::token_t type, const std::string& text)
: m_type(type)
, m_text(text)
{
}


#define JSON_ASSERT(expr, error_value)\
({\
	if (!(expr)) {\
		::printf("Failure while parsing, using default value!\n");\
		return error_value;\
	}\
})

#include <iostream>
json::json_ptr json::parse(std::string const& path) noexcept
{
	std::ifstream file;
	file.open(path);
	if (!file.is_open()) {
		return nullptr;
	}
	file.seekg(0, std::ios::end);
	std::vector<char> chars(file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(chars.data(), chars.size());
	file.close();

	std::vector<token> tokens;
	size_t const num_tokens = chars.size();
	for (size_t index=0; index<num_tokens; index++) {
		switch (chars[index]) {
		case ':':
			tokens.push_back({token::COLON, ":"});
			break;
		case ',':
			tokens.push_back({token::COMMA, ","});
			break;
		case '{':
			tokens.push_back({token::LBRACE, "{"});
			break;
		case '}':
			tokens.push_back({token::RBRACE, "}"});
			break;
		case '[':
			tokens.push_back({token::LBRACKET, "["});
			break;
		case ']':
			tokens.push_back({token::RBRACKET, "]"});
			break;
		case '\t':
		case '\n':
		case '\r':
		case ' ':
			break;
		case '.': {
			size_t start = index;
			while (index+1 < num_tokens && std::isdigit(chars[index+1])) {
				index++;
			}
			size_t end = index;
			if (end != start) {
				tokens.push_back({token::FRACTION, std::string(&chars[start], end-start+1)});
			}
			break;
		}
		case 'E':
		case 'e': {
			size_t start = index;
			index++;
			if (index+1 < num_tokens && (chars[index+1] == '-' || chars[index+1] == '+')) {
				index++;
			}
			size_t mid = index;
			while (index+1 < num_tokens && std::isdigit(chars[index+1])) {
				index++;
			}
			size_t end = index;
			if (end != mid) {
				tokens.push_back({token::EXPONENT, std::string(&chars[start], end-start+1)});
			}
			break;
		}
		case '"': {
			index++;
			size_t start = index;
			while (index < num_tokens && (chars[index-1] == '\\' || chars[index] != '"')) {
				index++;
			}
			size_t end = index;
			tokens.push_back({token::STRING, std::string(&chars[start], end - start)});
			break;
		}
		default: {
			if (std::isdigit(chars[index]) && chars[index] == '0') {
				tokens.push_back({token::INTEGER, "0"});
			} else if (std::isdigit(chars[index]) && chars[index] != '0') {
				size_t start = index;
				while (index+1 < num_tokens && std::isdigit(chars[index+1])) {
					index++;
				}
				size_t end = index;
				tokens.push_back({token::INTEGER, std::string(&chars[start], end-start+1)});
			} else if (index+4 < num_tokens && chars[index] == 't' && chars[index+1] == 'r' && chars[index+2] == 'u' && chars[index+3] == 'e') {
				tokens.push_back({token::JSON_TRUE, "true"});
				index += 3;
			} else if (index+5 < num_tokens && chars[index] == 'f' && chars[index+1] == 'a' && chars[index+2] == 'l' && chars[index+3] == 's' && chars[index+4] == 'e') {
				tokens.push_back({token::JSON_FALSE, "false"});
				index += 4;
			} else if (index+4 < num_tokens && chars[index] == 'n' && chars[index+1] == 'u' && chars[index+2] == 'l' && chars[index+3] == 'l') {
				tokens.push_back({token::JSON_NULL, "null"});
				index += 3;
			} else {
				::printf("Unexpected char: %c at index %lu!\n > %s ...", chars[index], index, std::string(chars[index], 20).c_str());
				return std::make_shared<json::basic_json>();
			}
		}
		}
	}

	size_t index = 0;
	return parse_value(tokens, index);
}


json::json_ptr json::parse_object(std::vector<token> const& tokens, size_t& index) noexcept
{
	auto js = std::make_shared<json::object>();
	JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::LBRACE, js);
	index++;

	if (index < tokens.size() && tokens[index].m_type != token::RBRACE) {
		auto member = parse_member(tokens, index);
		js->emplace(member.first, member.second);
		while (index < tokens.size() && tokens[index].m_type != token::RBRACE) {
			JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::COMMA, js);
			index++;
			auto member = parse_member(tokens, index);
			js->emplace(member.first, member.second);
		}
	}

	JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::RBRACE, js);
	index++;
	return js;
}

json::json_ptr json::parse_array(std::vector<token> const& tokens, size_t& index) noexcept
{
	auto js = std::make_shared<json::array>();
	JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::LBRACKET, js);
	index++;

	if (index < tokens.size() && tokens[index].m_type != token::RBRACKET) {
		js->push_back(parse_value(tokens, index));
		while (index < tokens.size() && tokens[index].m_type != token::RBRACKET) {
			JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::COMMA, js);
			index++;
			js->push_back(parse_value(tokens, index));
		}
	}

	JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::RBRACKET, js);
	index++;
	return js;
}

std::pair<std::string, json::json_ptr> json::parse_member(const std::vector<token>& tokens, size_t& index) noexcept
{
	auto js = std::make_pair("", std::make_shared<json::basic_json>());
	JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::STRING, js);
	std::string name = tokens[index].m_text;
	index++;
	JSON_ASSERT(index < tokens.size() && tokens[index].m_type == token::COLON, js);
	index++;
	json::json_ptr value = parse_value(tokens, index);
	return {name, value};
}

json::json_ptr json::parse_value(std::vector<token> const& tokens, size_t& index) noexcept
{
	json::json_ptr value = std::make_shared<json::basic_json>();
	if (index >= tokens.size()) {
		return value;
	}
	switch (tokens[index].m_type) {
	case token::LBRACE:
		value = parse_object(tokens, index);
		break;
	case token::LBRACKET:
		value = parse_array(tokens, index);
		break;
	case token::INTEGER: {
		std::string number_str = tokens[index].m_text;
		index++;
		if (index < tokens.size() && tokens[index].m_type == token::FRACTION) {
			number_str += tokens[index].m_text;
			index++;
			double number = std::stod(number_str);
			if (index < tokens.size() && tokens[index].m_type == token::EXPONENT) {
				number *= std::pow(10.0, std::stoi(tokens[index].m_text));
				index++;
			}
			value = std::make_shared<json::number_float>(number);
		} else {
			int number = std::stoi(number_str);
			if (index < tokens.size() && tokens[index].m_type == token::EXPONENT) {
				number *= std::pow(10, std::stoi(tokens[index].m_text));
				index++;
			}
			value = std::make_shared<json::number_int>(number);
		}
		break;
	}
	case token::STRING:
		value = std::make_shared<json::string>(tokens[index].m_text);
		index++;
		break;
	case token::JSON_TRUE:
		value = std::make_shared<json::boolean>(true);
		index++;
		break;
	case token::JSON_FALSE:
		value = std::make_shared<json::boolean>(false);
		index++;
		break;
	case token::JSON_NULL:
		value = std::make_shared<json::basic_json>();
		index++;
		break;
	default:
		break;
	}
	return value;
}
