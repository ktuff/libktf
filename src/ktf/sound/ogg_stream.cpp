#include <string.h>
#include <thread>
#include "ogg_stream.hpp"

#define OGG_PACKET_SIZE 4096
#define OGG_BUFFER_SIZE 16384

ktf::ogg_stream::ogg_stream(const std::string& path)
: audio_stream()
, size(0)
, current_section(0)
{
    this->file.open(path, std::ios_base::binary);
    if (!file.is_open()) {
        this->failed = true;
		return;
    } else {
        this->file.seekg(0, std::ios_base::end);
        this->size = file.tellg();
        this->file.seekg(0, std::ios_base::beg);
    }
    
    ov_callbacks ogg_callbacks;
    ogg_callbacks.read_func = read_ogg;
    ogg_callbacks.close_func = NULL;
    ogg_callbacks.seek_func = seek_ogg;
    ogg_callbacks.tell_func = tell_ogg;
    if (::ov_open_callbacks(this, &vf, NULL, 0, ogg_callbacks) < 0) {
        ::printf("Failed ov_open_callbacks\n");
        this->file.close();
        ::ov_clear(&vf);
        this->failed = true;
        return;
    }
    
    this->m_duration = ::ov_time_total(&vf, -1);
    vorbis_info* vinfo = ::ov_info(&vf, -1);
    this->m_sample_rate = vinfo->rate;
    int channels = vinfo->channels;
    if (channels == 1) {
        this->format = AL_FORMAT_MONO16;
    } else if (channels == 2) {
        this->format = AL_FORMAT_STEREO16;
    } else {
        ::printf("Invalid number of channels: %d", channels);
    }
    
    this->init_buffers();
    this->failed = false;
}

ktf::ogg_stream::~ogg_stream()
{
    if (!failed) {
        ::ov_clear(&vf);
    }
    this->file.close();

    ::alDeleteBuffers(NUM_BUFFERS, &buffers[0]);
	::alDeleteSources(1, &source);
}


void ktf::ogg_stream::update()
{
	ALint proc_buffers = 0;
	::alGetSourcei(source, AL_BUFFERS_PROCESSED, &proc_buffers);
	for (int i=0; i<proc_buffers; i++) {
		ALuint buffer;
		::alSourceUnqueueBuffers(source, 1, &buffer);
		buffer_queue.push(buffer);
	}

	int limiter = NUM_BUFFERS;
	while (!buffer_queue.empty() && limiter > 0) {
		this->fill_buffer();
		limiter--;
	}
}

size_t ktf::ogg_stream::read_ogg(void* destination, size_t size1, size_t size2, void* datasource)
{
    ktf::ogg_stream* ogg = reinterpret_cast<ktf::ogg_stream*>(datasource);
    ALsizei length = size1 * size2;
    
    if (ogg->m_progress + length > ogg->size) {
        length = ogg->size - ogg->m_progress;
    }

    size_t d_size = sizeof(char)*length;
    char *data = (char*)::malloc(d_size);
    ::memset(data, '\0', d_size);
    ogg->file.seekg(ogg->m_progress, std::ios_base::beg);
    if (ogg->m_progress < ogg->size) {
        if (!ogg->file.read(&data[0], length)) {
            ::printf("Error while reading file\n");
        }
    }
    ogg->m_progress += length;
    ::memcpy(destination, &data[0], d_size);

    ::free(data);
    return length;
}

int ktf::ogg_stream::seek_ogg(void* datasource, ogg_int64_t offset, int base)
{
    ktf::ogg_stream* fp = reinterpret_cast<ktf::ogg_stream*>(datasource);
    switch (base) {
        case SEEK_CUR:
            fp->m_progress += offset;
            break;
        case SEEK_END:
            fp->m_progress = fp->size - offset;
            break;
        case SEEK_SET:
            fp->m_progress = offset;
            break;
        default:
            return -1;
    }
    if (fp->m_progress > fp->size) {
        fp->m_progress = fp->size;
        return -1;
    }
    if (fp->m_progress < 0) {
        fp->m_progress = 0;
        return -1;
    }
    return 0;
}

long ktf::ogg_stream::tell_ogg(void* datasource)
{
    return reinterpret_cast<ktf::ogg_stream*>(datasource)->file.tellg();
}

void ktf::ogg_stream::init_buffers()
{
	// fill and queue all buffers
	for (int b=0; b<NUM_BUFFERS; b++) {
		this->fill_buffer();
	}
}

void ktf::ogg_stream::reset_buffers()
{
	this->current_section = 0;
	this->clear_buffer_queue();
}

bool ktf::ogg_stream::fill_buffer()
{
	if (buffer_queue.empty()) {
		return false;
	}

	ALsizei read_data = 0;
    char data[OGG_BUFFER_SIZE];
	int length = OGG_PACKET_SIZE;
	::memset(data, '\0', OGG_BUFFER_SIZE);
	for (long read = 1; read_data < OGG_BUFFER_SIZE && read; read_data += read) {
		length = std::min(OGG_BUFFER_SIZE - read_data, length);
		read = ::ov_read(&vf, &data[read_data], length, 0, 2, 1, &current_section);
	}
	if (read_data == 0) {
		return false;
	}

	ALuint buffer = buffer_queue.front();
	::alBufferData(buffer, format, data, read_data, m_sample_rate);
	if (alGetError() == AL_NO_ERROR) {
		::alSourceQueueBuffers(source, 1, &buffer);
		this->buffer_queue.pop();
	}

	if (m_loop && m_progress >= size) {
		this->m_progress = 0;
		this->file.seekg(std::ios_base::beg);
	}
	return true;
}
