#include <AL/alc.h>
#include <vector>
#include "soundsystem.hpp"
#ifdef ENABLE_FFMPEG
	#include "ffmpeg_stream.hpp"
#else
	#include "ogg_stream.hpp"
#endif

ktf::soundsystem::soundsystem()
: device(nullptr)
, context(nullptr)
, created(false)
, running(false)
, next_id(1)
, enumeration_support(false)
, master_volume(1.0f)
, rolloff_factor(1.0f)
, max_distance(30.0f)
, reference_distance(1.0f)
, min_gain(0.0f)
, max_gain(1.0f)
{
}

ktf::soundsystem::~soundsystem()
{
    this->destroy();
}


void ktf::soundsystem::create()
{
    if (!created) {        
        // create OpenAL-Device
        this->device = ::alcOpenDevice(NULL);
        if (!device) {
            return;
        }
    
        // check enumeration support
        ALboolean enumeration = ::alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
        if (enumeration == AL_FALSE) {
            this->enumeration_support = false;
        } else {
            this->enumeration_support = true;
        }
    
        // create OpenAL-Context
        this->context = ::alcCreateContext(device, nullptr);
        if (!context) {
            ::alcCloseDevice(device);
            this->device = nullptr;
            return;
        }
        if (!::alcMakeContextCurrent(context)) {
            ::alcDestroyContext(context);
            this->context = nullptr;
            ::alcCloseDevice(device);
            this->device = nullptr;
            return;
        }
        
        this->set_listener_position(0.0f, 0.0f, 0.0f);
        this->set_listener_velocity(0.0f, 0.0f, 0.0f);
        this->set_listener_orientation(0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);
        
        this->created = true;
    }
}

void ktf::soundsystem::destroy()
{
    if (created) {        
        this->stop();
        this->streams.clear();

        ::alcMakeContextCurrent(nullptr);
        if (context) {
            ::alcDestroyContext(context);
            this->context = nullptr;
        }
        if (device) {
            ALCboolean rc = ::alcCloseDevice(device);
            if (!rc) {
                // could not destroy device
            } else {
                this->device = nullptr;
            }
        }
        this->created = false;
    }
}

bool ktf::soundsystem::is_created() const
{
    return created;
}

bool ktf::soundsystem::is_running() const
{
    return running;
}

bool ktf::soundsystem::is_enumeration_supported() const
{
    return enumeration_support;
}

std::vector<std::string> ktf::soundsystem::get_device_list() const
{
    if (created) {
        std::vector<std::string> device_list;
        const ALCchar* devices = ::alcGetString(NULL, ALC_DEVICE_SPECIFIER);
        const ALCchar *device_c = devices, *next = devices+1;
        size_t len = 0;
        while (device_c && *device_c != '\0' && next && *next != '\0') {
            std::string device_name(device_c);
            device_list.push_back(device_name);
            len = device_name.length();
            device_c += (len + 1);
            next += (len + 2);
        }
        return device_list;
    } else {
        return std::vector<std::string>();
    }
}

void ktf::soundsystem::start()
{
    if (created && !running) {
        this->running = true;
        this->thread = std::thread([=]() {
            this->run();
        });
    }
}

void ktf::soundsystem::stop()
{
    if (created && running) {
        this->running = false;
        this->thread.join();
    }
}

void ktf::soundsystem::clear()
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        this->streams.clear();
    }
}

void ktf::soundsystem::set_listener_position(float x, float y, float z) const
{
    if (created) {
        ::alListener3f(AL_POSITION, x, y, z);
    }
}

void ktf::soundsystem::set_listener_velocity(float x, float y, float z) const
{
    if (created) {
        ::alListener3f(AL_VELOCITY, x, y, z);
    }
}

void ktf::soundsystem::set_listener_orientation(float dir_x, float dir_y, float dir_z, float up_x, float up_y, float up_z) const
{
    if (created) {
        float listener_orientation[] = {
            dir_x, dir_y, dir_z, up_x, up_y, up_z
        };
        ::alListenerfv(AL_ORIENTATION, listener_orientation);
    }
}

void ktf::soundsystem::play(int64_t& id, const std::string& path, float gain, float pitch, bool loop)
{
    if (running) {
        id = next_id++;
#ifdef ENABLE_FFMPEG
		auto stream = std::make_shared<ffmpeg_stream>(path);
#else
        auto stream = std::make_shared<ogg_stream>(path);
#endif
        if (!stream->fail()) {
            std::lock_guard<std::mutex> guard(mutex);
            this->streams.emplace(id, stream);
            stream->set_gain(gain, master_volume);
            stream->set_pitch(pitch);
            stream->set_loop(loop);
            stream->set_rolloff_factor(rolloff_factor);
            stream->set_max_distance(max_distance);
            stream->set_reference_dist(reference_distance);
            stream->set_min_gain(min_gain);
            stream->set_max_gain(max_gain);
            stream->set_source_relative(true);
            stream->set_position(0.0f, 0.0f, 0.0f);
            stream->play();
        }
    }
}

void ktf::soundsystem::play(int64_t& id, const std::string& path, float gain, float pitch, bool loop, float x, float y, float z)
{
    if (running) {
        id = next_id++;
#ifdef ENABLE_FFMPEG
		auto stream = std::make_shared<ffmpeg_stream>(path);
#else
        auto stream = std::make_shared<ogg_stream>(path);
#endif
        if (!stream->fail()) {
            std::lock_guard<std::mutex> guard(mutex);
            this->streams.emplace(id, stream);
            stream->set_gain(gain, master_volume);
            stream->set_pitch(pitch);
            stream->set_loop(loop);
            stream->set_rolloff_factor(rolloff_factor);
            stream->set_max_distance(max_distance);
            stream->set_reference_dist(reference_distance);
            stream->set_min_gain(min_gain);
            stream->set_max_gain(max_gain);
            stream->set_source_relative(false);
            stream->set_position(x, y, z);
            stream->play();
        }
    }
}

void ktf::soundsystem::stop(int64_t& id)
{
    if (running) {
        {
            std::lock_guard<std::mutex> guard(mutex);
            if (streams.find(id) != streams.end()) {
                this->streams.erase(id);
            }
        }
    }
}

bool ktf::soundsystem::is_playing(int64_t id)
{
    if (running) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            return streams.at(id)->is_playing();
        }
    }
    return false;
}

void ktf::soundsystem::pause(int64_t id)
{
    if (running) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            this->streams.at(id)->pause();
        }
    }
}

void ktf::soundsystem::resume(int64_t id)
{
    if (running) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            this->streams.at(id)->play();
        }
    }
}

void ktf::soundsystem::rewind(int64_t id)
{
    if (running) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            this->streams.at(id)->rewind();
        }
    }
}

void ktf::soundsystem::set_distance_model(distance_model model) const
{
    if (created) {
        switch (model) {
            case distance_model::att_exponent:
                ::alDistanceModel(AL_EXPONENT_DISTANCE);
                break;
            case distance_model::att_exponent_clamped:
                ::alDistanceModel(AL_EXPONENT_DISTANCE_CLAMPED);
                break;
            case distance_model::att_inverse:
                ::alDistanceModel(AL_INVERSE_DISTANCE);
                break;
            case distance_model::att_inverse_clamped:
                ::alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
                break;
            case distance_model::att_linear:
                ::alDistanceModel(AL_LINEAR_DISTANCE);
                break;
            case distance_model::att_linear_clamped:
                ::alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
                break;
        }
    }
}

float ktf::soundsystem::get_master_volume() const
{
    return master_volume;
}

void ktf::soundsystem::set_master_volume(float volume)
{
    if (created) {
        this->master_volume = volume;
        std::lock_guard<std::mutex> guard(mutex);
        for (auto const& stream : streams) {
            stream.second->set_gain(stream.second->get_gain(), master_volume);
        }
    }
}

void ktf::soundsystem::set_gain(float gain)
{
    if (created) {
        gain = std::max(0.0f, gain);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_gain(gain, master_volume);
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::set_pitch(float pitch)
{
    if (created) {
        pitch = std::max(0.0f, pitch);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_pitch(pitch);
        }
        ::alcProcessContext(context);
    }
}

float ktf::soundsystem::get_gain(int64_t id)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            return streams.at(id)->get_gain();
        }
    }
    return 0.0f;
}

float ktf::soundsystem::get_pitch(int64_t id)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            return streams.at(id)->get_pitch();
        }
    }
    return 0.0f;
}

void ktf::soundsystem::set_gain(int64_t id, float gain)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            return streams.at(id)->set_gain(gain, master_volume);
        }
    }
}

void ktf::soundsystem::set_pitch(int64_t id, float pitch)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            return streams.at(id)->set_pitch(pitch);
        }
    }
}

void ktf::soundsystem::set_rolloff_factor(float factor)
{
    if (created) {
        factor = std::max(0.0f, factor);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_rolloff_factor(factor);
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::set_max_distance(float distance)
{
    if (created) {
        distance = std::max(0.0f, distance);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_max_distance(distance);
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::set_reference_distance(float distance)
{
    if (created) {
        distance = std::max(0.0f, distance);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_reference_dist(distance);
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::set_min_gain(float value)
{
    if (created) {
        value = std::max(0.0f, value);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_min_gain(value);
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::set_max_gain(float value)
{
    if (created) {
        value = std::max(0.0f, value);
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for(auto const& stream : streams) {
            stream.second->set_max_gain(value);
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::set_rolloff_factor(int64_t id, float factor)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            factor = std::max(0.0f, factor);
            return streams.at(id)->set_rolloff_factor(factor);
        }
    }
}

void ktf::soundsystem::set_max_distance(int64_t id, float distance)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            distance = std::max(0.0f, distance);
            return streams.at(id)->set_max_distance(distance);
        }
    }
}

void ktf::soundsystem::set_reference_distance(int64_t id, float distance)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            distance = std::max(0.0f, distance);
            return streams.at(id)->set_reference_dist(distance);
        }
    }
}

void ktf::soundsystem::set_min_gain(int64_t id, float value)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            value = std::max(0.0f, value);
            return streams.at(id)->set_min_gain(value);
        }
    }
}

void ktf::soundsystem::set_max_gain(int64_t id, float value)
{
    if (created) {
        std::lock_guard<std::mutex> guard(mutex);
        if (streams.find(id) != streams.end()) {
            value = std::max(0.0f, value);
            return streams.at(id)->set_max_gain(value);
        }
    }
}

void ktf::soundsystem::run()
{
    if (running) {
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for (auto const& stream : streams) {
            stream.second->play();
        }
        ::alcProcessContext(context);
    }
    while (running) {
        {
            std::lock_guard<std::mutex> guard(mutex);
            std::vector<int64_t> ids;
            for (auto const& stream : streams) {
                if(stream.second->is_playing()) {
                    stream.second->update();
                } else {
                    ids.push_back(stream.first);
                }
            }
            for (int64_t id : ids) {
                this->streams.erase(id);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        this->check_error();
    }
    if (!running) {
        std::lock_guard<std::mutex> guard(mutex);
        ::alcSuspendContext(context);
        for (auto const& stream : streams) {
            stream.second->stop();
        }
        ::alcProcessContext(context);
    }
}

void ktf::soundsystem::check_error() const
{
    ALCenum error = ::alGetError();
    switch(error) {
        case AL_INVALID_NAME:
            break;
        case AL_INVALID_ENUM:
            break;
        case AL_INVALID_VALUE:
            break;
        case AL_INVALID_OPERATION:
            break;
        case AL_OUT_OF_MEMORY:
            break;
        default:
            break;
    }
}
