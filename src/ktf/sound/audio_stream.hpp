#pragma once

#include <AL/al.h>
#include <string>
#include <queue>

#define NUM_BUFFERS 16

namespace ktf {

class audio_stream {
  
    protected:
        int m_sample_rate;
        ALenum format;
        uint64_t m_duration;
        
        ALuint source;
        ALuint buffers[NUM_BUFFERS];
		std::queue<ALuint> buffer_queue;
        ALsizei m_progress;
        
        float pitch, gain;
        bool m_loop;
        
        bool failed;
        
        
    public:
        audio_stream();
        virtual ~audio_stream();
        
        
    public:
        bool fail() const;
        
        void play() const;
        void pause() const;
        void stop() const;
        void rewind();
        bool is_playing() const;
        
        float get_pitch() const;
        float get_gain() const;
        bool get_loop() const;
        
        void set_pitch(float pitch);
        void set_gain(float gain, float master_volume);
        void set_min_gain(float min_gain) const;
        void set_max_gain(float max_gain) const;
        void set_loop(bool loop);
        void set_reference_dist(float dist) const;
        void set_max_distance(float dist) const;
        void set_rolloff_factor(float fac) const;
        void set_source_relative(int relative) const;
        void set_position(float x, float y, float z) const;
        void set_velocity(float x, float y, float z) const;
        
        virtual void update() = 0;
        
    protected:
        virtual void reset_buffers() = 0;
        virtual void init_buffers() = 0;
		void clear_buffer_queue();
};

} // namespace ktf
