#include "audio_stream.hpp"

#include <thread>

ktf::audio_stream::audio_stream()
: m_sample_rate(0)
, format(0)
, m_duration(0)
, source(0)
, m_progress(0)
, pitch(1.0f)
, gain(1.0f)
, m_loop(false)
, failed(false)
{
	::alGenSources(1, &source);
	::alGenBuffers(NUM_BUFFERS, &buffers[0]);
	for (int i=0; i<NUM_BUFFERS; i++) {
		this->buffer_queue.push(buffers[i]);
	}
}

ktf::audio_stream::~audio_stream()
{
	::alDeleteBuffers(NUM_BUFFERS, &buffers[0]);
	::alDeleteSources(1, &source);
}


bool ktf::audio_stream::fail() const
{
    return failed;
}

bool ktf::audio_stream::is_playing() const
{
    ALenum source_state = AL_STOPPED;
    ::alGetSourcei(source, AL_SOURCE_STATE, &source_state);
    return (source_state == AL_PLAYING);
}

void ktf::audio_stream::play() const
{
    ::alSourcePlay(source);
}

void ktf::audio_stream::pause() const
{
    ::alSourcePause(source);
}

void ktf::audio_stream::stop() const
{
    ::alSourceStop(source);
}

void ktf::audio_stream::rewind()
{
    bool playing = is_playing();
    this->reset_buffers();
    if (playing) {
        ::alSourcePlay(source);
    }
}

float ktf::audio_stream::get_pitch() const
{
    return pitch;
}

float ktf::audio_stream::get_gain() const
{
    return gain;
}

bool ktf::audio_stream::get_loop() const
{
    return m_loop;
}

void ktf::audio_stream::set_pitch(float pitch)
{
    this->pitch = pitch;
    ::alSourcef(source, AL_PITCH, pitch);
}

void ktf::audio_stream::set_gain(float gain, float master_volume)
{
    this->gain = gain;
    ::alSourcef(source, AL_GAIN, gain * master_volume);
}

void ktf::audio_stream::set_min_gain(float min_gain) const
{
    ::alSourcef(source, AL_MIN_GAIN, min_gain);
}

void ktf::audio_stream::set_max_gain(float max_gain) const
{
    ::alSourcef(source, AL_MAX_GAIN, max_gain);
}

void ktf::audio_stream::set_loop(bool loop)
{
    this->m_loop = loop;
}

void ktf::audio_stream::set_reference_dist(float dist) const
{
    ::alSourcef(source, AL_REFERENCE_DISTANCE, dist);
}

void ktf::audio_stream::set_max_distance(float dist) const
{
    ::alSourcef(source, AL_MAX_DISTANCE, dist);
}

void ktf::audio_stream::set_rolloff_factor(float fac) const
{
    ::alSourcef(source, AL_ROLLOFF_FACTOR, fac);
}

void ktf::audio_stream::set_source_relative(int relative) const
{
    ::alSourcei(source, AL_SOURCE_RELATIVE, relative);
}

void ktf::audio_stream::set_position(float x, float y, float z) const
{
    ::alSource3f(source, AL_POSITION, x, y, z);
}

void ktf::audio_stream::set_velocity(float x, float y, float z) const
{
    ::alSource3f(source, AL_VELOCITY, x, y, z);
}

void ktf::audio_stream::clear_buffer_queue()
{
	this->play();

	// process all remaining buffers if not init
	ALint proc_buffers = 0;
	while (buffer_queue.size() < NUM_BUFFERS) {
		::alGetSourcei(source, AL_BUFFERS_PROCESSED, &proc_buffers);
		for (int i=0; i<proc_buffers; i++) {
			ALuint buffer;
			::alSourceUnqueueBuffers(source, 1, &buffer);
			buffer_queue.push(buffer);
		}
		std::this_thread::sleep_for(std::chrono::microseconds(100));
	}

	this->m_progress = 0;
	this->init_buffers();
}
