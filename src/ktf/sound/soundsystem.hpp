#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <vector>

struct ALCdevice;
struct ALCcontext;

namespace ktf {

class audio_stream;

class soundsystem {
    
    public:
        enum distance_model {
            att_exponent,
            att_exponent_clamped,
            att_inverse,
            att_inverse_clamped,
            att_linear,
            att_linear_clamped
        };
  
    private:
        ALCdevice* device;
        ALCcontext* context;
        std::atomic<bool> created;
        std::atomic<bool> running;
        std::thread thread;
        std::mutex mutex;
        
        std::atomic<int64_t> next_id;
        std::unordered_map<int64_t, std::shared_ptr<ktf::audio_stream>> streams;
        
        bool enumeration_support;
        float master_volume;
        float rolloff_factor;
        float max_distance;
        float reference_distance;
        float min_gain, max_gain;
        
        
    public:
        soundsystem();
        ~soundsystem();
        
        
    public:
        void create();
        void destroy();
        
        bool is_created() const;
        bool is_running() const;
        bool is_enumeration_supported() const;
        std::vector<std::string> get_device_list() const;
        void start();
        void stop();
        void clear();
        
        void set_listener_position(float x, float y, float z) const;
        void set_listener_velocity(float x, float y, float z) const;
        void set_listener_orientation(float dir_x, float dir_y, float dir_z, float up_x, float up_y, float up_z) const;
        
        void play(int64_t& id, const std::string& path, float gain, float pitch, bool loop);
        void play(int64_t& id, const std::string& path, float gain, float pitch, bool loop, float x, float y, float z);
        void stop(int64_t& id);
        bool is_playing(int64_t id);
        void pause(int64_t id);
        void resume(int64_t id);
        void rewind(int64_t id);
        
        void set_distance_model(distance_model model) const;
        float get_master_volume() const;
        void set_master_volume(float volume);
        void set_gain(float gain);
        void set_pitch(float pitch);
        float get_gain(int64_t id);
        float get_pitch(int64_t id);
        void set_gain(int64_t id, float gain);
        void set_pitch(int64_t id, float pitch);
        void set_rolloff_factor(float factor);
        void set_max_distance(float distance);
        void set_reference_distance(float distance);
        void set_min_gain(float value);
        void set_max_gain(float value);
        void set_rolloff_factor(int64_t id, float factor);
        void set_max_distance(int64_t id, float distance);
        void set_reference_distance(int64_t id, float distance);
        void set_min_gain(int64_t id, float value);
        void set_max_gain(int64_t id, float value);
        
        
    private:
        void run();
        void check_error() const;
};

} // namespace ktf
