#include "ffmpeg_stream.hpp"

size_t const MAX_FRAME_NUM = 128;

ktf::audio_frame::audio_frame(size_t out_num_samples, int channels, int samplerate, uint64_t pts, uint64_t pts_end, uint64_t duration)
: data(NULL)
, size(0)
, chs(channels)
, samplerate(samplerate)
, pts(pts)
, pts_end(pts_end)
, duration(duration)
{
	::av_samples_alloc(&data, NULL, channels, out_num_samples, AV_SAMPLE_FMT_S16, 0);
}

ktf::audio_frame::~audio_frame()
{
	::av_freep(&data);
}



ktf::ffmpeg_stream::ffmpeg_stream(const std::string& path)
: audio_stream()
, m_format_ctx(nullptr)
, m_codec_params(nullptr)
, m_codec_ctx(nullptr)
, m_codec(nullptr)
, m_stream_index(-1)
{
	if (::avformat_open_input(&m_format_ctx, path.c_str(), NULL, NULL) != 0) {
		this->m_format_ctx = nullptr;
		return;
	}
	if (::avformat_find_stream_info(m_format_ctx, NULL) < 0) {
		return;
	}

	this->m_codec = nullptr;
	this->m_codec_params = nullptr;
	for (unsigned int i = 0; i<m_format_ctx->nb_streams; i++) {
		this->m_codec_params = m_format_ctx->streams[i]->codecpar;
		this->m_codec = ::avcodec_find_decoder(m_codec_params->codec_id);
		if (m_codec_params->codec_type == AVMEDIA_TYPE_AUDIO) {
			m_stream_index = i;
			break;
		}
	}
	if (!m_codec_params || !m_codec) {
		return;
	}

	this->m_codec_ctx = ::avcodec_alloc_context3(m_codec);
	if (::avcodec_parameters_to_context(m_codec_ctx, m_codec_params) < 0) {
		return;
	}
	if (::avcodec_open2(m_codec_ctx, m_codec, NULL) < 0) {
		return;
	}
	this->m_duration = m_format_ctx->duration;
	this->m_sample_rate = m_codec_params->sample_rate;

	// work memory
	this->m_packet = ::av_packet_alloc();
	this->m_frame = ::av_frame_alloc();
	this->m_swr_ctx = ::swr_alloc();

	this->m_out_samplerate = 44100;
	this->m_out_channels = 2;

	if (m_out_channels == 1) {
		this->format = AL_FORMAT_MONO16;
	} else {
		this->format = AL_FORMAT_STEREO16;
	}

	this->m_seek = false;
	this->failed = false;
	this->m_stop = false;
	this->m_decoding_thread = std::thread(&ffmpeg_stream::decoding, this);
	this->init_buffers();
}

ktf::ffmpeg_stream::~ffmpeg_stream()
{
	this->stop_decoding();

	if (m_swr_ctx)
		::swr_free(&m_swr_ctx);
	if (m_packet)
		::av_free(m_packet);
	if (m_frame)
		::av_frame_free(&m_frame);
	if (m_codec_ctx)
		::avcodec_free_context(&m_codec_ctx);
	if (m_format_ctx)
		::avformat_close_input(&m_format_ctx);
	::avformat_free_context(m_format_ctx);

	::alDeleteBuffers(NUM_BUFFERS, &buffers[0]);
	::alDeleteSources(1, &source);
}


void ktf::ffmpeg_stream::update()
{
	ALint proc_buffers = 0;
	::alGetSourcei(source, AL_BUFFERS_PROCESSED, &proc_buffers);
	for (int i=0; i<proc_buffers; i++) {
		ALuint buffer;
		::alSourceUnqueueBuffers(source, 1, &buffer);
		buffer_queue.push(buffer);
	}

	int limiter = NUM_BUFFERS;
	while (!buffer_queue.empty() && limiter > 0) {
		this->fill_buffer();
		limiter--;
	}
}

void ktf::ffmpeg_stream::seek(double pos, bool clear)
{
	if (pos < 0.0 || pos > 1.0) {
		return;
	}

	uint64_t seek_pos = uint64_t(pos * m_duration);
	std::unique_lock<std::mutex> lck(m_mutex);
	this->m_seek = true;
	if (m_format_ctx->start_time != AV_NOPTS_VALUE) {
		seek_pos += m_format_ctx->start_time;
	}
	if (::av_seek_frame(m_format_ctx, -1, seek_pos, AVSEEK_FLAG_BACKWARD) >= 0) {
		::avcodec_flush_buffers(m_codec_ctx);
	}
	if (clear) {
		this->clear_queue();
		this->m_cond.notify_all();
	}
}

void ktf::ffmpeg_stream::init_buffers()
{
	for (int i=0; i<NUM_BUFFERS; i++) {
		this->fill_buffer();
	}
}

void ktf::ffmpeg_stream::reset_buffers()
{
	this->seek(0.0);
	this->clear_buffer_queue();
}

void ktf::ffmpeg_stream::decoding()
{
	int64_t in_channel_layout = av_get_default_channel_layout(m_codec_ctx->channels);
    int64_t out_channel_layout = 0;
    if (m_out_channels == 1) {
        out_channel_layout = AV_CH_LAYOUT_MONO;
    } else {
        out_channel_layout = AV_CH_LAYOUT_STEREO;
    }

    this->m_swr_ctx = ::swr_alloc_set_opts(m_swr_ctx,
		out_channel_layout,
		AV_SAMPLE_FMT_S16,
		m_out_samplerate,
		in_channel_layout,
		m_codec_ctx->sample_fmt,
		m_codec_ctx->sample_rate,
		0,
		NULL
	);
	::swr_init(m_swr_ctx);

	int rc;
	uint64_t last_pts;
begin_loop:
	last_pts = 0;
	while (::av_read_frame(m_format_ctx, m_packet) >= 0 && !m_stop) {
		if (m_packet->stream_index == m_stream_index) {
			rc = ::avcodec_send_packet(m_codec_ctx, m_packet);
			rc = ::avcodec_receive_frame(m_codec_ctx, m_frame);
			if (rc == 0) {
				// get timings
				AVRational const rational{1, AV_TIME_BASE};
				uint64_t duration = ::av_rescale_q(m_packet->duration, m_format_ctx->streams[m_stream_index]->time_base, rational);
				uint64_t pts = ::av_rescale_q(m_frame->best_effort_timestamp, m_format_ctx->streams[m_stream_index]->time_base, rational);
				uint64_t pts_end = ::av_rescale_q(m_frame->best_effort_timestamp + m_frame->pkt_duration, m_format_ctx->streams[m_stream_index]->time_base, rational);
				last_pts = pts_end;

				// resample
				const int out_num_samples = ::av_rescale_rnd(swr_get_delay(m_swr_ctx, m_frame->nb_samples) + m_frame->nb_samples, m_out_samplerate, m_frame->sample_rate, AV_ROUND_UP);
				std::shared_ptr<audio_frame> frame = std::make_shared<audio_frame>(out_num_samples, m_out_channels, m_out_samplerate, pts, pts_end, duration);
				int out_samples = ::swr_convert(m_swr_ctx, &frame->data, m_out_samplerate, (const uint8_t**)&m_frame->data[0], m_frame->nb_samples);
				frame->size = ::av_samples_get_buffer_size(NULL, m_out_channels, out_samples, AV_SAMPLE_FMT_S16, 1);

				std::unique_lock<std::mutex> lck(m_mutex);
				this->m_cond.wait(lck, [this]() {
					return m_stop || m_queue.size() < MAX_FRAME_NUM;
				});
				if (m_stop) {
					goto clean_step;
				}
				if (m_seek) {
					this->m_seek = false;
					goto clean_step;
				}
				this->m_queue.push(frame);
				lck.unlock();
				this->m_cond.notify_one();
			}
		clean_step:
			::av_frame_unref(m_frame);
			::av_packet_unref(m_packet);
		}
	}
	if (m_loop && last_pts >= m_duration) {
		this->seek(0.0, false);
		goto begin_loop;
	}
	this->m_stop = true;
}

void ktf::ffmpeg_stream::clear_queue()
{
	while (!m_queue.empty()) {
        this->m_queue.pop();
	}
}

std::shared_ptr<ktf::audio_frame> ktf::ffmpeg_stream::get_frame()
{
	std::shared_ptr<audio_frame> frame;;
	std::unique_lock<std::mutex> lck(m_mutex);
	this->m_cond.wait(lck, [this]() {
		return m_stop || m_queue.size() > 0;
	});
    if (!m_stop) {
        frame = m_queue.front();
        m_queue.pop();
    }
    lck.unlock();
    this->m_cond.notify_one();
    return frame;
}

void ktf::ffmpeg_stream::stop_decoding()
{
	this->m_stop = true;
    this->m_cond.notify_all();
    if (m_decoding_thread.joinable()) {
        this->m_decoding_thread.join();
	}
	{
		std::unique_lock<std::mutex> lck(m_mutex);
		this->clear_queue();
	}
}

bool ktf::ffmpeg_stream::fill_buffer()
{
	if (buffer_queue.empty()) {
		return false;
	}

	std::shared_ptr<audio_frame> frame = get_frame();
	if (!frame) {
		return false;
	}

	ALuint buffer = buffer_queue.front();
	::alBufferData(buffer, format, frame->data, frame->size, m_sample_rate);
	if (alGetError() == AL_NO_ERROR) {
		::alSourceQueueBuffers(source, 1, &buffer);
		this->buffer_queue.pop();
	}
	this->m_progress = frame->pts_end;

	return true;
}
