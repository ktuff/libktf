#pragma once

#include <atomic>
#include <condition_variable>
#include <queue>
#include <string>
#include <thread>

extern "C" {
	#include <libavformat/avformat.h>
	#include <libswresample/swresample.h>
}

#include "audio_stream.hpp"

namespace ktf {

class audio_frame {

	public:
		uint8_t* data;
		int size;
		int chs;
		int samplerate;
		uint64_t pts;
		uint64_t pts_end;
		uint64_t duration;


	public:
		audio_frame(size_t size, int channels, int samplerate, uint64_t pts, uint64_t pts_end, uint64_t duration);
		~audio_frame();
};


class ffmpeg_stream : public audio_stream {

	private:
		AVFormatContext *m_format_ctx;
		AVCodecParameters *m_codec_params;
		AVCodecContext *m_codec_ctx;
		AVCodec *m_codec;
		AVPacket *m_packet;
		AVFrame *m_frame;
		SwrContext *m_swr_ctx;

		int m_stream_index;
		int m_out_channels;
		int m_out_samplerate;
		std::queue<std::shared_ptr<audio_frame>> m_queue;
		std::thread m_decoding_thread;
		std::atomic<bool> m_stop;
		std::atomic<bool> m_seek;
		std::mutex m_mutex;
		std::condition_variable m_cond;


	public:
		ffmpeg_stream(std::string const& path);
		~ffmpeg_stream();


	public:
		void update() override final;
		void seek(double pos, bool clear = true);


    protected:
        void init_buffers() override final;
		void reset_buffers() override final;

	private:
		void decoding();
		void clear_queue();
		std::shared_ptr<audio_frame> get_frame();
		void stop_decoding();
		bool fill_buffer();
};

} // namespace ktf
