#pragma once

#include <vorbis/vorbisfile.h>
#include <fstream>

#include "audio_stream.hpp"

namespace ktf {

class ogg_stream : public ktf::audio_stream {
    
    private:
		OggVorbis_File vf;
		std::ifstream file;
		ALsizei size;
		int current_section;
        
        
    public:
        ogg_stream(const std::string& path);
        ~ogg_stream();
        
        
    public:
        void update() override final;
        
        static size_t read_ogg(void* destination, size_t size1, size_t size2, void *datasource);
        static int seek_ogg(void *datasource, ogg_int64_t offset, int base);
        static long tell_ogg(void *datasource);
        
        
    protected:
        void init_buffers() override final;
		void reset_buffers() override final;

	private:
		bool fill_buffer();
};

} // namespace ktf
