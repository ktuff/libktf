#pragma once

#include <stddef.h>

namespace ktf {

template<typename V>
class st_index_map {

	private:
		V* values;
		bool* set;
		size_t* indices;
		size_t used;
		size_t cap;
		size_t ind;


	public:
		st_index_map<V>();
		st_index_map<V>(size_t size);
		st_index_map<V>(st_index_map<V>&& map);
		st_index_map<V>(const st_index_map<V>& map);
		~st_index_map<V>();


	public:
		st_index_map<V>& operator=(st_index_map<V>&& m);
		st_index_map<V>& operator=(const st_index_map<V>& m);

	public:
		size_t size() const;
		size_t capacity() const;
		void resize(size_t size);
		void clear();

		V* data() const;
		V const& get(size_t index) const;
		V pop(size_t index);
		size_t push(V&& v);
		size_t push(const V& v);

		size_t index_of(const V& v) const;
		bool valid(size_t index) const;

	protected:
		size_t* get_indices() const;
		bool* get_valids() const;
		size_t free_indices() const;
};

} // namespace ktf

#include "st_index_map.cpp"
