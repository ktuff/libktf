#include <algorithm>

#include "st_index_map.hpp"

template<typename V>
ktf::st_index_map<V>::st_index_map()
: values(nullptr)
, set(nullptr)
, indices(nullptr)
, used(0)
, cap(0)
, ind(0)
{
}

template<typename V>
ktf::st_index_map<V>::st_index_map(size_t size)
: st_index_map()
{
	this->resize(size);
}

template<typename V>
ktf::st_index_map<V>::st_index_map(st_index_map<V> && map)
: values(map.values)
, set(map.set)
, indices(map.indices)
, used(map.used)
, cap(map.cap)
, ind(map.ind)
{
	map.values = nullptr;
	map.set = nullptr;
	map.indices = nullptr;
	map.used = 0;
	map.cap = 0;
	map.ind = 0;
}

template<typename V>
ktf::st_index_map<V>::st_index_map(const ktf::st_index_map<V>& map)
: values(nullptr)
, set(nullptr)
, indices(nullptr)
, used(0)
, cap(0)
, ind(0)
{
	this->resize(map.capacity());

	const V* dat = map.data();
	const size_t* indis = map.get_indices();
	const bool* sets = map.get_valids();

	size_t map_size = map.size();
	for (size_t i=0; i<map_size; i++) {
		values[i] = dat[i];
		set[i] = sets[i];
	}
	size_t index_count = map.ind_count();
	for (size_t i=0; i<map.ind_count(); i++) {
		indices[i] = indis[i];
	}
	this->ind = index_count;
	this->used = map.size();
}


template<typename V>
ktf::st_index_map<V>::~st_index_map()
{
	this->clear();
}


template<typename V>
ktf::st_index_map<V> & ktf::st_index_map<V>::operator=(st_index_map<V> && m)
{
	this->clear();
	this->values = m.values;
	this->set = m.set;
	this->indices = m.indices;
	this->used = m.used;
	this->cap = m.used;
	this->ind = m.used;
	m.values = nullptr;
	m.set = nullptr;
	m.indices = nullptr;
	m.used = 0;
	m.cap = 0;
	m.ind = 0;
	return *this;
}

template<typename V>
ktf::st_index_map<V>& ktf::st_index_map<V>::operator=(const ktf::st_index_map<V>& map)
{
	this->clear();
	this->resize(map.capacity());
	const V* dat = map.data();
	const size_t* indis = map.get_indices();
	const bool* sets = map.get_valids();
	// copy
	size_t map_size = map.size();
	for (size_t i=0; i<map_size; i++) {
		this->values[i] = dat[i];
		this->set[i] = sets[i];
	}
	size_t index_count = map.ind_count();
	for (size_t i=0; i<map.ind_count(); i++) {
		this->indices[i] = indis[i];
	}
	this->ind = index_count;
	this->used = map.size();

	map.clear();

	return *this;
}

template<typename V>
size_t ktf::st_index_map<V>::size() const
{
	return used;
}

template<typename V>
size_t ktf::st_index_map<V>::capacity() const
{
	return cap;
}

template<typename V>
void ktf::st_index_map<V>::resize(size_t size)
{
	V* nvals = new V[size];
	size_t* nind = new size_t[size];
	bool* nset = new bool[size];
	for (size_t i=0; i<cap; i++) {
		nvals[i] = std::move(values[i]);
		nind[i] = indices[i];
		nset[i] = set[i];
	}
	std::fill_n(&nset[cap], size-cap, false);
	if (values) {
		delete[] values;
	}
	if (indices) {
		delete[] indices;
	}
	if (set) {
		delete[] set;
	}
	this->values = nvals;
	this->indices = nind;
	this->set = nset;
	size_t j = size - cap - 1;
	for (size_t i=cap; i<size; i++) {
		this->indices[j] = i;
		j--;
	}
	this->ind += (size - cap);
	this->cap = size;
}

template<typename V>
void ktf::st_index_map<V>::clear()
{
	if (values) {
		delete[] values;
	}
	if (indices) {
		delete[] indices;
	}
	if (set) {
		delete[] set;
	}
	this->values = nullptr;
	this->indices = nullptr;
	this->set = nullptr;
	this->ind = 0;
	this->cap = 0;
	this->used = 0;
}

template<typename V>
V * ktf::st_index_map<V>::data() const
{
	return values;
}

template<typename V>
V const& ktf::st_index_map<V>::get(size_t index) const
{
	return values[index];
}

template<typename V>
V ktf::st_index_map<V>::pop(size_t index)
{
	this->indices[ind] = index;
	this->ind++;
	this->set[index] = false;
	this->used--;
	V val = std::move(values[index]);
	this->values[index] = V();
	return val;
}

template<typename V>
size_t ktf::st_index_map<V>::push(V&& v)
{
	if (!ind) {
		this->resize(std::max(cap * 2, (size_t)4));
	}
	size_t index = indices[ind - 1];
	this->values[index] = std::move(v);
	this->set[index] = true;
	this->ind--;
	this->used++;
	return index;
}

template<typename V>
size_t ktf::st_index_map<V>::push(const V& v)
{
	if (!ind) {
		this->resize(std::max(cap * 2, (size_t)4));
	}
	size_t index = indices[ind - 1];
	this->values[index] = v;
	this->set[index] = true;
	this->ind--;
	this->used++;
	return index;
}

template<typename V>
size_t ktf::st_index_map<V>::index_of(const V& v) const
{
	for (size_t i=0; i<cap; i++) {
		if (values[i] == v) {
			return i;
		}
	}
	return -1;
}

template<typename V>
bool ktf::st_index_map<V>::valid(size_t index) const
{
	if (index >= cap) {
		return false;
	} else {
		return set[index];
	}
}

template<typename V>
size_t* ktf::st_index_map<V>::get_indices() const
{
	return indices;
}

template<typename V>
bool * ktf::st_index_map<V>::get_valids() const
{
	return set;
}

template<typename V>
size_t ktf::st_index_map<V>::free_indices() const
{
	return ind;
}
