#include "parallel_queue.hpp"

template<typename T>
ktf::parallel_queue<T>::parallel_queue()
: m_values(nullptr)
, m_size(0)
, m_capacity(0)
, m_front_ptr(0)
, m_rear_ptr(0)
{
}

template<typename T>
ktf::parallel_queue<T>::parallel_queue(const ktf::parallel_queue<T>& q)
: m_size(q.m_size)
, m_capacity(q.m_capacity)
, m_front_ptr(0)
, m_rear_ptr(m_size)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	this->m_values = new T[q.m_capacity];
	for (size_t i=0; i<q.m_size; i++) {
		this->m_values[i] = q.m_values[(q.m_front_ptr + i) % q.m_capacity];
	}
	for (size_t i=q.m_size; i<q.m_capacity; i++) {
		this->m_values[i] = T();
	}
}

template<typename T>
ktf::parallel_queue<T>::parallel_queue(ktf::parallel_queue<T> && q)
: m_values(q.m_values)
, m_size(q.m_size)
, m_capacity(q.m_capacity)
, m_front_ptr(q.m_front_ptr)
, m_rear_ptr(q.m_rear_ptr)
{
	std::lock_guard<std::mutex> lock1(q.m_mutex);
	std::lock_guard<std::mutex> lock2(m_mutex);
	q.m_values = nullptr;
	q.m_size = 0;
	q.m_capacity = 0;
	q.m_front_ptr = 0;
	q.m_rear_ptr = 0;
}

template<typename T>
ktf::parallel_queue<T> & ktf::parallel_queue<T>::operator=(const ktf::parallel_queue<T>& q)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_values) {
		delete[] m_values;
	}
	this->m_size = q.m_size;
	this->m_capacity = q.m_capacity;
	this->m_front_ptr = 0;
	this->m_rear_ptr = m_size;

	this->m_values = new T[q.m_capacity];
	for (size_t i=0; i<q.m_size; i++) {
		this->m_values[i] = q.m_values[(q.m_front_ptr + i) % q.m_capacity];
	}
	for (size_t i=q.m_size; i<q.m_capacity; i++) {
		this->m_values[i] = T();
	}
	return *this;
}

template<typename T>
ktf::parallel_queue<T> & ktf::parallel_queue<T>::operator=(ktf::parallel_queue<T> && q)
{
	std::lock_guard<std::mutex> lock1(q.m_mutex);
	std::lock_guard<std::mutex> lock2(m_mutex);
	ktf::parallel_queue<T> nq;
	this->swap(q);
	q.swap(nq);
	return *this;
}

template<typename T>
ktf::parallel_queue<T>::~parallel_queue()
{
	if (m_values) {
		delete[] m_values;
	}
}


template<typename T>
size_t ktf::parallel_queue<T>::size() const
{
	return m_size;
}

template<typename T>
void ktf::parallel_queue<T>::swap(ktf::parallel_queue<T>& q)
{
	std::lock_guard<std::mutex> lock1(q.m_mutex);
	std::lock_guard<std::mutex> lock2(m_mutex);
	T* qv = q.m_values;
	long qs = q.m_size;
	long qc = q.m_capacity;
	long qf = q.m_front_ptr;
	long qr = q.m_rear_ptr;
	q.m_values = m_values;
	q.m_size = m_size;
	q.m_capacity = m_capacity;
	q.m_front_ptr = m_front_ptr;
	q.m_rear_ptr = m_rear_ptr;
	this->m_values = qv;
	this->m_size = qs;
	this->m_capacity = qc;
	this->m_front_ptr =  qf;
	this->m_rear_ptr = qr;
}

template<typename T>
void ktf::parallel_queue<T>::enqueue(T&& t)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_size >= m_capacity) {
		// grow size
		size_t nsize = std::max((size_t)1, m_capacity * 2);
		T* v_values = new T[nsize];
		for (size_t i=0; i<m_size; i++) {
			v_values[i] = std::move(m_values[(m_front_ptr + i) % m_capacity]);
		}
		for (size_t i=m_size; i<nsize; i++) {
			v_values[i] = T();
		}
		if (m_values)  {
			delete[] m_values;
		}
		this->m_values = v_values;
		this->m_capacity = nsize;
		this->m_front_ptr = 0;
		this->m_rear_ptr = m_size;
	}
	this->m_values[m_rear_ptr] = std::move(t);
	this->m_rear_ptr = (m_rear_ptr + 1) % m_capacity;
	this->m_size++;
}

template<typename T>
T ktf::parallel_queue<T>::dequeue()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_size <= 0) {
		throw std::exception();
	} else {
		T t = std::move(m_values[m_front_ptr]);
		this->m_front_ptr = (m_front_ptr+1) % m_capacity;
		if (m_size <= m_capacity*0.25) {
			// resize - shrink
			size_t nsize = std::max((size_t)1, m_capacity / 2);
			T* v_values = new T[nsize];
			for (size_t i=0; i<m_size; i++) {
				v_values[i] = std::move(v_values[(m_front_ptr + i) % m_capacity]);
			}
			for (size_t i=m_size; i<nsize; i++) {
				v_values[i] = T();
			}
			if (m_values) {
				delete[] m_values;
			}
			this->m_values = v_values;
			this->m_capacity = nsize;
			this->m_front_ptr = 0;
			this->m_rear_ptr = m_size;
		}
		this->m_size--;
		return t;
	}
}
