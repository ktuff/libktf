#include "vector.hpp"

template<typename T>
ktf::vector<T>::vector()
: values(nullptr)
, vindex(0)
, vcapacity(1)
{
	this->resize(1);
}

template<typename T>
ktf::vector<T>::vector(size_t size)
: values(nullptr)
, vindex(0)
, vcapacity(1)
{
	this->resize(size);
}

template<typename T>
ktf::vector<T>::vector(ktf::vector<T>&& vec)
: vindex(vec.vindex)
, vcapacity(vec.vcapacity)
{
	this->values = vec.values;

	vec.values = nullptr;
	vec.vindex = 0;
	vec.vcapacity = 0;
}

template<typename T>
ktf::vector<T>::vector(const ktf::vector<T>& vec)
: values(nullptr)
, vindex(0)
, vcapacity(1)
{
	this->resize(vec.capacity());

	const T* dat = vec.data();
	for (size_t i=0; i<vec.size(); i++) {
		this->values[i] = dat[i];
	}
	this->vindex = vec.size();
}

template<typename T>
ktf::vector<T>::vector(const T* data, size_t size)
: values(nullptr)
, vindex(0)
, vcapacity(1)
{
	this->resize(1);
	for (size_t i=0; i<size; i++) {
		this->push(data[i]);
	}
}

template<typename T>
ktf::vector<T>::~vector()
{
	if (values) {
		delete[] values;
	}
}

template<typename T>
ktf::vector<T>& ktf::vector<T>::operator=(ktf::vector<T>&& vec)
{
	if (values) {
		delete[] values;
	}
	this->values = vec.values;
	this->vindex = vec.vindex;
	this->vcapacity = vec.vcapacity;

	vec.values = nullptr;
	vec.vindex = 0;
	vec.vcapacity = 0;
	return *this;
}

template<typename T>
ktf::vector<T>& ktf::vector<T>::operator=(const ktf::vector<T>& vec)
{
	this->vcapacity = vec.capacity();
	this->vindex = vec.size();
	if (values) {
		delete[] values;
	}
	this->values = new T[vcapacity];

	const T* dat = vec.data();
	for (size_t i=0; i<vec.size(); i++) {
		this->values[i] = dat[i];
	}
	this->vindex = vec.size();
	return *this;
}

template<typename T>
size_t ktf::vector<T>::capacity() const
{
	return vcapacity;
}

template<typename T>
size_t ktf::vector<T>::size() const
{
	return vindex;
}

template<typename T>
void ktf::vector<T>::resize(size_t size)
{
	T* nvals = new T[size];
	if (values) {
		const size_t copy_size = (size < vcapacity) ? size : vcapacity;
		for (size_t i=0; i<copy_size; i++) {
			nvals[i] = values[i];
		}
		delete[] values;
	}
	this->values = nvals;
	this->vcapacity = size;
	if (size < vindex) {
		this->vindex = size;
	}
}

template<typename T>
void ktf::vector<T>::clear()
{
	if (values) {
		delete[] values;
	}
	this->values = nullptr;
	this->vindex = 0;
	this->vcapacity = 1;
	this->resize(1);
}

template<typename T>
void ktf::vector<T>::push(const T& t)
{
	if (vcapacity <= vindex) {
		this->resize(vcapacity * 2);
	}
	this->values[vindex] = t;
	this->vindex++;
}

template<typename T>
T& ktf::vector<T>::operator[](size_t index) const
{
	return values[index];
}

template<typename T>
T * ktf::vector<T>::data() const
{
	return values;
}
