#pragma once

#include <stddef.h>

namespace ktf {

template<typename T>
class vector {

	private:
		T* values;
		size_t vindex;
		size_t vcapacity;


	public:
		vector<T>();
		vector<T>(size_t size);
		vector<T>(vector<T>&& vec);
		vector<T>(const vector<T>& vec);
		vector<T>(const T* data, size_t size);
		~vector<T>();


	public:
		vector<T>& operator=(vector<T>&& vec);
		vector<T>& operator=(const vector<T>& vec);

	public:
		size_t capacity() const;
		size_t size() const;
		void resize(size_t size);
		void clear();
		void push(const T& t);
		T& operator[](size_t index) const;
		T* data() const;
};

#include "vector.cpp"

} // namespace ktf
