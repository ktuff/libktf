#pragma once

#include <mutex>

namespace ktf {

template <typename T>
class parallel_queue {

	private:
		std::mutex m_mutex;
		T* m_values;
		size_t m_size;
		size_t m_capacity;
		size_t m_front_ptr;
		size_t m_rear_ptr;


	public:
		parallel_queue();
		parallel_queue(const parallel_queue& q);
		parallel_queue(parallel_queue&& q);
		parallel_queue& operator=(const parallel_queue& q);
		parallel_queue& operator=(parallel_queue&& q);
		~parallel_queue();


	public:
		size_t size() const;
		void swap(parallel_queue& q);
		void enqueue(T&& t);
		T dequeue();
};

} // namespace ktf

#include "parallel_queue.cpp"
