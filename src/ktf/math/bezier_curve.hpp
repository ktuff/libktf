#pragma once

#include <stddef.h>
#include <vector>

namespace ktf {

class bezier_curve {

	private:
		size_t const m_num_points;
		std::vector<double> const m_values;


	public:
		bezier_curve(std::vector<double> const& pts, size_t cpts);
		bezier_curve(bezier_curve && curve) = delete;
		bezier_curve(bezier_curve const& curve) = delete;
		~bezier_curve();
		bezier_curve operator=(bezier_curve && curve) = delete;
		bezier_curve operator=(bezier_curve const & curve) = delete;


	public:
		size_t size() const;
		double at(size_t index) const;

	private:
		std::vector<double> generate_values(std::vector<double> const& pts, size_t cpts) const;

};

} // namespace ktf

