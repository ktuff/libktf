#pragma once

#include <cmath>
#include <functional>

namespace ktf {

    template<typename T>
    struct vec4 {
        T x, y, z, w;
        
        constexpr vec4();
        constexpr vec4(T f);
        constexpr vec4(T x, T y, T z, T w);
        constexpr vec4(const vec4<T>& v);
        template<typename R> constexpr vec4(const vec4<R>& v);
        ~vec4();
        
        T & operator[](unsigned int index);
        T const & operator[](unsigned int index) const;
        
        vec4<T> operator-() const;
        
        vec4<T> operator+(T f) const;
        vec4<T> operator-(T f) const;
        vec4<T> operator*(T f) const;
        vec4<T> operator/(T f) const;
        
        vec4<T> operator+(const vec4<T>& v) const;
        vec4<T> operator-(const vec4<T>& v) const;
        
        T operator*(const vec4<T>& v) const;
        vec4<T> operator^(const vec4<T>& v) const;
        
        void operator=(const vec4<T>& v);
        void operator+=(const vec4<T>& v);
        void operator-=(const vec4<T>& v);
        
        void operator+=(T v);
        void operator-=(T v);
        void operator*=(T v);
        void operator/=(T v);
        
        bool operator==(const vec4<T>& v) const;
        bool operator!=(const vec4<T>& v) const;
        bool operator<(const vec4<T>& v) const;
        bool operator<=(const vec4<T>& v) const;
        bool operator>(const vec4<T>& v) const;
        bool operator>=(const vec4<T>& v) const;
            
        T length() const;
        T dist(const vec4<T>& v) const;
        void normalize();
        static vec4<T> normalize(const vec4<T>& v);
    };

}

namespace std {
    template<typename T>
    struct hash<ktf::vec4<T>> {
        size_t operator() (const ktf::vec4<T>& v) const;
    };
}

#include "vec4.cpp"
