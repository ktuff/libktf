#pragma once

namespace ktf {

class spring {

	private:
		double const m_ks;
		double const m_kd;
		double const m_l0;
		double m_x;
		double m_dx;


	public:
		spring(double ks, double kd, double l0);
		~spring() = default;


	public:
		void set_x(double x);
		void push(double dx);
		double get_x() const;
		void step(double dt);
};

} // namespace ktf
