#include <cmath>

#include "pendulum.hpp"

ktf::pendulum::pendulum(double length, double mass, double offset, double damping)
: m_l(length)
, m_m(mass)
, m_d(damping)
, m_u(offset)
, m_phi(0)
, m_omega(0)
, m_t(0)
{
}


void ktf::pendulum::step(double dt)
{
	double eps = 0.01;

	double domega = m_u - m_d/(m_m*m_l*m_l)*m_omega - g/m_l * std::sin(m_phi);
	this->m_omega += dt*domega;
	this->m_phi += dt*m_omega;

	if (std::abs(m_phi) < eps && std::abs(m_omega) < eps) {
		this->m_omega = 0.0;
		this->m_phi = 0.0;
	}
}

void ktf::pendulum::push(double p)
{
	if (m_omega < 0) {
		this->m_omega -= p;
	} else {
		this->m_omega += p;
	}
}

double ktf::pendulum::get_phi() const
{
	return m_phi;
}
