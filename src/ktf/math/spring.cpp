#include <cmath>

#include "spring.hpp"

ktf::spring::spring(double ks, double kd, double l0)
: m_ks(ks)
, m_kd(kd)
, m_l0(l0)
, m_x(0.0)
, m_dx(0.0)
{
}

void ktf::spring::set_x(double x)
{
	this->m_x = x;
	this->m_dx = 0.0;
}

void ktf::spring::push(double dx)
{
	this->m_dx += ((m_dx >= 0) - (m_dx < 0))*dx;
}

double ktf::spring::get_x() const
{
	return m_x;
}

void ktf::spring::step(double dt)
{
	double dx = (std::abs(m_x) - m_l0) * ((m_x > 0) - (m_x < 0));
	double f = -(dx*m_ks + m_dx*m_kd);
	this->m_x += dt * m_dx;
	this->m_dx += dt * f;
}
