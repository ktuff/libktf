#pragma once

#include <cmath>
#include <functional>

namespace ktf {

    template<typename T>
    struct vec2 {
        T x, y;
        
        constexpr vec2();
        constexpr vec2(T f);
        constexpr vec2(T x, T y);
        constexpr vec2(const vec2<T>& v);
        template<typename R> constexpr vec2(const vec2<R>& v);
        ~vec2();
        
        T & operator[](unsigned int index);
        T const & operator[](unsigned int index) const;
        
        vec2<T> operator-() const;
        
        vec2<T> operator+(T f) const;
        vec2<T> operator-(T f) const;
        vec2<T> operator*(T f) const;
        vec2<T> operator/(T f) const;
        
        vec2<T> operator+(const vec2<T>& v) const;
        vec2<T> operator-(const vec2<T>& v) const;
        T operator*(const vec2<T>& v) const;
        vec2<T> operator^(const vec2<T>& v) const;
        
        void operator=(const vec2<T>& v);
        void operator+=(const vec2<T>& v);
        void operator-=(const vec2<T>& v);
        
        void operator+=(T v);
        void operator-=(T v);
        void operator*=(T v);
        void operator/=(T v);
        
        bool operator==(const vec2<T>& v) const;
        bool operator!=(const vec2<T>& v) const;
        bool operator<=(const vec2<T>& v) const;
        bool operator>=(const vec2<T>& v) const;
        bool operator<(const vec2<T>& v) const;
        bool operator>(const vec2<T>& v) const;
        
        T length() const;
        T dist(const vec2<T>& v) const;
        void normalize();
        static vec2<T> normalize(const vec2<T>& v);
    };
}

namespace std {
    template<typename T>
    struct hash<ktf::vec2<T>> {
        size_t operator() (const ktf::vec2<T>& v) const;
    };
}

#include "vec2.cpp"
