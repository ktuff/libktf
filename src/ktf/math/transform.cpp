#include "transform.hpp"

template<typename T>
ktf::matrix<T> ktf::translate(const ktf::matrix<T>& m, const ktf::vec3<T>& v)
{
	ktf::matrix<T> mat(
		ktf::vec4<T>(1.0, 0.0, 0.0, 0.0),
		ktf::vec4<T>(0.0, 1.0, 0.0, 0.0),
		ktf::vec4<T>(0.0, 0.0, 1.0, 0.0),
		ktf::vec4<T>(v.x, v.y, v.z, 1.0)
	);
	return mat * m;
}

template<typename T>
ktf::matrix<T> ktf::scale(const ktf::matrix<T>& m, const ktf::vec3<T>& v)
{
	ktf::matrix<T> mat(1.0);
	mat[0][0] = v.x;
	mat[1][1] = v.y;
	mat[2][2] = v.z;
	return mat * m;
}

template<typename T, typename U, typename S>
ktf::matrix<T> ktf::rotate(const ktf::matrix<T>& m, const U& rad, const ktf::vec3<S>& d)
{
	const ktf::vec3<T> v(ktf::vec3<T>::normalize(d));
	const T cos = std::cos(rad);
	const T sin = std::sin(rad);
	const ktf::vec3<T> rv(v*(1.0-cos));
	const ktf::vec3<T> sv(v*sin);

	ktf::matrix<T> rmat(1.0);
	rmat[0][0] = rv.x*v.x + cos;
	rmat[0][1] = rv.x*v.y + sv.z;
	rmat[0][2] = rv.x*v.z - sv.y;
	rmat[1][0] = rv.y*v.x - sv.z;
	rmat[1][1] = rv.y*v.y + cos;
	rmat[1][2] = rv.y*v.z + sv.x;
	rmat[2][0] = rv.z*v.x + sv.y;
	rmat[2][1] = rv.z*v.y - sv.x;
	rmat[2][2] = rv.z*v.z + cos;

	return rmat * m;
}

template<typename T, typename U, typename S>
ktf::matrix<T> ktf::shear(ktf::matrix<U> const& m, ktf::vec2<S> const& s1, ktf::vec2<S> const& s2, ktf::vec2<S> const& s3)
{
	ktf::matrix<T> mat(1.0);
	mat[0][1] = s2[0];
	mat[0][2] = s3[0];
	mat[1][0] = s1[0];
	mat[1][2] = s3[1];
	mat[2][0] = s1[1];
	mat[2][1] = s2[1];
	return mat * m;
}
