#include "vec3.hpp"

template<typename T> constexpr ktf::vec3<T>::vec3() : x(0), y(0), z(0) {
}

template<typename T> constexpr ktf::vec3<T>::vec3(T f) : x(f), y(f), z(f) {
}

template<typename T> constexpr ktf::vec3<T>::vec3(T x, T y, T z) : x(x), y(y), z(z) {
}

template<typename T> constexpr ktf::vec3<T>::vec3(const ktf::vec3<T>& v) : x(v.x), y(v.y), z(v.z) {
}

template<typename T>
template<typename R> constexpr ktf::vec3<T>::vec3(const ktf::vec3<R>& v) {
    x = static_cast<T>(v.x);
    y = static_cast<T>(v.y);
    z = static_cast<T>(v.z);
}

template<typename T> ktf::vec3<T>::~vec3() {
}


template<typename T> T & ktf::vec3<T>::operator[](unsigned int index) {
    switch(index) {
        default:
        case 0: return x;
        case 1: return y;
        case 2: return z;
    }
}

template<typename T> T const & ktf::vec3<T>::operator[](unsigned int index) const {
    switch(index) {
        default:
        case 0: return x;
        case 1: return y;
        case 2: return z;
    }
}


template<typename T> ktf::vec3<T> ktf::vec3<T>::operator-() const {
    return vec3<T>(-x, -y, -z);
}


template<typename T> ktf::vec3<T> ktf::vec3<T>::operator+(T f) const {
    return vec3<T>(x+f, y+f, z+f);
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::operator-(T f) const {
    return vec3<T>(x-f, y-f, z-f);
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::operator*(T f) const {
    return vec3<T>(x*f, y*f, z*f);
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::operator/(T f) const {
    return vec3<T>(x/f, y/f, z/f);
}


template<typename T> ktf::vec3<T> ktf::vec3<T>::operator+(const ktf::vec3<T>& v) const {
    return vec3<T>(x+v.x, y+v.y, z+v.z);
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::operator-(const ktf::vec3<T>& v) const {
    return vec3<T>(x-v.x, y-v.y, z-v.z);
}


template<typename T> T ktf::vec3<T>::operator*(const ktf::vec3<T>& v) const {
    return x*v.x + y*v.y + z*v.z;
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::operator^(const ktf::vec3<T>& v) const {
    return vec3<T>(x*v.x, y*v.y, z*v.z);
}


template<typename T> void ktf::vec3<T>::operator=(const ktf::vec3<T>& v) {
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
}

template<typename T> void ktf::vec3<T>::operator+=(const ktf::vec3<T>& v) {
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
}

template<typename T> void ktf::vec3<T>::operator-=(const ktf::vec3<T>& v) {
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
}


template<typename T> void ktf::vec3<T>::operator+=(T v) {
    this->x += v;
    this->y += v;
    this->z += v;
}

template<typename T> void ktf::vec3<T>::operator-=(T v) {
    this->x -= v;
    this->y -= v;
    this->z -= v;
}

template<typename T> void ktf::vec3<T>::operator*=(T v) {
    this->x *= v;
    this->y *= v;
    this->z *= v;
}

template<typename T> void ktf::vec3<T>::operator/=(T v) {
    this->x /= v;
    this->y /= v;
    this->z /= v;
}


template<typename T> bool ktf::vec3<T>::operator==(const ktf::vec3<T>& v) const {
    return x==v.x && y==v.y && z==v.z;
}

template<typename T> bool ktf::vec3<T>::operator!=(const ktf::vec3<T>& v) const {
    return x!=v.x || y!=v.y || z!=v.z;
}

template<typename T> bool ktf::vec3<T>::operator>=(const ktf::vec3<T>& v) const {
    return x>=v.x && y>=v.y && z>=v.z;
}

template<typename T> bool ktf::vec3<T>::operator<=(const ktf::vec3<T>& v) const {
    return x<=v.x && y<=v.y && z<=v.z;
}


template<typename T> bool ktf::vec3<T>::operator>(const ktf::vec3<T>& v) const {
    return x>v.x && y>v.y && z>v.z;
}

template<typename T> bool ktf::vec3<T>::operator<(const ktf::vec3<T>& v) const {
    return x<v.x && y<v.y && z<v.z;
}


template<typename T> T ktf::vec3<T>::length() const {
    return std::sqrt(x*x + y*y + z*z);
}

template<typename T> T ktf::vec3<T>::dist(const ktf::vec3<T>& v) const {
    return (*this - v).length();
}

template<typename T> void ktf::vec3<T>::normalize() {
    double t = length();
    if(t == 0.0f)
        return;
    double fac = 1/t;
    x *= fac;
    y *= fac;
    z *= fac;
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::normalize(const ktf::vec3<T>& v) {
    double t = v.length();
    if(t == 0.0)
        return vec3<T>(0.0);
    double fac = 1/t;
    return vec3<T>(v.x*fac, v.y*fac, v.z*fac);
}

template<typename T> ktf::vec3<T> ktf::vec3<T>::cross(const ktf::vec3<T>& v1, const ktf::vec3<T>& v2) {
    return vec3<T>(v1.y*v2.z - v1.z*v2.y, v1.z*v2.x - v1.x*v2.z, v1.x*v2.y - v1.y*v2.x);
}


template<typename T> size_t std::hash<ktf::vec3<T>>::operator()(const ktf::vec3<T>& v) const {
    return ((hash<T>()(v.x) ^ (hash<T>()(v.y) << 8)) >> 1) ^ (hash<T>()(v.z) << 16);
}
