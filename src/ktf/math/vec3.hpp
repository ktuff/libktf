#pragma once

#include <cmath>
#include <functional>

namespace ktf {
    
    template<typename T>
    struct vec3 {
        T x, y, z;
        
        constexpr vec3();
        constexpr vec3(T f);
        constexpr vec3(T x, T y, T z);
        constexpr vec3(const vec3<T>& v);
        template<typename R> constexpr vec3(const vec3<R>& v);
        ~vec3();
        
        T & operator[](unsigned int index);
        T const & operator[](unsigned int index) const;
        
        vec3<T> operator-() const;
        
        vec3<T> operator+(T f) const;
        vec3<T> operator-(T f) const;
        vec3<T> operator*(T f) const;
        vec3<T> operator/(T f) const;
        
        vec3<T> operator+(const vec3<T>& v) const;
        vec3<T> operator-(const vec3<T>& v) const;
        
        T operator*(const vec3<T>& v) const;
        vec3<T> operator^(const vec3<T>& v) const;
        
        void operator=(const vec3<T>& v);
        void operator+=(const vec3<T>& v);
        void operator-=(const vec3<T>& v);
        
        void operator+=(T v);
        void operator-=(T v);
        void operator*=(T v);
        void operator/=(T v);
        
        bool operator==(const vec3<T>& v) const;
        bool operator!=(const vec3<T>& v) const;
        bool operator<(const vec3<T>& v) const;
        bool operator<=(const vec3<T>& v) const;
        bool operator>(const vec3<T>& v) const;
        bool operator>=(const vec3<T>& v) const;
            
        T length() const;
        T dist(const vec3<T>& v) const;
        void normalize();
        static vec3<T> normalize(const vec3<T>& v);
        static vec3<T> cross(const vec3<T>& v1, const vec3<T>& v2);
    };
}

namespace std {
    template<typename T>
    struct hash<ktf::vec3<T>> {
        size_t operator() (const ktf::vec3<T>& v) const;
    };
}

#include "vec3.cpp"
