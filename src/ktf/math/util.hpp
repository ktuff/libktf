#pragma once

#include <stdint.h>

namespace ktf {

int64_t factorial(int64_t n);
int64_t binomial(int64_t n, int64_t k);

};
