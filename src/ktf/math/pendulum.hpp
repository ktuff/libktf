#pragma once

namespace ktf {

class pendulum {

	private:
		constexpr static const double g = 9.81;
		double const m_l;
		double const m_m;
		double const m_d;
		double const m_u;
		double m_phi;
		double m_omega;
		double m_t;


	public:
		pendulum(double length, double mass, double offset, double damping);


	public:
		void step(double dt);
		void push(double p);
		double get_phi() const;
};

} // namespace ktf
