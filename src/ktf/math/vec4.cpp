#include "vec4.hpp"

template<typename T> constexpr ktf::vec4<T>::vec4() : x(0), y(0), z(0), w(0) {
}

template<typename T> constexpr ktf::vec4<T>::vec4(T f) : x(f), y(f), z(f), w(f) {
}

template<typename T> constexpr ktf::vec4<T>::vec4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {
}

template<typename T> constexpr ktf::vec4<T>::vec4(const ktf::vec4<T>& v) : x(v.x), y(v.y), z(v.z), w(v.w) {
}

template<typename T>
template<typename R> constexpr ktf::vec4<T>::vec4(const ktf::vec4<R>& v) {
    x = static_cast<T>(v.x);
    y = static_cast<T>(v.y);
    z = static_cast<T>(v.z);
    w = static_cast<T>(v.w);
}

template<typename T> ktf::vec4<T>::~vec4() {
}


template<typename T> T & ktf::vec4<T>::operator[](unsigned int index) {
    switch(index) {
        default:
        case 0: return x;
        case 1: return y;
        case 2: return z;
        case 3: return w;
    }
}

template<typename T> T const & ktf::vec4<T>::operator[](unsigned int index) const {
    switch(index) {
        default:
        case 0: return x;
        case 1: return y;
        case 2: return z;
        case 3: return w;
    }
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::operator-() const {
    return vec4<T>(-x, -y, -z, -w);
}


template<typename T> ktf::vec4<T> ktf::vec4<T>::operator+(T f) const {
    return vec4<T>(x+f, y+f, z+f, w+f);
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::operator-(T f) const {
    return vec4<T>(x-f, y-f, z-f, w-f);
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::operator*(T f) const {
    return vec4<T>(x*f, y*f, z*f, w*f);
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::operator/(T f) const {
    return vec4<T>(x/f, y/f, z/f, w/f);
}


template<typename T> ktf::vec4<T> ktf::vec4<T>::operator+(const ktf::vec4<T>& v) const {
    return vec4<T>(x+v.x, y+v.y, z+v.z, w+v.w);
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::operator-(const ktf::vec4<T>& v) const {
    return vec4<T>(x-v.x, y-v.y, z-v.z, w-v.w);
}


template<typename T> T ktf::vec4<T>::operator*(const ktf::vec4<T>& v) const {
    return x*v.x + y*v.y + z*v.z + w*v.w;
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::operator^(const ktf::vec4<T>& v) const {
    return vec4<T>(x*v.x, y*v.y, z*v.z, w*v.w);
}


template<typename T> void ktf::vec4<T>::operator=(const ktf::vec4<T>& v) {
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
    this->w = v.w;
}

template<typename T> void ktf::vec4<T>::operator+=(const ktf::vec4<T>& v){
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
    this->w += v.w;
}

template<typename T> void ktf::vec4<T>::operator-=(const ktf::vec4<T>& v){
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
    this->w -= v.w;
}


template<typename T> void ktf::vec4<T>::operator+=(T v) {
    this->x += v;
    this->y += v;
    this->z += v;
    this->w += v;
}

template<typename T> void ktf::vec4<T>::operator-=(T v) {
    this->x -= v;
    this->y -= v;
    this->z -= v;
    this->w -= v;
}

template<typename T> void ktf::vec4<T>::operator*=(T v) {
    this->x *= v;
    this->y *= v;
    this->z *= v;
    this->w *= v;
}

template<typename T> void ktf::vec4<T>::operator/=(T v) {
    this->x /= v;
    this->y /= v;
    this->z /= v;
    this->w /= v;
}


template<typename T> bool ktf::vec4<T>::operator==(const ktf::vec4<T>& v) const {
    return x==v.x && y==v.y && z==v.z && w==v.w;
}

template<typename T> bool ktf::vec4<T>::operator!=(const ktf::vec4<T>& v) const {
    return x!=v.x || y!=v.y || z!=v.z || w!=v.w;
}

template<typename T> bool ktf::vec4<T>::operator>=(const ktf::vec4<T>& v) const {
    return x>=v.x && y>=v.y && z>=v.z && w>=v.w;
}

template<typename T> bool ktf::vec4<T>::operator<=(const ktf::vec4<T>& v) const {
    return x<=v.x && y<=v.y && z<=v.z && w<=v.w;
}


template<typename T> bool ktf::vec4<T>::operator>(const ktf::vec4<T>& v) const {
    return x>v.x && y>v.y && z>v.z && w>v.w;
}

template<typename T> bool ktf::vec4<T>::operator<(const ktf::vec4<T>& v) const {
    return x<v.x && y<v.y && z<v.z && w<v.w;
}


template<typename T> T ktf::vec4<T>::length() const {
    return std::sqrt(x*x + y*y + z*z + w*w);
}

template<typename T> T ktf::vec4<T>::dist(const ktf::vec4<T>& v) const {
    return (*this - v).length();
}

template<typename T> void ktf::vec4<T>::normalize() {
    double t = length();
    if(t == 0.0f)
        return;
    double fac = 1/t;
    x *= fac;
    y *= fac;
    z *= fac;
    w *= fac;
}

template<typename T> ktf::vec4<T> ktf::vec4<T>::normalize(const ktf::vec4<T>& v) {
    double t = v.length();
    if(t == 0.0)
        return vec4<T>(0.0);
    double fac = 1/t;
    return vec4<T>(v.x*fac, v.y*fac, v.z*fac, v.w*fac);
}


template<typename T> size_t std::hash<ktf::vec4<T>>::operator()(const ktf::vec4<T>& v) const {
    return (((std::hash<T>()(v.x)) ^ (std::hash<T>()(v.y) << 8)) ^ ((std::hash<T>()(v.z << 16)) ^ (std::hash<T>()(v.w) << 24)));
}
