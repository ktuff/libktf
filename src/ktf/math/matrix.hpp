#pragma once

#include "vec4.hpp"

namespace ktf {

template<typename T>
class matrix {

	private:
		vec4<T> cols[4];


	public:
		constexpr matrix<T>();
		constexpr matrix<T>(const T& v);
		constexpr matrix<T>(const matrix<T>& m);
		template<typename R> constexpr matrix<T>(const matrix<R>& m);
		constexpr matrix<T>(const vec4<T>& v0, const vec4<T>& v1, const vec4<T>& v2, const vec4<T>& v3);
		~matrix<T>();


	public:
		vec4<T>& operator[](unsigned int index);
		vec4<T> const& operator[](unsigned int index) const;
		matrix<T> operator+(const matrix<T>& m) const;
		matrix<T> operator-(const matrix<T>& m) const;
		matrix<T> operator*(const matrix<T>& m) const;

		matrix<T> operator+(const T& v) const;
		matrix<T> operator-(const T& v) const;
		matrix<T> operator*(const T& v) const;
		matrix<T> operator/(const T& v) const;

		matrix<T> operator!() const;
		void operator=(const matrix<T>& m);
		void operator+=(const matrix<T>& m);
		void operator-=(const matrix<T>& m);

		void operator+=(const T& v);
		void operator-=(const T& v);
		void operator*=(const T& v);
		void operator/=(const T& v);

		void operator==(const matrix<T>& m) const;
		void operator!=(const matrix<T>& m) const;
};

#include "matrix.cpp"

} // namespace ktf

