#include "frustum.hpp"

void ktf::frustum::calculate(const ktf::matrix<double>& view_space)
{
	ktf::matrix<double> vs = !view_space;
	this->planes[0] = ktf::vec4<double>::normalize((vs[3] + vs[0]));
	this->planes[1] = ktf::vec4<double>::normalize((vs[3] - vs[0]));
	this->planes[2] = ktf::vec4<double>::normalize((vs[3] - vs[1]));
	this->planes[3] = ktf::vec4<double>::normalize((vs[3] + vs[1]));
	this->planes[4] = ktf::vec4<double>::normalize((vs[3] + vs[2]));
	this->planes[5] = ktf::vec4<double>::normalize((vs[3] - vs[2]));
}

bool ktf::frustum::point_in_frustum(const ktf::vec3<double>& p) const
{
	for (const ktf::vec4<double>& plane : planes) {
		if (plane.x*p.x + plane.y*p.y + plane.z*p.z + plane.w <= 0) {
			return false;
		}
	}
	return true;
}

bool ktf::frustum::sphere_in_frustum(const ktf::vec3<double>& center, float radius) const
{
	for (const ktf::vec4<double>& plane : planes) {
		if ((plane.x*center.x + plane.y*center.y + plane.z*center.z + plane.w - radius) > 0) {
			return false;
		}
	}
	return true;
}

bool ktf::frustum::cube_in_frustum(const ktf::vec3<double>& p, int size) const
{
	for (int a=0; a<6; a++) {
		if ((planes[a].x*(p.x-size) + planes[a].y*(p.y-size) + planes[a].z*(p.z-size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x+size) + planes[a].y*(p.y-size) + planes[a].z*(p.z-size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x-size) + planes[a].y*(p.y+size) + planes[a].z*(p.z-size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x+size) + planes[a].y*(p.y+size) + planes[a].z*(p.z-size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x-size) + planes[a].y*(p.y-size) + planes[a].z*(p.z+size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x+size) + planes[a].y*(p.y-size) + planes[a].z*(p.z+size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x-size) + planes[a].y*(p.y+size) + planes[a].z*(p.z+size) + planes[a][3] > 0) ||
			(planes[a].x*(p.x+size) + planes[a].y*(p.y+size) + planes[a].z*(p.z+size) + planes[a][3] > 0))
			continue;
		return false;
	}
	return true;
}
