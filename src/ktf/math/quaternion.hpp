#pragma once

#include "matrix.hpp"
#include "vec3.hpp"

namespace ktf {

class quaternion {

	private:
		double a, b, c, d;


	public:
		quaternion();
		quaternion(quaternion && q);
		quaternion(quaternion const& q);
		quaternion(double a, double b, double c, double d);
		quaternion(ktf::vec3<double> const& u, double angle);

		quaternion operator=(quaternion && q);
		quaternion operator=(quaternion const& q);


	public:
		quaternion operator+(quaternion const& o) const;
		quaternion operator-(quaternion const& o) const;
		quaternion operator*(quaternion const& o) const;
		quaternion operator/(quaternion const& o) const;
		quaternion operator!() const;
		quaternion operator+=(quaternion const& o);
		quaternion operator-=(quaternion const& o);
		quaternion operator*=(quaternion const& o);
		quaternion operator/=(quaternion const& o);

		double norm() const;
		ktf::matrix<double> to_matrix() const;
		ktf::vec3<double> apply(ktf::vec3<double> const& p) const;
};

} // namespace ktf
