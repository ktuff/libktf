#include "vec2.hpp"

template<typename T> constexpr ktf::vec2<T>::vec2() : x(0), y(0) {
}

template<typename T> constexpr ktf::vec2<T>::vec2(T f) : x(f), y(f) {
}

template<typename T> constexpr ktf::vec2<T>::vec2(T x, T y) : x(x), y(y) {
}

template<typename T> constexpr ktf::vec2<T>::vec2(const ktf::vec2<T>& v) : x(v.x), y(v.y) {
}

template<typename T> template<typename R> constexpr ktf::vec2<T>::vec2(const ktf::vec2<R>& v) {
    this->x = static_cast<T>(v.x);
    this->y = static_cast<T>(v.y);
}

template<typename T> ktf::vec2<T>::~vec2() {
}


template<typename T> T & ktf::vec2<T>::operator[](unsigned int index) {
    switch(index) {
        default:
        case 0: return x;
        case 1: return y;
    }
}

template<typename T> T const & ktf::vec2<T>::operator[](unsigned int index) const {
    switch(index) {
        default:
        case 0: return x;
        case 1: return y;
    }
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator-() const {
    return vec2<T>(-x, -y);
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator+(T f) const {
    return vec2<T>(x+f, y+f);
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator-(T f) const {
    return vec2<T>(x-f, y-f);
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator*(T f) const {
    return vec2<T>(x*f, y*f);
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator/(T f) const {
    return vec2<T>(x/f, y/f);
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator+(const ktf::vec2<T>& v) const {
    return vec2<T>(x+v.x, y+v.y);
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator-(const ktf::vec2<T>& v) const {
    return vec2<T>(x-v.x, y-v.y);
}

template<typename T> T ktf::vec2<T>::operator*(const ktf::vec2<T>& v) const {
    return x*v.x + y*v.y;
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::operator^(const ktf::vec2<T>& v) const {
    return vec2<T>(x*v.x, y*v.y);
}

template<typename T> void ktf::vec2<T>::operator=(const ktf::vec2<T>& v) {
    x = v.x;
    y = v.y;
}

template<typename T> void ktf::vec2<T>::operator+=(const ktf::vec2<T>& v) {
    x += v.x;
    y += v.y;
}

template<typename T> void ktf::vec2<T>::operator-=(const ktf::vec2<T>& v) {
    x -= v.x;
    y -= v.y;
}

template<typename T> void ktf::vec2<T>::operator+=(T v) {
    x += v;
    y += v;
}

template<typename T> void ktf::vec2<T>::operator-=(T v) {
    x -= v;
    y -= v;
}

template<typename T> void ktf::vec2<T>::operator*=(T v) {
    x *= v;
    y *= v;
}

template<typename T> void ktf::vec2<T>::operator/=(T v) {
    x /= v;
    y /= v;
}

template<typename T> bool ktf::vec2<T>::operator==(const ktf::vec2<T>& v) const {
    return x==v.x && y==v.y;
}

template<typename T> bool ktf::vec2<T>::operator!=(const ktf::vec2<T>& v) const {
    return x!=v.x || y!=v.y;
}

template<typename T> bool ktf::vec2<T>::operator<=(const ktf::vec2<T>& v) const {
    return x<=v.x && y<=v.y;
}

template<typename T> bool ktf::vec2<T>::operator>=(const ktf::vec2<T>& v) const {
    return x>=v.x && y>=v.y;
}

template<typename T> bool ktf::vec2<T>::operator>(const ktf::vec2<T>& v) const {
    return x>v.x && y>v.y;
}

template<typename T> bool ktf::vec2<T>::operator<(const ktf::vec2<T>& v) const {
    return x<v.x && y<v.y;
}

template<typename T> T ktf::vec2<T>::length() const {
    return std::sqrt(x*x + y*y);
}

template<typename T> T ktf::vec2<T>::dist(const ktf::vec2<T>& v) const {
    return (*this - v).length();
}

template<typename T> void ktf::vec2<T>::normalize() {
    double t = length();
    if(t == 0.0f)
        return;
    double fac = 1/t;
    x *= fac;
    y *= fac;
}

template<typename T> ktf::vec2<T> ktf::vec2<T>::normalize(const ktf::vec2<T>& v) {
    double t = v.length();
    if(t == 0.0)
        return vec2<T>(0.0);
    double fac = 1/t;
    return vec2<T>(v.x*fac, v.y*fac);
}


template<typename T> size_t std::hash<ktf::vec2<T>>::operator()(const ktf::vec2<T>& v) const {
    return ((hash<T>()(v.x)) ^ (hash<T>()(v.y) << 16));
}
