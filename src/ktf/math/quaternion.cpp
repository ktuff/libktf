#include <cmath>

#include "quaternion.hpp"

ktf::quaternion::quaternion()
: a(0.0)
, b(0.0)
, c(0.0)
, d(0.0)
{
}

ktf::quaternion::quaternion(ktf::quaternion && q)
: a(q.a)
, b(q.b)
, c(q.c)
, d(q.d)
{
}

ktf::quaternion::quaternion(const ktf::quaternion& q)
: a(q.a)
, b(q.b)
, c(q.c)
, d(q.d)
{
}

ktf::quaternion::quaternion(double a, double b, double c, double d)
: a(a)
, b(b)
, c(c)
, d(d)
{
}

ktf::quaternion::quaternion(const ktf::vec3<double>& u, double angle)
: a(std::cos(angle / 2.0))
, b(u.x * std::sin(angle/2.0))
, c(u.y * std::sin(angle/2.0))
, d(u.z * std::sin(angle/2.0))
{
}


ktf::quaternion ktf::quaternion::operator=(ktf::quaternion && q)
{
	this->a = q.a;
	this->b = q.b;
	this->c = q.c;
	this->d = q.d;
	return *this;
}

ktf::quaternion ktf::quaternion::operator=(const ktf::quaternion& q)
{
	this->a = q.a;
	this->b = q.b;
	this->c = q.c;
	this->d = q.d;
	return *this;
}

ktf::quaternion ktf::quaternion::operator+(const ktf::quaternion& o) const
{
	return quaternion(a+o.a, b+o.b, c+o.c, d+o.d);
}

ktf::quaternion ktf::quaternion::operator-(const ktf::quaternion& o) const
{
	return quaternion(a-o.a, b-o.b, c-o.c, d-o.d);
}

ktf::quaternion ktf::quaternion::operator*(const ktf::quaternion& o) const
{
	double qa = a*o.a - b*o.b - c*o.c - d*o.d;
	double qb = a*o.b + b*o.a + c*o.d - d*o.c;
	double qc = a*o.c - b*o.d + c*o.a + d*o.b;
	double qd = a*o.d + b*o.c - c*o.b + d*o.a;
	return quaternion(qa, qb, qc, qd);
}

ktf::quaternion ktf::quaternion::operator/(const ktf::quaternion& o) const
{
	double fac = 1.0/(o.a*o.a + o.b*o.b + o.c*o.c + o.d*o.d);
	quaternion q(o.a*fac, -o.b*fac, -o.c*fac, -o.d*fac);
	return *this * q;
}

ktf::quaternion ktf::quaternion::operator!() const
{
	return quaternion(a, -b, -c, -d);
}

ktf::quaternion ktf::quaternion::operator+=(const ktf::quaternion& o)
{
	this->a += o.a;
	this->b += o.b;
	this->c += o.c;
	this->d += o.d;
	return *this;
}

ktf::quaternion ktf::quaternion::operator-=(const ktf::quaternion& o)
{
	this->a -= o.a;
	this->b -= o.b;
	this->c -= o.c;
	this->d -= o.d;
	return *this;
}

ktf::quaternion ktf::quaternion::operator*=(const ktf::quaternion& o)
{
	*this = (*this) * o;
	return *this;
}

ktf::quaternion ktf::quaternion::operator/=(const ktf::quaternion& o)
{
	*this = (*this) / o;
	return *this;
}

double ktf::quaternion::norm() const
{
	return std::sqrt(a*a + b*b + c*c + d*d);
}

ktf::matrix<double> ktf::quaternion::to_matrix() const
{
	double s = 1.0/(norm()*norm());
	ktf::vec4<double> c4(0.0,  0.0, 0.0,  1.0);
	ktf::vec4<double> c3(2.0*s*(b*d + c*a), 2.0*s*(c*d - b*a), 1.0 - 2.0*s*(b*b + c*c), 0.0);
	ktf::vec4<double> c2(2.0*s*(b*c - d*a), 1.0 - 2.0*s*(b*b + d*d), 2.0*s*(c*d + b*a), 0.0);
	ktf::vec4<double> c1(1.0 - 2.0*s*(c*c + d*d), 2.0*s*(b*c + d*a), 2.0*s*(b*d - c*a), 0.0);
	return ktf::matrix<double>(c1, c2, c3, c4);
}

ktf::vec3<double> ktf::quaternion::apply(const ktf::vec3<double>& p) const
{
	quaternion pi(0.0, p.x, p.y, p.z);
	quaternion const& q = *this;
	quaternion r = q*pi*(!q);
	return ktf::vec3<double>(r.b, r.c, r.d);
}
