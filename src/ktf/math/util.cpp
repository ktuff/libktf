#include "util.hpp"

int64_t ktf::factorial(int64_t n)
{
    int64_t prod = 1;
    while (n > 1) {
        prod *= n;
        n--;
    }
    return prod;
}

int64_t ktf::binomial(int64_t n, int64_t k)
{
    return factorial(n) / double(factorial(k) * factorial(n - k));
}
