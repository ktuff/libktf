#include "matrix.hpp"

template<typename T>
constexpr matrix<T>::matrix()
{
	this->cols[0] = {1, 0, 0, 0};
	this->cols[1] = {0, 1, 0, 0};
	this->cols[2] = {0, 0, 1, 0};
	this->cols[3] = {0, 0, 0, 1};
}

template<typename T>
constexpr matrix<T>::matrix(const T& v)
{
	this->cols[0] = {v, 0, 0, 0};
	this->cols[1] = {0, v, 0, 0};
	this->cols[2] = {0, 0, v, 0};
	this->cols[3] = {0, 0, 0, v};
}

template<typename T>
constexpr matrix<T>::matrix(const matrix<T>& m)
: cols{m[0], m[1], m[2], m[3]}
{
}

template<typename T>
template<typename R>
constexpr matrix<T>::matrix(const matrix<R>& m)
: cols{m[0], m[1], m[2], m[3]}
{
}

template<typename T>
constexpr matrix<T>::matrix(const vec4<T>& v0, const vec4<T>& v1, const vec4<T>& v2, const vec4<T>& v3)
: cols{v0, v1, v2, v3}
{
}


template<typename T>
matrix<T>::~matrix()
{
}


template<typename T>
vec4<T>& matrix<T>::operator[](unsigned int index)
{
	return cols[index];
}

template<typename T>
const vec4<T> & matrix<T>::operator[](unsigned int index) const
{
	return cols[index];
}


template<typename T>
matrix<T> matrix<T>::operator+(const matrix<T>& m) const
{
	return matrix<T>(cols[0]+m[0], cols[1]+m[1], cols[2]+m[2], cols[3]+m[3]);
}

template<typename T>
matrix<T> matrix<T>::operator-(const matrix<T>& m) const
{
	return matrix<T>(cols[0]-m[0], cols[1]-m[1], cols[2]-m[2], cols[3]-m[3]);
}

template<typename T>
matrix<T> matrix<T>::operator*(const matrix<T>& m) const
{
	const vec4<T> r0(cols[0][0], cols[1][0], cols[2][0], cols[3][0]);
	const vec4<T> r1(cols[0][1], cols[1][1], cols[2][1], cols[3][1]);
	const vec4<T> r2(cols[0][2], cols[1][2], cols[2][2], cols[3][2]);
	const vec4<T> r3(cols[0][3], cols[1][3], cols[2][3], cols[3][3]);
	T m00 = r0*m[0];
	T m01 = r0*m[1];
	T m02 = r0*m[2];
	T m03 = r0*m[3];
	T m10 = r1*m[0];
	T m11 = r1*m[1];
	T m12 = r1*m[2];
	T m13 = r1*m[3];
	T m20 = r2*m[0];
	T m21 = r2*m[1];
	T m22 = r2*m[2];
	T m23 = r2*m[3];
	T m30 = r3*m[0];
	T m31 = r3*m[1];
	T m32 = r3*m[2];
	T m33 = r3*m[3];
	const vec4<T> c0(m00, m10, m20, m30);
	const vec4<T> c1(m01, m11, m21, m31);
	const vec4<T> c2(m02, m12, m22, m32);
	const vec4<T> c3(m03, m13, m23, m33);
	return matrix<T>(c0, c1, c2, c3);
}


template<typename T>
matrix<T> matrix<T>::operator+(const T& v) const
{
	return matrix<T>(cols[0]+v, cols[1]+v, cols[2]+v, cols[3]+v);
}

template<typename T>
matrix<T> matrix<T>::operator-(const T& v) const
{
	return matrix<T>(cols[0]-v, cols[1]-v, cols[2]-v, cols[3]-v);
}

template<typename T>
matrix<T> matrix<T>::operator*(const T& v) const
{
	return matrix<T>(cols[0]*v, cols[1]*v, cols[2]*v, cols[3]*v);
}

template<typename T>
matrix<T> matrix<T>::operator/(const T& v) const
{
	return matrix<T>(cols[0]/v, cols[1]/v, cols[2]/v, cols[3]/v);
}


template<typename T>
matrix<T> matrix<T>::operator!() const
{
	const vec4<T> c0(cols[0][0], cols[1][0], cols[2][0], cols[3][0]);
	const vec4<T> c1(cols[0][1], cols[1][1], cols[2][1], cols[3][1]);
	const vec4<T> c2(cols[0][2], cols[1][2], cols[2][2], cols[3][2]);
	const vec4<T> c3(cols[0][3], cols[1][3], cols[2][3], cols[3][3]);
	return matrix<T>(c0, c1, c2, c3);
}

template<typename T>
void matrix<T>::operator=(const matrix<T>& m)
{
	this->cols[0] = m[0];
	this->cols[1] = m[1];
	this->cols[2] = m[2];
	this->cols[3] = m[3];
}

template<typename T>
void matrix<T>::operator+=(const matrix<T>& m)
{
	this->cols[0] += m[0];
	this->cols[1] += m[1];
	this->cols[2] += m[2];
	this->cols[3] += m[3];
}

template<typename T>
void matrix<T>::operator-=(const matrix<T>& m)
{
	this->cols[0] -= m[0];
	this->cols[1] -= m[1];
	this->cols[2] -= m[2];
	this->cols[3] -= m[3];
}


template<typename T>
void matrix<T>::operator+=(const T& v)
{
	this->cols[0] += v;
	this->cols[1] += v;
	this->cols[2] += v;
	this->cols[3] += v;
}

template<typename T>
void matrix<T>::operator-=(const T& v)
{
	this->cols[0] -= v;
	this->cols[1] -= v;
	this->cols[2] -= v;
	this->cols[3] -= v;
}

template<typename T>
void matrix<T>::operator*=(const T& v)
{
	this->cols[0] *= v;
	this->cols[1] *= v;
	this->cols[2] *= v;
	this->cols[3] *= v;
}

template<typename T>
void matrix<T>::operator/=(const T& v)
{
	this->cols[0] /= v;
	this->cols[1] /= v;
	this->cols[2] /= v;
	this->cols[3] /= v;
}


template<typename T>
void matrix<T>::operator==(const matrix<T>& m) const
{
	return m[0] == cols[0] && m[1] == cols[1] && m[2] == cols[2] && m[3] == cols[3];
}

template<typename T>
void matrix<T>::operator!=(const matrix<T>& m) const
{
	return m[0] != cols[0] || m[1] != cols[1] || m[2] != cols[2] || m[3] != cols[3];
}
