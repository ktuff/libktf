#pragma once

#include "matrix.hpp"
#include "vec.hpp"

namespace ktf {

template<typename T>
ktf::matrix<T> translate(const ktf::matrix<T>& m, const ktf::vec3<T>& v);

template<typename T>
ktf::matrix<T> scale(const ktf::matrix<T>& m, const ktf::vec3<T>& v);

template<typename T, typename U, typename S>
ktf::matrix<T> rotate(const ktf::matrix<T>& m, const U& rad, const ktf::vec3<S>& d);

template<typename T, typename U, typename S>
ktf::matrix<T> shear(ktf::matrix<U> const& m, ktf::vec2<S> const& s1, ktf::vec2<S> const& s2, ktf::vec2<S> const& s3);

} // namespace ktf

#include "transform.cpp"
