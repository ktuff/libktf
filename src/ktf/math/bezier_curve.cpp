#include <cmath>

#include "bezier_curve.hpp"
#include "util.hpp"

ktf::bezier_curve::bezier_curve(std::vector<double> const& pts, size_t cpts)
: m_num_points(cpts)
, m_values(generate_values(pts, cpts))
{
}

ktf::bezier_curve::~bezier_curve()
{
}


size_t ktf::bezier_curve::size() const
{
    return m_num_points;
}

double ktf::bezier_curve::at(size_t index) const
{
    return m_values.at(index);
}

std::vector<double> ktf::bezier_curve::generate_values(const std::vector<double>& pts, size_t cpts) const
{
	size_t npts = pts.size();
	double dt = 1.0 / (cpts - 1);
	double t = 0;
	size_t n = npts - 1;
	std::vector<int64_t> binom_cache(npts);
	std::vector<double> values(cpts);

	for (size_t k=0; k<npts; k++) {
		binom_cache[k] = ktf::binomial(n, k);
	}
	for (size_t p=0; p<cpts; p++) {
		if (1.0 - t < 5e-8) {
			t = 1.0;
		}
		values[p] = 0.0;
		for (size_t i=0; i<npts; i++) {
			double basis = binom_cache[i] * std::pow(t, i) * std::pow(1-t, n-i);
			values[p] += basis * pts[i];
		}
		t += dt;
	}
	return values;
}
