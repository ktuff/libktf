#pragma once

#include "../math/matrix.hpp"
#include "../math/vec3.hpp"

namespace ktf {
    
class frustum {
    private: 
        ktf::vec4<double> planes[6];

    public: 
        void calculate(const ktf::matrix<double>& view_space);
        bool point_in_frustum(const ktf::vec3<double>& p) const;
        bool sphere_in_frustum(const ktf::vec3<double>& center, float radius) const;
        bool cube_in_frustum(const ktf::vec3<double>& p0, int size) const;
};

} // namespace ktf
