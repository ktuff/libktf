#pragma once

namespace ktf {
    
    template<typename T> T degree(T value);
    template<typename T> T radians(T value);
    
}

#include "radians.cpp"
