#include "radians.hpp"

template<typename T> T ktf::degree(T value)
{
    return value * 57.295779513082320876798154814105;
}

template<typename T> T ktf::radians(T value)
{
    return value * 0.01745329251994329576923690768489;
}
