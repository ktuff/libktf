#include <ktf/math/radians.hpp>
#include <ktf/math/transform.hpp>

#include "renderable.hpp"


ktf::ui::renderable::renderable(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, const std::string& content)
: widget(ui, parent, name)
, m_id((size_t)-1)
, m_model(1.0f)
, m_color(1.0f)
, m_content(content)
, m_hidden(false)
{
}

ktf::ui::renderable::~renderable()
{
}


size_t ktf::ui::renderable::id() const
{
	if (m_hidden) {
		return (size_t)-1;
	} else {
		return m_id;
	}
}

const ktf::matrix<float> & ktf::ui::renderable::model() const
{
	return m_model;
}

const ktf::vec4<float> & ktf::ui::renderable::color() const
{
	return m_color;
}

ktf::ui::renderable& ktf::ui::renderable::identity()
{
	this->m_model = ktf::matrix<float>(1.0f);
	return *this;
}

ktf::ui::renderable& ktf::ui::renderable::translate(float x, float y, float z)
{
	this->m_model = ktf::translate(m_model, ktf::vec3<float>(x, y, z));
	return *this;
}

ktf::ui::renderable& ktf::ui::renderable::scale(float x, float y, float z)
{
	this->m_model = ktf::scale(m_model, ktf::vec3<float>(x, y, z));
	return *this;
}

ktf::ui::renderable& ktf::ui::renderable::rotate(float r)
{
	this->m_model = ktf::rotate(m_model, ktf::radians(r), ktf::vec3<float>(0.0f, 0.0f, 1.0f));
	return *this;
}

ktf::ui::renderable& ktf::ui::renderable::color(const ktf::vec4<float>& c)
{
	this->m_color = c;
	return *this;
}

void ktf::ui::renderable::hide(bool value)
{
	this->m_hidden = value;
}

bool ktf::ui::renderable::visible() const
{
	return (!m_hidden) && (m_id != (size_t)-1);
}
