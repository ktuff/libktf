#include "gui.hpp"
#include "sprite.hpp"
#include "text.hpp"
#include "widget.hpp"

char const * const group_all = "widgets_all";
char const * const group_text = "widgets_text";
char const * const group_sprite = "widgets_sprite";


ktf::ui::gui::gui()
: m_root(this, nullptr, "root", 0)
{
	this->m_groups.emplace(group_all, std::make_shared<ktf::ui::group>(group_all));
	this->m_groups.emplace(group_text, std::make_shared<ktf::ui::group>(group_text));
	this->m_groups.emplace(group_sprite, std::make_shared<ktf::ui::group>(group_sprite));
}

ktf::ui::gui::~gui()
{
	this->remove_all();
}


ktf::ui::widget * ktf::ui::gui::get_root()
{
	return &m_root;
}

void ktf::ui::gui::add_widget(std::shared_ptr<ktf::ui::widget> widget, bool free)
{
	if (m_widgets.find(widget->m_name) == m_widgets.end()) {
		this->m_widgets.emplace(widget->m_name, widget);
		this->m_groups.at(group_all)->add(widget.get());
		if (ktf::ui::text* tw = dynamic_cast<ktf::ui::text*>(widget.get())) {
			this->m_groups.at(group_text)->add(tw);
		} else if (ktf::ui::sprite* sw = dynamic_cast<ktf::ui::sprite*>(widget.get())) {
			this->m_groups.at(group_sprite)->add(sw);
		}
		if (free) {
			widget->set_parent(nullptr);
			this->m_free_nodes.emplace(widget.get());
		}
	}
}

std::shared_ptr<ktf::ui::widget> ktf::ui::gui::find(const std::string& name) const
{
	auto it = m_widgets.find(name);
	if (it != m_widgets.end()) {
		return it->second;
	} else {
		return nullptr;
	}
}

std::shared_ptr<ktf::ui::widget> ktf::ui::gui::find(uint32_t id) const
{
	for (auto& [key, value] : m_widgets) {
		if (value->m_id == id) {
			return value;
		}
	}
	return nullptr;
}

void ktf::ui::gui::remove_widget(const std::string& name)
{
	auto it = m_widgets.find(name);
	if (it != m_widgets.end()) {
		this->m_groups.at(group_all)->remove(it->second.get());
		if (ktf::ui::text* tw = dynamic_cast<ktf::ui::text*>(it->second.get())) {
			this->m_groups.at(group_text)->remove(tw);
		} else if (ktf::ui::sprite* tw = dynamic_cast<ktf::ui::sprite*>(it->second.get())) {
			this->m_groups.at(group_sprite)->remove(tw);
		}
		this->m_free_nodes.erase(m_widgets.at(name).get());
		this->m_widgets.erase(name);
	}
}

void ktf::ui::gui::remove_all()
{
	this->m_free_nodes.clear();
	this->m_groups.clear();
	this->m_widgets.clear();

	this->m_groups.emplace(group_all, std::make_shared<ktf::ui::group>(group_all));
	this->m_groups.emplace(group_text, std::make_shared<ktf::ui::group>(group_text));
	this->m_groups.emplace(group_sprite, std::make_shared<ktf::ui::group>(group_sprite));
}

void ktf::ui::gui::set_free(std::string const& name, bool value)
{
	std::shared_ptr<ktf::ui::widget> node = find(name);
	if (node) {
		if (value) {
			node->set_parent(nullptr);
			this->m_free_nodes.emplace(node.get());
		} else {
			this->m_free_nodes.erase(node.get());
		}
	}
}

void ktf::ui::gui::create_group(const std::string& name)
{
	if (m_groups.find(name) == m_groups.end()) {
		this->m_groups.emplace(name, std::make_shared<group>(name));
	}
}

void ktf::ui::gui::destroy_group(const std::string& name)
{
	if (name == group_all || name == group_text || name == group_sprite) {
		return;
	}
	if (m_groups.find(name) != m_groups.end()) {
		this->m_groups.erase(name);
	}
}

std::shared_ptr<ktf::ui::group> ktf::ui::gui::get_group(const std::string& name)
{
	auto it = m_groups.find(name);
	if (it != m_groups.end()) {
		return it->second;
	} else {
		return nullptr;
	}
}

void ktf::ui::gui::resize(int width, int height)
{
	this->m_root.resize(0.0f, 0.0f, 0.0f, 0.0f, width, height, 0.0f, true);
	for (ktf::ui::widget* w : m_free_nodes) {
		w->resize(0.0f, 0.0f, 0.0f, 0.0f, width, height, 0.0f, true);
	}
}

void ktf::ui::gui::on_focus_event(ktf::event* event)
{
	this->m_root.on_focus_event(event);
}

void ktf::ui::gui::on_drop_event(ktf::event* event)
{
	this->m_root.on_drop_event(event);
}

void ktf::ui::gui::on_key_event(ktf::event* event)
{
	this->m_root.on_key_event(event);
}

void ktf::ui::gui::on_char_event(ktf::event* event)
{
	this->m_root.on_char_event(event);
}

void ktf::ui::gui::on_click_event(ktf::event* event)
{
	this->m_root.on_click_event(event);
}

void ktf::ui::gui::on_cursor_pos_event(ktf::event* event)
{
	this->m_root.on_cursor_pos_event(event);
}

void ktf::ui::gui::on_scroll_event(ktf::event* event)
{
	this->m_root.on_scroll_event(event);
}
