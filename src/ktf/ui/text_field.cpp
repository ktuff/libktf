#include <GLFW/glfw3.h>

#include <ktf/opengl/window.hpp>
#include <ktf/util/string.hpp>

#include "gui.hpp"
#include "text_field.hpp"

ktf::ui::text_field::text_field(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, font_alloc const& allocator)
: interactable(ui, parent, name)
, m_selected(-1)
, m_active(false)
{
	this->m_content = std::make_shared<ktf::ui::text>(ui, this, name + ":text", allocator);
	ui->add_widget(m_content);
}

ktf::ui::text_field::~text_field()
{
}


std::string ktf::ui::text_field::get_text() const
{
	return m_content->get_content();
}

void ktf::ui::text_field::set_text(const std::string& text)
{
	this->m_codepoints = ktf::to_utf32(text);
	this->m_content->set_content(text);
}

void ktf::ui::text_field::handle_key_event(ktf::event* event)
{
	if (!m_active) {
		return;
	}

	ktf::key_event *kev = static_cast<ktf::key_event*>(event);

	if (kev->action == GLFW_PRESS || kev->action == GLFW_REPEAT) {
		bool modified = false;
		if (kev->mods & GLFW_MOD_CONTROL) {
			if (kev->key == GLFW_KEY_A) {
				this->m_selected[0] = 0;
				this->m_selected[1] = m_codepoints.size();
				event->handled = true;
			} else if (kev->key == GLFW_KEY_X) {
				std::vector<uint32_t> selected_codepoints;
				for (int i=m_selected[0]; i<m_selected[1]; ++i) {
					selected_codepoints.push_back(m_codepoints[i]);
				}
				this->m_codepoints.erase(m_codepoints.begin() + m_selected[0], m_codepoints.begin() + m_selected[1]);
				std::vector<char> selected_string = ktf::to_utf8(selected_codepoints);
				kev->pwindow->copy_to_clipboard(std::string(selected_string.data(), selected_string.size()));
				this->m_selected = ktf::vec2<int>(-1);
				modified = true;
				event->handled = true;
			} else if (kev->key == GLFW_KEY_C) {
				std::vector<uint32_t> selected_codepoints;
				for (int i=m_selected[0]; i<m_selected[1]; ++i) {
					selected_codepoints.push_back(m_codepoints[i]);
				}
				std::vector<char> selected_string = ktf::to_utf8(selected_codepoints);
				kev->pwindow->copy_to_clipboard(std::string(selected_string.data(), selected_string.size()));
				event->handled = true;
			} else if (kev->key == GLFW_KEY_V) {
				std::string content = kev->pwindow->copy_from_clipboard();
				if (content != "") {
					std::vector<uint32_t> codepoints = ktf::to_utf32(std::string(content));
					for (uint32_t codepoint : codepoints) {
						this->m_codepoints.push_back(codepoint);
					}
					this->m_selected = ktf::vec2<int>(-1);
					modified = true;
				}
				event->handled = true;
			} else if (kev->key == GLFW_KEY_BACKSPACE) {
				this->m_selected = ktf::vec2<int>(-1);
				this->m_codepoints.clear();
				modified = true;
				event->handled = true;
			}
		} else if (kev->key == GLFW_KEY_BACKSPACE) {
			if (!m_codepoints.empty()) {
				if (m_selected[0] != -1 && m_selected[1] != -1) {
					this->m_codepoints.erase(m_codepoints.begin() + m_selected[0], m_codepoints.begin() + m_selected[1]);
					this->m_selected = ktf::vec2<int>(-1);
				} else {
					this->m_codepoints.pop_back();
				}
				modified = true;
			}
			event->handled = true;
		} else if (kev->key == GLFW_KEY_TAB) {
			if (auto next = m_next.lock()) {
				this->m_active = false;
				next->m_active = true;
				event->handled = true;
			}
		} else if (kev->key == GLFW_KEY_ENTER) {
			// TODO finish event
			event->handled = true;
		}
		if (modified) {
			std::vector<char> utf8 = ktf::to_utf8(m_codepoints);
			this->m_content->set_content(std::string(utf8.data(), utf8.size()));
		}
	}
	interactable::handle_key_event(event);
}

void ktf::ui::text_field::handle_char_event(ktf::event* event)
{
	if (!m_active) {
		return;
	}

	ktf::key_event *kev = static_cast<ktf::key_event*>(event);

	if (m_char_filter.empty() || m_char_filter.find(kev->codepoint) != m_char_filter.end()) {
		if (m_selected[0] != -1 && m_selected[1] != -1) {
			this->m_codepoints.erase(m_codepoints.begin() + m_selected[0], m_codepoints.begin() + m_selected[1]);
			this->m_codepoints.insert(m_codepoints.begin() + m_selected[0], kev->codepoint);
			this->m_selected = ktf::vec2<int>(-1);
		} else {
			this->m_codepoints.push_back(kev->codepoint);
		}
		std::vector<char> ch = ktf::to_utf8({kev->codepoint});
		this->m_content->set_content(m_content->get_content() + std::string(ch.data(), ch.size()));
	}
	this->m_selected = ktf::vec2<int>(-1);
	interactable::handle_char_event(event);
}

void ktf::ui::text_field::handle_click_event(ktf::event* event)
{
	ktf::mouse_event *mev = static_cast<ktf::mouse_event*>(event);

	if (mev->action == GLFW_PRESS) {
		this->m_active = in_frame(mev->x, mev->y);
	}
}

void ktf::ui::text_field::set_next(std::weak_ptr<ktf::ui::text_field> const& next_cb)
{
	this->m_next = next_cb;
}
