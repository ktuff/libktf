#pragma once

#include "interactable.hpp"

namespace ktf {

namespace ui {

class drop_area : public interactable {

	private:
		std::vector<std::string> m_content;


	public:
		drop_area(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name);
		~drop_area();


	public:
		void clear();
		void handle_drop_event(ktf::event* event) override;
};

} // namespace ui

} // namespace ktf
