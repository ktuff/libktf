#include "group.hpp"
#include "widget.hpp"

bool ktf::ui::group::layer_order::operator()(const ktf::ui::widget* w1, const ktf::ui::widget* w2) const
{
	if (w1->r_layer == w2->r_layer) {
		return w1->m_id < w2->m_id;
	} else {
		return w1->r_layer < w2->r_layer;
	}
}


ktf::ui::group::group(const std::string& name)
: m_name(name)
{
}

ktf::ui::group::~group()
{
	while (!m_members.empty()) {
		this->remove(*m_members.begin());
	}
}


void ktf::ui::group::add(ktf::ui::widget* widget)
{
	if (m_members.find(widget) == m_members.end()) {
		this->m_members.emplace(widget);
		widget->add_to_group(this);
	}
}

void ktf::ui::group::remove(ktf::ui::widget* widget)
{
	if (m_members.find(widget) != m_members.end()) {
		this->m_members.erase(widget);
		widget->remove_from_group(this);
	}
}

const std::set<ktf::ui::widget *, ktf::ui::group::layer_order> ktf::ui::group::get_members() const
{
	return m_members;
}
