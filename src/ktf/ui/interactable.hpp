#pragma once

#include <functional>
#include "widget.hpp"

namespace ktf {

namespace ui {

union keyt {
	uint64_t value;
	struct {
		uint64_t key : 32;
		uint64_t mods : 6;
		uint64_t action : 2;
		uint64_t _: 24;
	};

	keyt(int key, int action, int mods);
};


class interactable : public widget {

	protected:
		std::function<void(ktf::event* event)> m_focus_action;
		std::function<void(ktf::event* event)> m_drop_action;
		std::function<void(ktf::event* event)> m_click_action;
		std::function<void(ktf::event* event)> m_cursor_pos_action;
		std::function<void(ktf::event* event)> m_scroll_action;
		std::function<void(ktf::event* event)> m_key_action;
		std::function<void(ktf::event* event)> m_char_action;
		std::unordered_map<uint64_t, std::function<void(void)>> m_key_map;


	public:
		interactable(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, int layer = -1);
		virtual ~interactable();


	public:
		void reset_actions();
		void set_action(event_type index, std::function<void(ktf::event* event)> action);
		void add_key(uint64_t key, std::function<void(void)> const& action);
		void remove_key(uint64_t key);

	protected:
		virtual void handle_focus_event(ktf::event* event) override;
		virtual void handle_drop_event(ktf::event* event) override;
		virtual void handle_key_event(ktf::event* event) override;
		virtual void handle_char_event(ktf::event* event) override;
		virtual void handle_click_event(ktf::event* event) override;
		virtual void handle_position_event(ktf::event* event) override;
		virtual void handle_scroll_event(ktf::event* event) override;
};

} // namespace ui

} // namespace ktf
