#include "drop_area.hpp"

ktf::ui::drop_area::drop_area(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name)
: interactable(ui, parent, name)
{
}

ktf::ui::drop_area::~drop_area()
{
}


void ktf::ui::drop_area::clear()
{
	this->m_content.clear();
}

void ktf::ui::drop_area::handle_drop_event(ktf::event* event)
{
	ktf::drop_event* e = static_cast<ktf::drop_event*>(event);
	for (auto const& p : e->paths) {
		this->m_content.push_back(p);
	}
}
