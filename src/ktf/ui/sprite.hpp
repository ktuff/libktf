#pragma once

#include "renderable.hpp"

namespace ktf {

namespace ui {

class sprite : public renderable {

	private:
		std::function<size_t(std::string const&)> m_loadf;


	public:
		sprite(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, std::function<size_t(std::string const&)> const& loadf, std::string const& content = "");
		~sprite();


	public:
		void on_resize() override;
		void load() override;
		void set_content(std::string const& content);
};

} // namespace ui

} // namespace ktf
