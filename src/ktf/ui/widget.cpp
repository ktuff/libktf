#include <ktf/math/radians.hpp>
#include <ktf/math/transform.hpp>

#include "gui.hpp"
#include "group.hpp"
#include "interactable.hpp"
#include "widget.hpp"
#include "text.hpp"

size_t ktf::ui::widget::s_next_id = 1;


bool ktf::ui::widget::widget_order::operator()(ktf::ui::widget* w1, ktf::ui::widget* w2) const
{
	if (w1->m_layout_position == w2->m_layout_position) {
		return w1->m_id < w2->m_id;
	} else {
		return w1->m_layout_position < w2->m_layout_position;
	}
}


ktf::ui::widget::size_policy::size_policy()
: m_type(EXPANDING)
, value(1.0f)
{
}

ktf::ui::widget::size_policy::size_policy(ktf::ui::widget::size_policy::policy_type type, float value)
: m_type(type)
, value(value)
{
}

void ktf::ui::widget::size_policy::apply(float& w, float& h) const
{
	float rw = h * value;
	float rh = w / value;
	switch (m_type) {
	case FIXED_RATIO:
		if (rw > w) {
			h = rh;
		} else {
			w = rw;
		}
		break;
	case OVERSIZE:
		if (rw < w) {
			h = rh;
		} else {
			w = rw;
		}
		break;
	case EXPANDING:
		break;
	case EXPANDING_WIDTH:
		if (rh > h) {
		} else {
			h = rh;
		}
		break;
	case EXPANDING_HEIGHT:
		if (rw > w) {
		} else {
			w = rw;
		}
		break;
	}
}


ktf::ui::widget::layout::layout(ktf::ui::widget::layout::layout_type type)
: m_type(type)
{
}

void ktf::ui::widget::layout::apply(float x, float y, float w, float h, float rotation, bool force, const std::set<ktf::ui::widget*, widget_order>& widgets) const
{
	// resize children according to layout
	switch (m_type) {
	case HORIZONTAL: {
		float total = 0.0f;
		int unscaled = 0;
		float offset = 0.0f;
		for (auto* child : widgets) {
			if (child->m_frame.x == 1.0f) {
				unscaled++;
			} else {
				total += child->m_frame.x;
			}
		}
		for (auto* child : widgets) {
			float wf = (child->m_frame.x == 1.0f) ? ((1.0f-total)*w / unscaled) : w * child->m_frame.x;
			wf = std::min(wf, w - offset);
			child->resize(x, y, x + offset + wf/2 - w/2.0, y, wf, h * child->m_frame.y, rotation, force);
			offset += wf;
		}
		break;
	}
	case VERTICAL: {
		float total = 0.0f;
		int unscaled = 0;
		float offset = 0.0f;
		for (auto* child : widgets) {
			if (child->m_frame.y == 1.0f) {
				unscaled++;
			} else {
				total += child->m_frame.y;
			}
		}
		for (auto* child : widgets) {
			float hf = (child->m_frame.y == 1.0f) ? ((1.0f - total) * h / unscaled) : h * child->m_frame.y;
			hf = std::min(hf, h - offset);
			child->resize(x, y, x, y + offset + hf/2 - h/2.0, w * child->m_frame.x, hf, rotation, force);
			offset += hf;
		}
		break;
	}
	case FREE: {
		for (auto* child : widgets) {
			child->resize(x, y, x, y, w * child->m_frame.x, h * child->m_frame.y, rotation, force);
		}
		break;
	}
	}
}


ktf::ui::widget::offset::offset()
: offset(PIXEL, 0, 0)
{
}

ktf::ui::widget::offset::offset(offset::size_type type, float x, float y)
: m_type(type)
, val(x, y)
{
}

void ktf::ui::widget::offset::apply(float& x, float& y, float w, float h, float fx, float fy, float fw, float fh) const
{
	if (m_type == PIXEL) {
		x += val.x;
		y += val.y;
	} else {
		x += val.x * fw;
		y += val.y * fh;
	}

	bool overlap_xn = x - w/2.0f < fx - fw/2.0f;
	bool overlap_xp = x + w/2.0f > fx + fw/2.0f;
	bool overlap_yn = y - h/2.0f < fy - fh/2.0f;
	bool overlap_yp = y + h/2.0f > fy + fh/2.0f;
	if (overlap_xn && overlap_xp) {
		// ignore, overlap in both directions
	} else if (overlap_xn) {
		x = fx - fw/2.0f + w/2.0f;
	} else if (overlap_xp) {
		x = fx + fw/2.0f - w/2.0f;
	} else {
	}
	if (overlap_yn && overlap_yp) {
		// ignore, overlap in both directions
	} else if (overlap_yn) {
		y = fy - fh/2.0f + h/2.0f;
	} else if (overlap_yp) {
		y = fy + fh/2.0f - h/2.0f;
	} else {
	}
}


ktf::ui::widget::padding::padding()
: padding(PIXEL, 0, 0, 0, 0)
{
}

ktf::ui::widget::padding::padding(size_type type, float left, float right, float top, float bottom)
: m_type(type)
, m_val(left, right, top, bottom)
{
}

void ktf::ui::widget::padding::apply(float& x, float& y, float& wp, float& wn, float& hp, float& hn) const
{
	if (m_type == PIXEL) {
		wp -= m_val.x;
		wn += m_val.y;
		hp -= m_val.z;
		hn += m_val.w;
	} else {
		float w = wp - wn;
		float h = hp - hn;
		wp -= w * m_val.x;
		wn += w * m_val.y;
		hp -= h * m_val.z;
		hn += h * m_val.w;
	}
	float w = wp - wn;
	float h = hp - hn;
	x = x + wn + w / 2.0f;
	y = y + hn + h / 2.0f;
	wp =  w / 2.0;
	wn = -w / 2.0;
	hp =  h / 2.0;
	hn = -h / 2.0;
}




ktf::ui::widget::widget(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, int layer)
: m_id(widget::next_id())
, m_name(name)
, m_ui(ui)
, m_parent(parent)
, m_layer(layer)
, m_layer_offset(0)
, m_layout_position(0)
, m_pos(0.0f)
, m_dim(0.0f)
, m_rotation(0.0f)
, m_scale(1.0f)
, m_old_in_rotation(0.0f)
, m_local_rotation(0.0f)
, m_frame(1.0f)
, m_layout(layout::FREE)
, m_enabled(true)
, m_state(0)
{
	if (m_parent) {
		this->m_parent->add_child(this);
	}
}

ktf::ui::widget::~widget()
{
	if (m_parent) {
		this->m_parent->remove_child(this);
	}
	for (widget* child : m_children) {
		child->m_parent = nullptr;
	}
	for (group * g : m_groups) {
		g->remove(this);
	}
}


size_t ktf::ui::widget::next_id()
{
	size_t id = widget::s_next_id;
	widget::s_next_id += 1;
	return id;
}

void ktf::ui::widget::set_parent(ktf::ui::widget* widget)
{
	if (widget == this) {
		return;
	}
	if (m_parent) {
		this->m_parent->remove_child(this);
	}
	this->m_parent = widget;
	if (widget) {
		widget->add_child(this);
	}
}

void ktf::ui::widget::add_child(ktf::ui::widget* widget)
{
	// prevent circular structures
	if (widget->m_layer == -1) {
		if (widget->m_parent) {
			widget->m_parent->remove_child(widget);
		}
		this->m_children.emplace(widget);
		widget->m_parent = this;
		widget->m_layer = m_layer + 1;

		widget->resize(m_pos.x, m_pos.y, m_pos.x, m_pos.y, m_dim.x, m_dim.y, m_rotation, true);
	}
}

void ktf::ui::widget::remove_child(ktf::ui::widget* widget)
{
	if (m_children.find(widget) != m_children.end()) {
		this->m_children.erase(widget);
		widget->m_parent = nullptr;
		widget->m_layer = -1;
	}
}

void ktf::ui::widget::add_to_group(ktf::ui::group* group)
{
	if (m_groups.find(group) != m_groups.end()) {
		this->m_groups.emplace(group);
		group->add(this);
	}
}

void ktf::ui::widget::remove_from_group(ktf::ui::group* group)
{
	if (m_groups.find(group) == m_groups.end()) {
		this->m_groups.erase(group);
		group->remove(this);
	}
}

ktf::ui::widget * ktf::ui::widget::find(const std::string& name)
{
	for (auto* child : m_children) {
		if (child->m_name == name) {
			return child;
		}
	}
	for (auto* child : m_children) {
		if (auto* widget = child->find(name)) {
			return widget;
		}
	}
	return nullptr;
}

void ktf::ui::widget::resize()
{
	// TODO correct parameters
	this->resize(m_old_in_pos.x, m_old_in_pos.y, m_old_in_pos.x, m_old_in_pos.y, m_old_in_dim.x, m_old_in_dim.y, m_old_in_rotation, true);
}

void ktf::ui::widget::resize(float px, float py, float x, float y, float w, float h, float r, bool force)
{
	bool force_or_change = force || m_old_in_pos.x != x || m_old_in_pos.y != y || m_old_in_dim.x != w || m_old_in_dim.y != h || m_old_in_rotation != r;
	if (!force_or_change) {
		return;
	}

	if (m_copy != "") {
		if (auto temp = m_ui->find(m_copy)) {
			this->m_pos = temp->m_pos;
			this->m_dim = temp->m_dim;
			this->m_rotation = temp->m_rotation;
		} else {
			this->m_pos = ktf::vec2<float>(0.0f);
			this->m_dim = ktf::vec2<float>(0.0f);
			this->m_rotation = 0.0f;
		}
		this->on_resize();
		return;
	}

	// calculate new size
	float nx = x;
	float ny = y;
	float nw = w;
	float nh = h;
	ktf::ui::text* t = dynamic_cast<ktf::ui::text*>(this);
	if (t && t->font_size() != 0) {
		// skip resizing and use text size
		nw = m_dim.x;
		nh = m_dim.y;
	} else {
		this->m_size_policy.apply(nw, nh);
		float wp =  nw / 2.0;
		float wn = -nw / 2.0;
		float hp =  nh / 2.0;
		float hn = -nh / 2.0;
		this->m_padding.apply(nx, ny, wp, wn, hp, hn);
		nw = wp - wn;
		nh = hp - hn;
	}

	nw *= m_scale;
	nh *= m_scale;

	// set position
	this->m_offset.apply(nx, ny, nw, nh, x, y, w, h);

	// rotation and repositioning
	if (r != 0.0f) {
		float dx = nx - px;
		float dy = ny - py;
		float rad = ktf::radians(r);
		nx = px + std::cos(rad) * dx - std::sin(rad) * dy;
		ny = py + std::cos(rad) * dy + std::sin(rad) * dx;
	}

	float cumulated_rotation = r + m_local_rotation;
	this->m_pos = ktf::vec2<float>(nx, ny);
	this->m_dim = ktf::vec2<float>(nw, nh);
	this->m_rotation = cumulated_rotation;

	// resize children
	if (!m_children.empty()) {
		this->m_layout.apply(nx, ny, nw, nh, cumulated_rotation, force, m_children);
	}

	this->on_resize();

	this->m_old_in_pos = m_pos;
	this->m_old_in_dim = m_dim;
	this->m_old_in_rotation = m_rotation;
}

bool ktf::ui::widget::in_frame(int px, int py) const
{
	float hw = m_dim.x * 0.5f;
	float hh = m_dim.y * 0.5f;
	float x_off = px - m_pos.x;
	float y_off = py - m_pos.y;
	float sqdist = x_off*x_off + y_off*y_off;
	// if point is further away from center than a corner, the point cannot be in the rectangle
	if (sqdist > (hw * hw + hh * hh)) {
		return false;
	}
	// rotate point around center
	if (m_rotation != 0.0f && x_off != 0.0f && y_off != 0.0f) {
		float p_rot = ktf::degree(std::atan2(y_off, x_off));
		float rd = ktf::radians(p_rot - m_rotation);
		float radius = std::sqrt(sqdist);
		px = m_pos.x + radius * std::cos(rd);
		py = m_pos.y + radius * std::sin(rd);
	}
	return px >= (m_pos.x - m_dim.x/2)
		&& px <= (m_pos.x + m_dim.x/2)
		&& py >= (m_pos.y - m_dim.y/2)
		&& py <= (m_pos.y + m_dim.y/2);
}

void ktf::ui::widget::enable()
{
	this->m_enabled = true;
}

void ktf::ui::widget::disable()
{
	this->m_enabled = false;
}

uint64_t ktf::ui::widget::state() const
{
	return m_state;
}

void ktf::ui::widget::set_state(uint64_t state)
{
	this->m_state = state;
}

ktf::vec2<float> ktf::ui::widget::dim() const
{
	return m_dim;
}

void ktf::ui::widget::set_policy(ktf::ui::widget::size_policy pol)
{
	this->m_size_policy = pol;
}

void ktf::ui::widget::set_layout(ktf::ui::widget::layout lo)
{
	this->m_layout = lo;
}

void ktf::ui::widget::set_offset(ktf::ui::widget::offset o)
{
	this->m_offset = o;
}

void ktf::ui::widget::set_frame(const ktf::vec2<float>& frame)
{
	this->m_frame = frame;
}

void ktf::ui::widget::set_rotation(float rotation)
{
	this->m_local_rotation = rotation;
}

void ktf::ui::widget::set_scale(float scale)
{
	this->m_scale = scale;
}

void ktf::ui::widget::set_copy(const std::string& copy)
{
	this->m_copy = copy;
}

void ktf::ui::widget::set_layer_offset(int offset)
{
	this->m_layer_offset = offset;
}

void ktf::ui::widget::on_focus_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_focus_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_focus_event(event);
}

void ktf::ui::widget::on_drop_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_drop_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_drop_event(event);
}

void ktf::ui::widget::on_key_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_key_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_key_event(event);
}

void ktf::ui::widget::on_char_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_char_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_char_event(event);
}

void ktf::ui::widget::on_click_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_click_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_click_event(event);
}

void ktf::ui::widget::on_cursor_pos_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_cursor_pos_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_position_event(event);
}

void ktf::ui::widget::on_scroll_event(ktf::event* event)
{
	if (!m_enabled) {
		return;
	}

	for (auto* child : m_children) {
		child->on_scroll_event(event);
		if (event->handled) {
			return;
		}
	}
	this->handle_scroll_event(event);
}

void ktf::ui::widget::on_resize()
{
}

void ktf::ui::widget::handle_focus_event(ktf::event*)
{
}

void ktf::ui::widget::handle_drop_event(ktf::event*)
{
}

void ktf::ui::widget::handle_key_event(ktf::event*)
{
}

void ktf::ui::widget::handle_char_event(ktf::event*)
{
}

void ktf::ui::widget::handle_click_event(ktf::event*)
{
}

void ktf::ui::widget::handle_position_event(ktf::event*)
{
}

void ktf::ui::widget::handle_scroll_event(ktf::event*)
{
}
