#pragma once

#include <set>
#include <string>

namespace ktf {

namespace ui {

class widget;
struct event;

class group {

	private:
		struct layer_order {
			bool operator()(ktf::ui::widget const* w1, ktf::ui::widget const* w2) const;
		};

	private:
		std::string const m_name;
		std::set<ktf::ui::widget*, layer_order> m_members;


	public:
		group(std::string const& name);
		~group();


	public:
		void add(ktf::ui::widget* widget);
		void remove(ktf::ui::widget* widget);
		void on_click(ktf::ui::event* event);
		void on_hover(ktf::ui::event* event);
		void on_key(ktf::ui::event* event);

		std::set<ktf::ui::widget*, layer_order> const get_members() const;
};

} // namespace ui

} // namespace ktf
