#include <GLFW/glfw3.h>
#include <ktf/math/radians.hpp>

#include "gui.hpp"
#include "slider.hpp"


ktf::ui::slider::slider(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, std::shared_ptr<widget> s)
: interactable(ui, parent, name)
, m_slider(s.get())
, m_type(HORIZONTAL)
, m_value(0.5f)
, m_steps(0)
{
	if (m_slider) {
		this->add_child(m_slider);
	}
}

ktf::ui::slider::~slider()
{
}


void ktf::ui::slider::remove_child(ktf::ui::widget* widget)
{
	if (widget == m_slider) {
		this->m_slider = nullptr;
	}
}

void ktf::ui::slider::handle_click_event(ktf::event* event)
{
	ktf::mouse_event* e = static_cast<ktf::mouse_event*>(event);
	bool hovered = in_frame(e->x, e->y);
	if (hovered && e->action == GLFW_PRESS) {
		this->m_state = 2;
		this->adjust(e->x, e->y);
	} else if (e->action == GLFW_RELEASE) {
		this->m_state = (hovered ? 1 : 0);
	}
	interactable::handle_click_event(event);
}

void ktf::ui::slider::handle_position_event(ktf::event* event)
{
	ktf::mouse_event* e = static_cast<ktf::mouse_event*>(event);

	bool hovered = in_frame(e->x, e->y);
	if (m_state == 2) {
		this->adjust(e->x, e->y);
	} else if (hovered && m_state == 0) {
		this->m_state = 1;
	} else if (!hovered && m_state == 1) {
		this->m_state = 0;
	}
	interactable::handle_position_event(event);
}

void ktf::ui::slider::on_resize()
{
	float s, e;
	ktf::vec2<float> dim = m_slider ? m_slider->dim() : ktf::vec2<float>(0.0f);
	if (m_type == HORIZONTAL) {
		s = m_pos.x - m_dim.x / 2.0f + dim.x / 2.0f;
		e = m_pos.x + m_dim.x / 2.0f - dim.x / 2.0f;
	} else {
		s = m_pos.y - m_dim.y / 2.0f + dim.y / 2.0f;
		e = m_pos.y + m_dim.y / 2.0f - dim.y / 2.0f;
	}
	this->m_range = ktf::vec2<float>(s, e);

	this->set_value(m_value);
}

float ktf::ui::slider::get_value() const
{
	return m_value;
}

void ktf::ui::slider::set_value(float value)
{
	if (m_steps >= 2) {
		float step_size = 1.0f / (m_steps-1);
		int step = ::ceil((value - step_size*0.5f) / step_size);
		value = step * step_size;
	}

	this->m_value = value;
	if (m_slider) {
		float off = (m_range[1] - m_range[0]) * (m_value - 0.5f);
		if (m_type == HORIZONTAL) {
			this->m_slider->set_offset(offset(widget::offset::PIXEL, off, 0.0f));
		} else {
			this->m_slider->set_offset(offset(widget::offset::PIXEL, 0.0f, off));
		}
		this->m_slider->resize(m_pos.x, m_pos.y, m_pos.x, m_pos.y, m_dim.x, m_dim.y, m_rotation, true);
	}
}

void ktf::ui::slider::set_steps(int steps)
{
	this->m_steps = steps;
}

void ktf::ui::slider::set_type(bool horizontal)
{
	this->m_type = horizontal ? HORIZONTAL : VERTICAL;
	this->set_value(m_value);
}

ktf::ui::widget* ktf::ui::slider::get_slider()
{
	return m_slider;
}

void ktf::ui::slider::set_slider(ktf::ui::widget* widget)
{
	this->m_slider = widget;
	if (widget) {
		this->add_child(widget);
	}
}

void ktf::ui::slider::adjust(float x, float y)
{
	float mx = x;
	float my = y;
	float x_off = x - m_pos.x;
	float y_off = y - m_pos.y;
	float sqdist = x_off*x_off + y_off*y_off;
	if (m_rotation != 0.0f && x_off != 0.0f && y_off != 0.0f) {
		float p_rot = ktf::degree(std::atan2(y_off, x_off));
		float rd = ktf::radians(p_rot - m_rotation);
		float radius = std::sqrt(sqdist);
		mx = m_pos.x + radius * std::cos(rd);
		my = m_pos.y + radius * std::sin(rd);
	}
	float v = std::min(std::max((m_type == HORIZONTAL) ? mx : my, m_range[0]), m_range[1]);
	this->set_value((v - m_range[0]) / (m_range[1] - m_range[0]));
}
