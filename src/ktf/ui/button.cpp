#include <GLFW/glfw3.h>

#include "button.hpp"


ktf::ui::button::button(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name)
: interactable(ui, parent, name)
{
}

ktf::ui::button::~button()
{
}


void ktf::ui::button::handle_click_event(ktf::event* event)
{
	ktf::mouse_event* me = static_cast<ktf::mouse_event*>(event);
	bool hovered = in_frame(me->x, me->y);
	if (me->action == GLFW_PRESS && hovered && m_state != 2) {
		this->m_state = 2;
		event->handled = true;
	} else if (me->action == GLFW_RELEASE && m_state == 2) {
		this->m_state = (hovered ? 1 : 0);
		event->handled = true;
	}
	interactable::handle_click_event(event);
}

void ktf::ui::button::handle_position_event(ktf::event* event)
{
	ktf::mouse_event* me = static_cast<ktf::mouse_event*>(event);
	bool hovered = in_frame(me->x, me->y);
	if (hovered && m_state == 0) {
		this->m_state = 1;
		interactable::handle_position_event(event);
	} else if (!hovered && m_state == 1) {
		this->m_state = 0;
		interactable::handle_position_event(event);
	}
}
