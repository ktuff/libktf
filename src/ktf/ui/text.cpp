#include "text.hpp"

ktf::ui::text::text(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, ktf::ui::font_alloc const& allocator)
: renderable(ui, parent, name)
, m_size(0.0, 0.0)
, m_num_lines(0)
, m_font_size(0)
, m_font_alloc(allocator)
{
}

ktf::ui::text::~text()
{
	if (m_id != (size_t)-1) {
		this->m_font_alloc.dealloc(&m_id);
	}
}


int ktf::ui::text::font_size() const
{
	return m_font_size;
}

void ktf::ui::text::set_font_size(int size)
{
	this->m_font_size = size;
	if (m_font_size != 0.0f) {
		this->m_dim = m_size * m_font_size;
	}
	this->on_resize();
}

std::string ktf::ui::text::get_content() const
{
	return m_content;
}

void ktf::ui::text::set_content(const std::string& content)
{
	this->m_content = content;
	this->load();
}

void ktf::ui::text::on_resize()
{
	if (m_font_size != 0.0f) {
		this->identity()
			.translate(-m_size.x/2.0f, 0.0f, 0.0f)
			.scale(m_font_size, m_font_size, 1.0f)
			.rotate(m_rotation)
			.translate(m_pos.x, m_pos.y, 0.01f * (m_layer + m_layer_offset) + 0.05);
	} else {
		this->identity()
			.translate(-m_size.x/2.0f, m_size.y/5.0f, 0.0f)
			.scale(m_dim.y, m_dim.y, 1.0f)
			.rotate(m_rotation)
			.translate(m_pos.x, m_pos.y, 0.01f * (m_layer + m_layer_offset) + 0.05);
	}
}

void ktf::ui::text::load()
{
	if (m_id != (size_t)-1) {
		this->m_font_alloc.dealloc(&m_id);
		this->m_id = (size_t)-1;
	}
	if (m_content != "") {
		this->m_font_alloc.alloc(&m_id, m_content, m_size, m_num_lines, {});
		this->set_font_size(m_font_size);
		this->on_resize();
	}
}
