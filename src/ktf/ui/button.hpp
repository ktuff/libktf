#pragma once

#include "interactable.hpp"

namespace ktf {

namespace ui {

class button : public interactable {

	public:
		button(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name);
		~button();


	public:
		void handle_click_event(ktf::event * event) override;
		void handle_position_event(ktf::event * event) override;
};

} // namespace ui

} // namespace ktf

