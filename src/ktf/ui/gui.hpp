#pragma once

#include <memory>
#include <unordered_map>

#include "group.hpp"
#include "interactable.hpp"

namespace ktf {

namespace ui {

class gui {

	private:
		std::unordered_map<std::string, std::shared_ptr<ktf::ui::widget>> m_widgets;
		std::unordered_map<std::string, std::shared_ptr<ktf::ui::group>> m_groups;
		std::set<ktf::ui::widget*> m_free_nodes;
		ktf::ui::interactable m_root;


	public:
		gui();
		~gui();


	public:
		widget* get_root();
		void add_widget(std::shared_ptr<ktf::ui::widget> widget, bool free = false);
		std::shared_ptr<ktf::ui::widget> find(std::string const& name) const;
		std::shared_ptr<ktf::ui::widget> find(uint32_t id) const;
		void remove_widget(std::string const& name);
		void remove_all();
		void set_free(std::string const& name, bool value);
		void create_group(std::string const& name);
		void destroy_group(std::string const& name);
		std::shared_ptr<ktf::ui::group> get_group(std::string const& name);
		void resize(int width, int height);

		void on_focus_event(ktf::event* event);
		void on_drop_event(ktf::event* event);
		void on_click_event(ktf::event* event);
		void on_cursor_pos_event(ktf::event* event);
		void on_scroll_event(ktf::event* event);
		void on_key_event(ktf::event* event);
		void on_char_event(ktf::event* event);
};

} // namespace ui

} // namespace ktf
