#pragma once

#include <ktf/math/vec2.hpp>
#include "renderable.hpp"


namespace ktf {

namespace ui {


struct font_alloc {
	std::function<void(size_t*, std::string const&, ktf::vec2<float>&, size_t&, const std::vector<ktf::vec4<float>>&)> alloc;
	std::function<void(size_t*)> dealloc;
};


class text : public renderable {

	private:
		ktf::vec2<float> m_size;
		size_t m_num_lines;
		int m_font_size;
		font_alloc m_font_alloc;


	public:
		text(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, font_alloc const& allocator);
		~text();


	public:
		int font_size() const;
		void set_font_size(int size);
		std::string get_content() const;
		void set_content(std::string const& content);
		void on_resize() override;
		void load() override;
};

} // namespace ui

} // namespace ktf


// NOTE
// marked letters:
// shader 2 variables => marked_start, marked_end
// font => save highest/lowest end, on quad from left to right for selected text
// render extra quad, create mask, etc.
