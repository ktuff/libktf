#pragma once

#include "widget.hpp"

namespace ktf {

namespace ui {

class renderable : public widget {

	protected:
		size_t m_id;
		ktf::matrix<float> m_model;
		ktf::vec4<float> m_color;
		std::string m_content;
		bool m_hidden;


	public:
		renderable(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, const std::string& content = "");
		virtual ~renderable();


	public:
		size_t id() const;
		ktf::matrix<float> const& model() const;
		ktf::vec4<float> const& color() const;
		renderable& identity();
		renderable& translate(float x, float y, float z);
		renderable& scale(float x, float y, float z);
		renderable& rotate(float r);
		renderable& color(ktf::vec4<float> const& c);
		void hide(bool value);
		bool visible() const;
		virtual void load() = 0;
};

} // namespace ui

} // namespace ktf
