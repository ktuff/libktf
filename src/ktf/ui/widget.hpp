#pragma once

#include <memory>
#include <set>
#include <string>
#include <ktf/math/matrix.hpp>
#include <ktf/math/vec2.hpp>
#include <ktf/opengl/event.hpp>


namespace ktf {

namespace ui {

class gui;
class group;
class text;

class widget {

	private:
		static size_t s_next_id;

	private:
		struct widget_order {
			bool operator()(widget* w1, widget* w2) const;
		};

	public:
		struct size_policy {
			enum policy_type {
				FIXED_RATIO,
				OVERSIZE,
				EXPANDING,
				EXPANDING_WIDTH,
				EXPANDING_HEIGHT
			} m_type;
			float value;

			size_policy();
			size_policy(policy_type type, float value);

			void apply(float& w, float& h) const;
		};
		struct layout {
			enum layout_type {
				HORIZONTAL,
				VERTICAL,
				FREE
			} m_type;

			layout(layout_type type);

			void apply(float x, float y, float width, float height, float rotation, bool force, std::set<ktf::ui::widget*, widget_order> const& children) const;
		};
		struct offset {
			enum size_type {
				PIXEL,
				PERCENT
			} m_type;
			ktf::vec2<float> val;

			offset();
			offset(size_type type, float x, float y);

			void apply(float& x, float& y, float w, float h, float fx, float fy, float fw, float fh) const;
		};
		struct padding {
			enum size_type {
				PIXEL,
				PERCENT
			} m_type;
			ktf::vec4<float> m_val;

			padding();
			padding(size_type type, float left, float right, float top, float bottom);

			void apply(float& x, float& y, float& wp, float& wn, float& hp, float& hn) const;
		};


	public:
		uint64_t const m_id;
		std::string const m_name;
		int const& r_layer = m_layer;

	protected:
		ktf::ui::gui* m_ui;
		ktf::ui::widget* m_parent;
		std::set<ktf::ui::group*> m_groups;
		std::set<ktf::ui::widget*, widget_order> m_children;
		int m_layer;
		int m_layer_offset;
		int m_layout_position;
		ktf::vec2<float> m_pos;
		ktf::vec2<float> m_dim;
		float m_rotation;
		float m_scale;

		ktf::vec2<float> m_old_in_pos;
		ktf::vec2<float> m_old_in_dim;
		float m_old_in_rotation;

		float m_local_rotation;
		ktf::vec2<float> m_frame;
		std::string m_copy;
		size_policy m_size_policy;
		layout m_layout;
		offset m_offset;
		padding m_padding;
		bool m_enabled;
		uint64_t m_state;


	public:
		widget(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, int layer = -1);
		virtual ~widget();


	private:
		static size_t next_id();

	public:
		void set_parent(ktf::ui::widget* widget);
		void add_child(ktf::ui::widget* widget);
		virtual void remove_child(ktf::ui::widget* widget);
		void add_to_group(ktf::ui::group* group);
		void remove_from_group(ktf::ui::group* group);
		widget* find(std::string const& name);
		void resize();
		void resize(float px, float py, float x, float y, float w, float h, float r, bool force);
		bool in_frame(int px, int py) const;

		void enable();
		void disable();
		uint64_t state() const;
		ktf::vec2<float> dim() const;
		void set_state(uint64_t state);
		void set_policy(size_policy pol);
		void set_layout(layout lo);
		void set_offset(offset o);
		void set_frame(ktf::vec2<float> const& frame);
		void set_rotation(float rotation);
		void set_scale(float scale);
		void set_copy(std::string const& copy);
		void set_layer_offset(int offset);

		void on_focus_event(ktf::event* event);
		void on_drop_event(ktf::event* event);
		void on_click_event(ktf::event* event);
		void on_cursor_pos_event(ktf::event* event);
		void on_scroll_event(ktf::event* event);
		void on_key_event(ktf::event* event);
		void on_char_event(ktf::event* event);

	protected:
		virtual void on_resize();
		virtual void handle_focus_event(ktf::event* event);
		virtual void handle_drop_event(ktf::event* event);
		virtual void handle_key_event(ktf::event* event);
		virtual void handle_char_event(ktf::event* event);
		virtual void handle_click_event(ktf::event* event);
		virtual void handle_position_event(ktf::event* event);
		virtual void handle_scroll_event(ktf::event* event);
};

} // namespace ui

} // namespace ktf
