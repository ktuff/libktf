#pragma once

#include "interactable.hpp"
#include "text.hpp"

namespace ktf {

namespace ui {

class text_field : public ktf::ui::interactable {

	private:
		std::shared_ptr<ktf::ui::text> m_content;
		std::vector<uint32_t> m_codepoints;
		std::set<uint32_t> m_char_filter;
		ktf::vec2<int> m_selected;
		bool m_active;
		std::weak_ptr<ktf::ui::text_field> m_next;


	public:
		text_field(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, font_alloc const& allocator);
		~text_field();


	public:
		std::string get_text() const;
		void set_text(std::string const& text);
		void handle_key_event(ktf::event* event) override;
		void handle_char_event(ktf::event* event) override;
		void handle_click_event(ktf::event * event) override;
		void set_next(std::weak_ptr<ktf::ui::text_field> const& next);
};

} // namespace ui

} // namespace ktf
