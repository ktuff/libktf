#pragma once

#include "interactable.hpp"
#include "sprite.hpp"

namespace ktf {

namespace ui {

class slider : public interactable {

	private:
		widget* m_slider;
		enum slider_type {
			VERTICAL,
			HORIZONTAL
		} m_type;
		ktf::vec2<float> m_range;
		float m_value;
		int m_steps;


	public:
		slider(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, std::shared_ptr<widget> s = nullptr);
		~slider();


	public:
		void remove_child(ktf::ui::widget* widget) override;
		void handle_click_event(ktf::event * event) override;
		void handle_position_event(ktf::event * event) override;
		void on_resize() override;
		float get_value() const;
		void set_value(float value);
		void set_steps(int steps);
		void set_type(bool horizontal);
		ktf::ui::widget* get_slider();
		void set_slider(ktf::ui::widget* widget);

	private:
		void adjust(float x, float y);
};

} // namespace ui

} // namespace ktf
