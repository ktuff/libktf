#include "sprite.hpp"

ktf::ui::sprite::sprite(ktf::ui::gui* ui, ktf::ui::widget* parent, const std::string& name, const std::function<size_t(std::string const&)>& loadf, std::string const& content)
: renderable(ui, parent, name)
, m_loadf(loadf)
{
	this->m_content = content;
}

ktf::ui::sprite::~sprite()
{
}



void ktf::ui::sprite::on_resize()
{
	this->identity()
		 .scale(m_dim.x, m_dim.y, 1.0f)
		 .rotate(m_rotation)
		 .translate(m_pos.x, m_pos.y, 0.01f * (m_layer + m_layer_offset));
}

void ktf::ui::sprite::load()
{
	if (m_loadf) {
		this->m_id = m_loadf(m_content);
	}
}

void ktf::ui::sprite::set_content(std::string const& content)
{
	this->m_content = content;
	this->load();
}
