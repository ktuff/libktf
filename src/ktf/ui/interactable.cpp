#include "interactable.hpp"

ktf::ui::keyt::keyt(int p_key, int p_action, int p_mods)
: key(p_key)
, mods(p_mods)
, action(p_action)
, _(0)
{
}


ktf::ui::interactable::interactable(ktf::ui::gui* ui, ktf::ui::widget* parent, std::string const& name, int layer)
: widget(ui, parent, name, layer)
{
}

ktf::ui::interactable::~interactable()
{
}


void ktf::ui::interactable::reset_actions()
{
}

void ktf::ui::interactable::set_action(ktf::event_type index, std::function<void (ktf::event*)> action)
{
	switch (index) {
	case event_size:
		break;
	case event_focus:
		this->m_focus_action = action;
		break;
	case event_drop:
		this->m_drop_action = action;
		break;
	case event_key:
		this->m_key_action = action;
		break;
	case event_char:
		this->m_char_action = action;
		break;
	case event_button:
		this->m_click_action = action;
		break;
	case event_position:
		this->m_cursor_pos_action = action;
		break;
	case event_scroll:
		this->m_scroll_action = action;
		break;
	}
}

void ktf::ui::interactable::add_key(uint64_t key, std::function<void ()> const& action)
{
	this->m_key_map.emplace(key, action);
}

void ktf::ui::interactable::remove_key(uint64_t key)
{
	if (m_key_map.find(key) != m_key_map.end()) {
		this->m_key_map.erase(key);
	}
}

void ktf::ui::interactable::handle_focus_event(ktf::event* event)
{
	if (m_focus_action) {
		this->m_focus_action(event);
	}
}

void ktf::ui::interactable::handle_drop_event(ktf::event* event)
{
	if (m_drop_action) {
		this->m_drop_action(event);
	}
}

void ktf::ui::interactable::handle_key_event(ktf::event* event)
{
	ktf::key_event* kev = static_cast<ktf::key_event*>(event);
	uint64_t value = keyt(kev->key, kev->action, kev->mods).value;
	if (m_key_map.find(value) != m_key_map.end()) {
		this->m_key_map.at(value)();
		event->handled = true;
	}
	if (!event->handled && m_key_action) {
		this->m_key_action(event);
	}
}

void ktf::ui::interactable::handle_char_event(ktf::event* event)
{
	if (m_char_action) {
		this->m_char_action(event);
	}
}

void ktf::ui::interactable::handle_click_event(ktf::event* event)
{
	if (m_click_action) {
		this->m_click_action(event);
	}
}

void ktf::ui::interactable::handle_position_event(ktf::event* event)
{
	if (m_cursor_pos_action) {
		this->m_cursor_pos_action(event);
	}
}

void ktf::ui::interactable::handle_scroll_event(ktf::event* event)
{
	if (m_scroll_action) {
		this->m_scroll_action(event);
	}
}
