#include <string.h>
#include "packet.hpp"

ktf::packet::packet()
: type(KTF_CON_PACKET_INVALID)
, size(KTF_CON_PACKET_NO_SIZE)
, data(nullptr)
{
}

ktf::packet::packet(int64_t type, int64_t size, const char* data) 
: type(type)
, size(size)
, data(nullptr)
{
    if (data && size > 0) {
        this->data = new char[size];
        ::memcpy(ktf::packet::data, data, size);
    } else {
        this->type = KTF_CON_PACKET_INVALID;
        this->size = KTF_CON_PACKET_NO_SIZE;
    }
}

ktf::packet::packet(ktf::packet && p)
: type(p.type)
, size(p.size)
, data(p.data)
{
    p.type = KTF_CON_PACKET_INVALID;
    p.size = KTF_CON_PACKET_NO_SIZE;
    p.data = nullptr;
}

ktf::packet::~packet()
{
    if (data) {
        delete[] data;
    }
}

ktf::packet& ktf::packet::operator=(ktf::packet&& p)
{
    if (data) {
        delete[] data;
    }
    this->type = p.type;
    this->size = p.size;
    this->data = p.data;
    p.type = KTF_CON_PACKET_INVALID;
    p.size = KTF_CON_PACKET_NO_SIZE;
    p.data = nullptr;
    return *this;
}

void ktf::packet::zero()
{
    this->type = KTF_CON_PACKET_INVALID;
    this->size = KTF_CON_PACKET_NO_SIZE;
    if (data) {
        delete[] data;
        this->data = nullptr;
    }
}

void ktf::packet::copy(const ktf::packet& p)
{
    if (data) {
        delete[] data;
    }
    if (p.data) {
        this->type = p.type;
        this->size = p.size;
        this->data = new char[size];
        ::memcpy(ktf::packet::data, p.data, size);
    }
}
