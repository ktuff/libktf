#pragma once

#include "socket_base.hpp"

namespace ktf {
  
const int disconnect_disconnect = 0;
const int disconnect_error = 1;
    
class client_observer {
    public:
        virtual void on_connect() = 0;
        virtual void on_disconnect(int code) = 0;
};
  

class client_socket : public ktf::socket_base {
        
    private:
        client_observer* observer;
        bool state;
    
    
    public:
        client_socket(client_observer* observer);
        ~client_socket();
        
        
    public:
        void connect(const char* address, int port, int timeout);
        void disconnect();
        void poll();
};

} // namespace ktf
