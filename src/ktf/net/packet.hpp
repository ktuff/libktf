#pragma once

#include "constants.hpp"

namespace ktf {
    
class packet {
    
    public:
        int64_t type;
        int64_t size;
        char* data;
        
    
    public:
        packet();
        packet(int64_t type, int64_t size, const char* data);
        packet(packet&& p);
        packet(const packet& p) = delete;
        ~packet();
        packet& operator=(packet&& p);
        packet& operator=(const packet& p) = delete;
    
    public:
        void zero();
        void copy(const packet& p);
        
};

} // namespace ktf
