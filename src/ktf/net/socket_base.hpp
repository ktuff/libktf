#pragma once

#include <queue>
#include "packet.hpp"

namespace ktf {

class socket_base {
    
    protected:
        int socket;
        int port;
        int64_t packet_type;
        int64_t packet_size;
        int64_t received_size;
        char* data;
        
        std::deque<packet> packets_in;
        std::deque<packet> packets_out;
        
    
    public:
        socket_base();
        socket_base(socket_base&& sb) = delete;
        socket_base(const socket_base& sb) = delete;
        socket_base& operator=(socket_base&& sb) = delete;
        socket_base& operator=(const socket_base& sb) = delete;
        ~socket_base();
        
        
    public:
        bool is_connected();
        int status_code();
        bool packets_available();
        void send(packet& p);
        void receive(packet& p);
        void update();
};

} // namespace ktf
