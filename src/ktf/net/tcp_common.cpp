#include <string.h>

#include "tcp_common.hpp"


tcp_packet::tcp_packet()
: m_size(0)
{
}

tcp_packet::tcp_packet(void const* data, size_t size)
: m_size(size)
{
	if (size > 0) {
		this->m_data = std::make_unique<char[]>(size);
		::memcpy(m_data.get(), data, size);
	}
}

tcp_packet::tcp_packet(std::unique_ptr<char []>& data, size_t size)
: m_data(std::move(data))
, m_size(size)
{
}

tcp_packet::tcp_packet(tcp_packet && packet)
: m_data(std::move(packet.m_data))
, m_size(packet.m_size)
{
	packet.m_size = 0;
}

tcp_packet::tcp_packet(const tcp_packet& packet)
: m_size(packet.m_size)
{
	if (packet.m_data) {
		this->m_data = std::make_unique<char[]>(packet.m_size);
		::memcpy(m_data.get(), packet.m_data.get(), packet.m_size);
	}
}

tcp_packet & tcp_packet::operator=(tcp_packet && packet)
{
	this->m_data = std::move(packet.m_data);
	this->m_size = packet.m_size;
	packet.m_size = 0;
	return *this;
}

tcp_packet & tcp_packet::operator=(const tcp_packet& packet)
{
	if (packet.m_data) {
		this->m_data = std::make_unique<char[]>(packet.m_size);
		::memcpy(m_data.get(), packet.m_data.get(), packet.m_size);
	} else {
		this->m_data = nullptr;
	}
	this->m_size = packet.m_size;
	return *this;
}

