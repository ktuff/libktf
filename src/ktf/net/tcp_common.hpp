#pragma once

#include <memory>


enum tcp_connection_close_status {
	tcp_closed_normal,
	tcp_closed_by_client,
	tcp_closed_by_server,
	tcp_closed_by_timeout,
	tcp_closed_by_other
};


struct tcp_packet {
public:
	enum Type : size_t {
		NOOP,
		DATA
	};

public:
	std::unique_ptr<char[]> m_data;
	size_t m_size;

public:
	tcp_packet();
	tcp_packet(void const* data, size_t size);
	tcp_packet(std::unique_ptr<char[]>& data, size_t size);
	tcp_packet(tcp_packet && packet);
	tcp_packet(tcp_packet const& packet);

	tcp_packet& operator=(tcp_packet && packet);
	tcp_packet& operator=(tcp_packet const& packet);
};

