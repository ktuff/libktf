#include "tcp_client.hpp"


tcp_client::tcp_client()
: m_socket(m_context)
, m_packet_size(0)
, m_received_size(0)
, m_timeout_limit(5000)
, m_connected(false)
{
}

tcp_client::~tcp_client()
{
	this->disconnect(tcp_closed_normal);
}


void tcp_client::set_timeout_limit(size_t ms)
{
	this->m_timeout_limit = std::chrono::milliseconds(ms);
}

void tcp_client::set_callbacks(const std::function<void ()>& on_connect, const std::function<void (tcp_connection_close_status)>& on_disconnect)
{
	this->m_on_connect = on_connect;
	this->m_on_disconnect = on_disconnect;
}

void tcp_client::connect(std::string const& ip, int port)
{
	boost::asio::ip::tcp::resolver resolver(m_context);
	boost::asio::ip::tcp::resolver::results_type endpoints = resolver.resolve(ip, std::to_string(port));
	boost::system::error_code error;
	auto endpoint = (*endpoints).endpoint();
	this->m_socket.open(endpoint.protocol());
	this->m_socket.connect(endpoint, error);
	if (error == boost::system::errc::success) {
		auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
		this->m_prev_ping = now;
		this->m_ping_delay = now;
		this->m_connected = true;
		if (m_on_connect) {
			this->m_on_connect();
		}
	} else {
		throw std::runtime_error(error.message());
	}
}

void tcp_client::disconnect(tcp_connection_close_status code)
{
	if (m_socket.is_open()) {
		boost::system::error_code ec;
		this->m_socket.shutdown(boost::asio::socket_base::shutdown_both, ec);
		this->m_socket.cancel(ec);
		this->m_socket.close(ec);

		this->m_connected = false;
		if (m_on_disconnect) {
			this->m_on_disconnect(code);
		}
	}
}

bool tcp_client::is_connected() const
{
	return m_connected;
}

void tcp_client::poll(std::function<void(tcp_packet const&)> const& on_data)
{
	if (!m_socket.is_open()) {
		return;
	}

	this->m_context.poll();
	auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());

	boost::system::error_code ec;
	size_t avail = m_socket.available();
	while (avail > 0) {
		if (!m_packet_data && avail > sizeof(size_t) * 2) {
			std::array<size_t, 2> header;
			this->m_socket.receive(boost::asio::buffer(header), 0, ec);
			if (ec == boost::asio::error::eof) {
				this->disconnect(tcp_closed_by_server);
				return;
			}
			if (header[0] == tcp_packet::NOOP) {
				this->m_prev_ping = now;
			} else if (header[0] == tcp_packet::DATA) {
				this->m_packet_size = header[1];
				this->m_packet_data = std::make_unique<char[]>(m_packet_size);
				this->m_received_size = 0;
			}
		} else if (m_packet_data) {
			size_t data_length = m_socket.receive(boost::asio::buffer(m_packet_data.get() + m_received_size, m_packet_size - m_received_size), 0, ec);
			if (ec == boost::asio::error::eof) {
				this->disconnect(tcp_closed_by_server);
				return;
			}
			this->m_received_size += data_length;
			if (m_received_size == m_packet_size) {
				if (on_data) {
					tcp_packet packet(m_packet_data, m_packet_size);
					on_data(packet);
				}
				this->m_packet_data = nullptr;
			}
		} else {
			break;
		}
		avail = m_socket.available();
	}

	// check for timeout
	if (now - m_prev_ping > m_timeout_limit) {
		this->disconnect(tcp_closed_by_timeout);
	}
	if (now - m_ping_delay >= std::chrono::milliseconds(50)) {
		// send timeout check message
		size_t timeout_msg[2] = {
			tcp_packet::NOOP,
			0
		};
		size_t send_bytes = m_socket.send(boost::asio::buffer(timeout_msg, sizeof(timeout_msg)), 0, ec);
		if (ec == boost::asio::error::connection_reset || send_bytes == 0 || send_bytes < sizeof(timeout_msg)) {
			this->disconnect(tcp_closed_by_other);
		}
		this->m_ping_delay = now;
	}
}

void tcp_client::send(tcp_packet& packet)
{
	if (m_socket.is_open()) {
		size_t header[2] {
			tcp_packet::DATA,
			packet.m_size
		};
		boost::system::error_code ec;
		size_t send_bytes = m_socket.send(boost::asio::buffer(header, sizeof(header)), 0, ec);
		if (ec == boost::asio::error::connection_reset || send_bytes == 0 || send_bytes < sizeof(header)) {
			this->disconnect(tcp_closed_by_other);
			return;
		}
		if (packet.m_size > 0) {
			send_bytes = m_socket.send(boost::asio::buffer(packet.m_data.get(), packet.m_size), 0, ec);
			if (ec == boost::asio::error::connection_reset || send_bytes == 0 || send_bytes < sizeof(packet.m_size)) {
				this->disconnect(tcp_closed_by_other);
			}
		}
	}
}
