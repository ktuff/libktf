#include <arpa/inet.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <sys/poll.h>
#include <unistd.h>
#include "server.hpp"
#include "server_socket.hpp"

ktf::server::server(server_observer* observer, int max_connections)
: socket(KTF_CON_STATUS_CLOSED)
, port(KTF_CON_INVALID_PORT)
, quit(true)
, num_max_clients(max_connections)
, observer(observer)
{
	assert(max_connections > 0);
	this->connections.resize(num_max_clients);
	for (int i=0; i<num_max_clients; i++) {
		this->free_slots.push(i);
		this->connections[i] = nullptr;
	}
}

ktf::server::~server()
{
	this->halt();
}


void ktf::server::launch(int port)
{
	if (!quit) {
		return;
	}

	this->socket = ::socket(AF_INET, SOCK_STREAM, 0);
	if (socket < 0) {
		this->socket = KTF_CON_ERROR_SOCKET;
		return;
	}
	int opt = 0;
	if (::setsockopt(socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) == -1) {
		::close(socket);
		this->socket = KTF_CON_ERROR_SOCKOPT;
		return;
	}
	if (::fcntl(socket, F_SETFL, ::fcntl(socket, F_GETFL) | O_NONBLOCK) < 0) {
		::close(socket);
		this->socket = KTF_CON_ERROR_SOCKOPT;
	}
	sockaddr_in address;
	::memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = ::htonl(INADDR_ANY);
	if (port >= 0) {
		address.sin_port = ::htons(port);
	}
	if (::bind(socket, (sockaddr*)&address, sizeof(address)) == -1) {
		::close(socket);
		this->socket = KTF_CON_ERROR_BIND;
		return;
	}

	socklen_t len = sizeof(address);
	::memset(&address, 0, sizeof(address));
	::getsockname(socket, (sockaddr*)&address, &len);
	this->port = ::ntohs(address.sin_port);

	// launch master server thread
	this->quit = false;
	this->thread = std::thread([=]() {
		this->listen();
	});
}

void ktf::server::halt()
{
	if (!quit) {
		this->quit = true;
		this->thread.join();
		::close(socket);
		this->socket = KTF_CON_STATUS_CLOSED;
		while (!free_slots.empty()) {
			this->free_slots.pop();
		}
		std::lock_guard<std::mutex> guard(mutex);
		for (int i=0; i<num_max_clients; i++) {
			this->free_slots.push(i);
			if (connections[i]) {
				this->connections[i] = nullptr;
				this->observer->on_disconnect(i, 0);
			}
		}
	}
}

bool ktf::server::is_running() const
{
	return (socket >= 0);
}

int ktf::server::get_port() const
{
	return port;
}

int ktf::server::status_code() const
{
	return socket;
}


void ktf::server::send(ktf::packet& p, int connection_id)
{
	if (p.type == KTF_CON_PACKET_INVALID || p.size <= 0) {
		return;
	}
	if (connection_id == -1) {
		ktf::packet pkt;
		std::lock_guard<std::mutex> guard(mutex);
		for (int c=0; c<num_max_clients; ++c) {
			pkt.copy(p);
			if (connections[c] && connections[c]->is_connected()) {
				this->connections[c]->send(pkt);
			}
		}
	} else if (connection_id >= 0 && connection_id < num_max_clients) {
		std::lock_guard<std::mutex> guard(mutex);
		if (connections[connection_id] && connections[connection_id]->is_connected()) {
			this->connections[connection_id]->send(p);
		}
	}
}

void ktf::server::receive(ktf::packet& p, int& connection_id)
{
	static int next = 0;
	int start = next;
	std::lock_guard<std::mutex> guard(mutex);
	while (!connections[next] || !connections[next]->is_connected() || !connections[next]->packets_available()) {
		next = (next + 1) % num_max_clients;
		if (start == next) {
			connection_id = -1;
			p.zero();
			return;
		}
	}
	connection_id = next;
	this->connections[next]->receive(p);
}

void ktf::server::disconnect(int id)
{
	std::lock_guard<std::mutex> guard(mutex);
	if (connections[id]) {
		this->connections[id] = nullptr;
		this->free_slots.push(id);
		this->observer->on_disconnect(id, 0);
	}
}

void ktf::server::listen()
{
	if (socket < 0) {
		return;
	}

	pollfd ufds;
	ufds.fd = socket;
	ufds.events = POLLIN;
	unsigned int nfds = 1;
	int timeout = 5;

	while (!quit) {
		if (!free_slots.empty()) {
			if (::listen(socket, 1) < 0) {
				this->halt();
			}
			int event = ::poll(&ufds, nfds, timeout);
			if (event > 0) {
				int fd = ::accept(socket, NULL, NULL);
				if (fd >= 0) {
					int32_t id = free_slots.front();
					this->free_slots.pop();
					std::lock_guard<std::mutex> guard(mutex);
					this->connections[id] = std::make_shared<server_socket>(id, fd);
					this->observer->on_connect(id);
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		this->update();
    }
}

void ktf::server::update()
{
	std::lock_guard<std::mutex> guard(mutex);
	for (int i=0; i<num_max_clients; i++) {
		if (connections[i] && !connections[i]->is_connected()) {
			this->connections[i] = nullptr;
			this->free_slots.push(i);
			this->observer->on_disconnect(i, 1);
		}
		if (connections[i]) {
			this->connections[i]->update();
		}
	}
}
