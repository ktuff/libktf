#include <arpa/inet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "server_socket.hpp"

ktf::server_socket::server_socket(int32_t id, int socket)
: socket_base()
, id(id)
{
    this->socket = socket;
}

ktf::server_socket::~server_socket()
{
}
