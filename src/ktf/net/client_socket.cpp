#include <arpa/inet.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <thread>
#include "client_socket.hpp"

ktf::client_socket::client_socket(client_observer* observer) 
: socket_base()
, observer(observer)
, state(false)
{
}

ktf::client_socket::~client_socket()
{
	this->disconnect();
}


void ktf::client_socket::connect(const char* address, int port, int timeout)
{
	if (port < 0) {
		this->socket = KTF_CON_ERROR_PORT;
		return;
	}

	// ipv4, tcp
	this->socket = ::socket(AF_INET, SOCK_STREAM, 0);
	if (socket < 0) {
		this->socket = KTF_CON_ERROR_SOCKET;
		return;
	}
	if (::fcntl(socket, F_SETFL, ::fcntl(socket, F_GETFL) | O_NONBLOCK) < 0) {
		::close(socket);
		this->socket = KTF_CON_ERROR_SOCKOPT;
		return;
	}

	sockaddr_in addr;
	::memset(&addr, '\0', sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	if (::inet_pton(addr.sin_family, address, &addr.sin_addr) != 1) {
		::close(socket);
		this->socket = KTF_CON_ERROR_ADDRESS;
		return;
	}

	// try connecting for 5 seconds
	int status = -1;
	std::chrono::milliseconds start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::chrono::milliseconds now = start;
	while (status == -1 && (now - start).count() < timeout) {
		status = ::connect(socket, (sockaddr*)&addr, sizeof(addr));
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	}
	if (status == -1) {
		::close(socket);
		this->socket = KTF_CON_ERROR_CONNECT;
		return;
	} else {
		this->port = port;
	}

	this->state = true;
	if (observer) {
		this->observer->on_connect();
	}
}

void ktf::client_socket::disconnect()
{
	if (socket >= 0) {
		::close(socket);
		this->socket = KTF_CON_STATUS_CLOSED;
		this->port = KTF_CON_INVALID_PORT;
	}
	if (data) {
		this->packet_type = KTF_CON_PACKET_INVALID;
		this->packet_size = KTF_CON_PACKET_NO_SIZE;
		this->received_size = 0;
		delete[] data;
		this->data = nullptr;
	}
	this->packets_in.clear();
	this->packets_out.clear();
	if (state) {
		this->state = false;
		if (observer) {
			this->observer->on_disconnect(disconnect_disconnect);
		}
	}
}

void ktf::client_socket::poll()
{
	if (state && !is_connected()) {
		this->state = false;
		if (observer) {
			this->observer->on_disconnect(disconnect_error);
		}
	}
	this->update();
}
