#pragma once

#include <boost/array.hpp>
#include <boost/asio.hpp>

#include "tcp_common.hpp"


class tcp_client {

	private:
		boost::asio::io_context m_context;
		boost::asio::ip::tcp::socket m_socket;
		std::unique_ptr<char[]> m_packet_data;
		size_t m_packet_size;
		size_t m_received_size;
		std::function<void()> m_on_connect;
		std::function<void(tcp_connection_close_status)> m_on_disconnect;
		std::chrono::milliseconds m_prev_ping;
		std::chrono::milliseconds m_ping_delay;
		std::chrono::milliseconds m_timeout_limit;
		bool m_connected;


	public:
		tcp_client();
		~tcp_client();


	public:
		void set_timeout_limit(size_t ms);
		void set_callbacks(std::function<void()> const& on_connect, std::function<void(tcp_connection_close_status)> const& on_disconnect);
		void connect(std::string const& ip, int port);
		void disconnect(tcp_connection_close_status code);
		bool is_connected() const;
		void poll(std::function<void(tcp_packet const&)> const& on_data);
		void send(tcp_packet& packet);
};
