#pragma once

#include <stdint.h>

#define KTF_CON_HEADER_SIZE         (sizeof(int64_t)*2)
#define KTF_CON_BUFFER_SIZE         1024

#define KTF_CON_INVALID_PORT        -1
#define KTF_CON_STATUS_CLOSED       -1
#define KTF_CON_ERROR_SOCKET        -2
#define KTF_CON_ERROR_SOCKOPT       -3
#define KTF_CON_ERROR_BIND          -4
#define KTF_CON_ERROR_ADDRESS       -5
#define KTF_CON_ERROR_PORT          -6
#define KTF_CON_ERROR_CONNECT       -7

#define KTF_CON_PACKET_INVALID      -1
#define KTF_CON_PACKET_TYPE_TIMEOUT -2
#define KTF_CON_PACKET_NO_SIZE      -1
