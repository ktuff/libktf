#include "socket_base.hpp"

#include <arpa/inet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "socket_base.hpp"

ktf::socket_base::socket_base()
: socket(KTF_CON_STATUS_CLOSED)
, port(KTF_CON_INVALID_PORT)
, packet_type(KTF_CON_PACKET_INVALID)
, packet_size(KTF_CON_PACKET_NO_SIZE)
, received_size(0)
, data(nullptr)
{
}

ktf::socket_base::~socket_base()
{
    if (socket >= 0) {
        ::close(socket);
    }
    if (data) {
        delete[] data;
    }
}


bool ktf::socket_base::is_connected()
{
    return (socket >= 0);
}

int ktf::socket_base::status_code()
{
    return socket;
}

bool ktf::socket_base::packets_available()
{
    return !packets_in.empty();
}

void ktf::socket_base::send(packet& p)
{
    this->packets_out.push_back(std::move(p));
}

void ktf::socket_base::receive(packet& p)
{
    if (packets_in.empty()) {
        p.zero();
    } else {
        p = std::move(packets_in.front());
        this->packets_in.pop_front();
    }
}

void ktf::socket_base::update()
{
    if (!is_connected()) {
        return;
    }
    
    char buffer[KTF_CON_BUFFER_SIZE];
    int64_t info[2];
    int64_t size;
    int rc = 1;

    /*
     * SEND PACKETS
     */
    while (!packets_out.empty() && rc > 0) {
        packet& p = packets_out.front();
        if (p.size > 0) {
            info[0] = p.type;
            info[1] = p.size;
            
            // header is included in data, no extra packages
            ::memcpy(&buffer[0], info, KTF_CON_HEADER_SIZE);
            size = std::min(p.size, (int64_t)(KTF_CON_BUFFER_SIZE - KTF_CON_HEADER_SIZE));
            if (size > 0) {
                ::memcpy(&buffer[KTF_CON_HEADER_SIZE], p.data, size);
            }
            int64_t bytes = size;
            rc = ::send(socket, buffer, bytes + KTF_CON_HEADER_SIZE, MSG_NOSIGNAL | MSG_DONTWAIT);
            while (bytes < p.size && rc > 0) {
                size = std::min(p.size - bytes, (int64_t)KTF_CON_BUFFER_SIZE);
                ::memcpy(buffer, &p.data[bytes], size);
                rc = ::send(socket, buffer, size, MSG_NOSIGNAL | MSG_DONTWAIT);
                bytes += size;
            }
        }
        this->packets_out.pop_front();
    }
    
    /*
     * RECEIVE PACKETS
     */
    int available = 0;
    ::ioctl(socket, FIONREAD, &available);
    while (available > 0 && rc > 0) {
        if (packet_size == KTF_CON_PACKET_NO_SIZE) {
            if (available < (int)KTF_CON_HEADER_SIZE) {
                break;
            }
            info[0] = KTF_CON_PACKET_INVALID;
            info[1] = KTF_CON_PACKET_NO_SIZE;
            rc = ::recv(socket, info, KTF_CON_HEADER_SIZE, MSG_NOSIGNAL | MSG_DONTWAIT);
            if(info[1] <= 0 || rc <= 0) {
                break;
            } else {
                this->packet_type = info[0];
                this->packet_size = info[1];
                this->received_size = 0;
                this->data = new char[packet_size];
                size = std::min(packet_size, (int64_t)KTF_CON_BUFFER_SIZE);
            }
            available -= KTF_CON_HEADER_SIZE;
        } else {
            size = std::min(packet_size - received_size, (int64_t)KTF_CON_BUFFER_SIZE);
        }
        if (size > 0 && available > 0) {
            rc = ::recv(socket, buffer, size, MSG_NOSIGNAL | MSG_DONTWAIT);
            ::memcpy(&data[received_size], buffer, size);
            this->received_size = received_size + rc;
        }
        if (received_size == packet_size) {
            packet p(packet_type, packet_size, data);
            this->packets_in.push_back(std::move(p));
            this->packet_type = KTF_CON_PACKET_INVALID;
            this->packet_size = KTF_CON_PACKET_NO_SIZE;
            this->received_size = 0;
            delete[] data;
            this->data = nullptr;
        }
        ::ioctl(socket, FIONREAD, &available);
    }
    
    // check for disconnect
    int error = 0;
    socklen_t errlen = sizeof(error);
    int retval = ::getsockopt(socket, SOL_SOCKET, SO_ERROR, &error, &errlen);
    if (rc <= 0 || (retval == 0 && error != 0)) {
        ::close(socket);
        this->socket = KTF_CON_STATUS_CLOSED;
    }
}
