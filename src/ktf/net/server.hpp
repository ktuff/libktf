#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include "packet.hpp"

namespace ktf {

class server_socket;

class server_observer {
    public:
        virtual void on_connect(int id) = 0;
        virtual void on_disconnect(int id, int code) = 0;
};


class server {
    
    private:
        int socket;
        int port;
        
        std::thread thread;
        std::atomic<bool> quit;
        std::vector<std::shared_ptr<server_socket>> connections;
        std::queue<int32_t> free_slots;
        int num_max_clients;
        std::mutex mutex;
        
        server_observer* observer;
        
        
    public:
        server(server_observer* observer, int max_connections);
        ~server();
        
        
    public:
        void launch(int port);
        void halt();
        bool is_running() const;
        int get_port() const;
        int status_code() const;
        
        void send(packet& p, int connection_id);
        void receive(packet& p, int& connection_id);
        
        void disconnect(int id);
        
    private:
		void listen();
		void update();
};

} // namespace ktf
