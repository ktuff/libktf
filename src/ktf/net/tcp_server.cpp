#include "tcp_server.hpp"
#include "tcp_server_connection.hpp"


tcp_server::tcp_server(size_t num_sockets)
: m_acceptor(m_context)
, m_max_connections(num_sockets)
, m_connections(num_sockets)
, m_timeout_limit(5000)
, m_active(false)
{
	for (size_t i=0; i<num_sockets; ++i) {
		this->m_free_indices.emplace(i);
	}
	this->m_acceptor.close();
}

tcp_server::~tcp_server()
{
	this->stop();
}


void tcp_server::set_timeout_limit(size_t ms)
{
	this->m_timeout_limit = ms;
	for (size_t con : m_connection_indices) {
		this->m_connections[con]->set_timeout_limit(ms);
	}
}

void tcp_server::set_callbacks(const std::function<void (size_t)>& on_connect, const std::function<void (size_t, tcp_connection_close_status)>& on_disconnect)
{
	this->m_on_connect = on_connect;
	this->m_on_disconnect = on_disconnect;
}

void tcp_server::launch(uint16_t port, std::function<void(uint16_t)> const& on_launch)
{
	if (!m_active) {
		auto protocol = boost::asio::ip::tcp::v4();
		boost::asio::ip::tcp::endpoint endpoint(protocol, port);
		this->m_acceptor.open(endpoint.protocol());
		this->m_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		this->m_acceptor.bind(endpoint);
		this->m_active = true;
		this->listen();
		if (on_launch) {
			int port = get_port();
			on_launch(port);
		}
	}
}

void tcp_server::stop()
{
	if (m_active) {
		this->m_acceptor.cancel();
		this->m_connections.clear();
		this->m_connections.resize(m_max_connections);
		this->m_acceptor.close();
		this->m_connection_indices.clear();
		this->m_free_indices.clear();
		for (size_t i=0; i<m_max_connections; ++i) {
			this->m_free_indices.emplace(i);
		}
		this->m_active = false;
	}
}

bool tcp_server::is_up() const
{
	return m_active;
}

uint16_t tcp_server::get_port() const
{
	uint16_t port = m_acceptor.local_endpoint().port();
	return port;
}

void tcp_server::poll(std::function<void(size_t, tcp_packet const&)> const& on_data)
{
	this->m_context.poll();

	// accept new connections
	if (!m_new_connections.empty()) {
		std::lock_guard<std::mutex> guard(m_mutex);
		for (auto& [index, connection] : m_new_connections) {
			this->m_connections[index] = std::move(connection);
			this->m_connections[index]->set_timeout_limit(m_timeout_limit);
			this->m_free_indices.erase(index);
			this->m_connection_indices.emplace(index);
		}
		this->m_new_connections.clear();
	}

	// tick connections
	for (size_t index : m_connection_indices) {
		this->m_connections[index]->poll(on_data);
	}

	// remove closed connections
	if (!m_closed_connections.empty()) {
		for (auto const& [index, code] : m_closed_connections) {
			this->close_connection(index);
		}
		this->m_closed_connections.clear();
	}

	this->m_context.restart();
}

void tcp_server::send(tcp_packet& packet, size_t index)
{
	if (m_connection_indices.find(index) != m_connection_indices.end()) {
		this->m_connections[index]->send(packet);
	}
}

void tcp_server::close_connection(size_t index)
{
	bool restart_listening;
	if (m_connection_indices.find(index) != m_connection_indices.end()) {
		restart_listening = m_free_indices.empty();
		this->m_connections[index] = nullptr;
		this->m_free_indices.emplace(index);
		this->m_connection_indices.erase(index);
		tcp_connection_close_status code = m_closed_connections[index];
		if (m_on_disconnect) {
			this->m_on_disconnect(index, code);
		}
	} else {
		restart_listening = false;
	}
	if (restart_listening) {
		this->listen();
	}
}

void tcp_server::listen()
{
	size_t index;
	if (m_free_indices.empty()) {
		return;
	} else {
		index = *m_free_indices.begin();
	}
	this->m_acceptor.listen(m_max_connections);
	this->m_acceptor.async_accept([this, index](boost::system::error_code const& ec, boost::asio::ip::tcp::socket&& socket) {
		if (ec != boost::system::errc::success) {
			// TODO throw std::runtime_error(ec.message());
		} else {
			std::lock_guard<std::mutex> guard(m_mutex);
			this->m_new_connections.emplace(index, std::make_unique<tcp_server_connection>(std::move(socket), index, [this](size_t index, tcp_connection_close_status code) {
				this->m_closed_connections.emplace(index, code);
			}));
		}
		if (m_on_connect) {
			this->m_on_connect(index);
		}
		this->listen();
	});
}
