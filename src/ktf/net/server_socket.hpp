#pragma once

#include <atomic>
#include <thread>
#include "socket_base.hpp"

namespace ktf {

class server_socket : public ktf::socket_base {

    public:
        const int32_t id;

    
    public:
        server_socket(int32_t id, int socket);
        ~server_socket();
};

} // namespace ktf
