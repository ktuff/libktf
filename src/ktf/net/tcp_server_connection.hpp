#pragma once

#include <boost/asio.hpp>

#include "tcp_common.hpp"


class tcp_server_connection {

	private:
		size_t const m_index;
		boost::asio::ip::tcp::socket m_socket;
		std::vector<tcp_packet> m_packets;
		std::unique_ptr<char[]> m_packet_data;
		size_t m_packet_size;
		size_t m_received_size;
		std::function<void(size_t, tcp_connection_close_status)> m_on_disconnect;
		std::chrono::milliseconds m_prev_ping;
		std::chrono::milliseconds m_ping_delay;
		std::chrono::milliseconds m_timeout_limit;


	public:
		tcp_server_connection(boost::asio::ip::tcp::socket&& socket, size_t index, std::function<void(size_t, tcp_connection_close_status)> on_disconnect);
		~tcp_server_connection();


	public:
		void set_timeout_limit(size_t ms);
		void poll(std::function<void(size_t index, tcp_packet const& packet)> on_data);
		void send(tcp_packet& packet);

	private:
		void disconnect(tcp_connection_close_status code);
};
