#pragma once

#include <set>
#include <string>

#include <boost/asio.hpp>

#include "tcp_common.hpp"


class tcp_server_connection;


class tcp_server {

	private:
		boost::asio::io_context m_context;
		boost::asio::ip::tcp::acceptor m_acceptor;
		size_t m_max_connections;
		std::vector<std::unique_ptr<tcp_server_connection>> m_connections;
		std::set<size_t> m_free_indices;
		std::set<size_t> m_connection_indices;
		std::unordered_map<size_t, std::unique_ptr<tcp_server_connection>> m_new_connections;
		std::unordered_map<size_t, tcp_connection_close_status> m_closed_connections;
		std::mutex m_mutex;
		std::function<void(size_t)> m_on_connect;
		std::function<void(size_t, tcp_connection_close_status)> m_on_disconnect;
		size_t m_timeout_limit;
		bool m_active;


	public:
		explicit tcp_server(size_t num_sockets);
		~tcp_server();


	public:
		void set_timeout_limit(size_t ms);
		void set_callbacks(std::function<void(size_t)> const& on_connect, std::function<void(size_t, tcp_connection_close_status)> const& on_disconnect);
		void launch(uint16_t port, std::function<void(uint16_t)> const& on_launch);
		void stop();
		bool is_up() const;
		uint16_t get_port() const;
		void poll(std::function<void(size_t, tcp_packet const&)> const& on_data);
		void send(tcp_packet& packet, size_t index);
		void close_connection(size_t index);

	private:
		void listen();
};
