if(WITH_BOOST_ASIO)
	add_library(ktfnet STATIC
		tcp_client.cpp
		tcp_common.cpp
		tcp_server.cpp
		tcp_server_connection.cpp
	)
	target_link_libraries(ktfnet pthread)
else()
	add_library(ktfnet STATIC
		client_socket.cpp
		packet.cpp
		server_socket.cpp
		server.cpp
		socket_base.cpp
	)
	target_link_libraries(ktfnet pthread)
endif()
