#include <png.h>
#include <stdlib.h>
#include <string.h>
#include "png.hpp"

ktf::png::png()
: width(0)
, height(0)
, channels(0)
, data(nullptr)
{
}

ktf::png::png(uint32_t width, uint32_t height, int channels, uint8_t const * data)
: width(width)
, height(height)
, channels(channels)
{
    size_t num_bytes = width * height * channels;
    this->data = new uint8_t[num_bytes];
    ::memcpy(this->data, data, num_bytes);
}

ktf::png::png(ktf::png && p)
: width(p.width)
, height(p.height)
, channels(p.channels)
, data(p.data)
{
    p.width = 0;
    p.height = 0;
    p.channels = 0;
    p.data = nullptr;
}

ktf::png::png(const ktf::png& p)
: width(p.width)
, height(p.height)
, channels(p.channels)
{
	size_t num_bytes = width * height * channels;
	this->data = new uint8_t[num_bytes];
	::memcpy(this->data, p.data, num_bytes);
}

ktf::png::~png()
{
    if (data) {
        delete[] data;
    }
}

ktf::png & ktf::png::operator=(ktf::png && p)
{
    this->width =p.width;
    this->height = p.height;
    this->channels = p.channels;
    this->data = p.data;
    p.width = 0;
    p.height = 0;
    p.channels = 0;
    p.data = nullptr;
    return *this;
}

ktf::png & ktf::png::operator=(const ktf::png& p)
{
	this->width =p.width;
    this->height = p.height;
    this->channels = p.channels;

	size_t num_bytes = p.width * p.height * p.channels;
	if (data) {
		delete[] data;
	}
	this->data = new uint8_t[num_bytes];
	::memcpy(this->data, p.data, num_bytes);

	return *this;
}


bool ktf::png::valid() const
{
    return (data != nullptr);
}

uint32_t ktf::png::get_width() const
{
    return width;
}

uint32_t ktf::png::get_height() const
{
    return height;
}

uint8_t ktf::png::get_channels() const
{
    return channels;
}

uint8_t * ktf::png::get_data() const
{
    return data;
}

void ktf::png::load(const std::string& path)
{
    if (data) {
        this->width = 0;
        this->height = 0;
        this->channels = 0;
        delete[] data;
    }
    
    FILE *fp;
    png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    png_uint_32 img_w, img_h;
    int bit_depth, color_type, interlace_type;
    size_t row_bytes;
    png_bytepp row_pointers;
    
    fp = ::fopen(path.c_str(), "rb");
    if (!fp) {
        return;
    }
    
    png_ptr = ::png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        ::fclose(fp);
        return;
    }
    
    info_ptr = ::png_create_info_struct(png_ptr);
    if (!info_ptr) {
        goto cleanup_2;
    }
    
    if (::setjmp(png_jmpbuf(png_ptr))) {
        goto cleanup_1;
    }
    
    ::png_init_io(png_ptr, fp);
    ::png_set_sig_bytes(png_ptr, sig_read);
    ::png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);
    
    ::png_get_IHDR(png_ptr, info_ptr, &img_w, &img_h, &bit_depth, &color_type, &interlace_type, NULL, NULL);
    this->width = img_w;
    this->height = img_h;
    switch (color_type) {
        case PNG_COLOR_TYPE_GRAY:
            this->channels = 1;
            break;
        case PNG_COLOR_TYPE_GRAY_ALPHA:
            this->channels = 2;
            break;
        case PNG_COLOR_TYPE_RGB:
            this->channels = 3;
            break;
        case PNG_COLOR_TYPE_RGBA:
            this->channels = 4;
            break;
        default:
            this->channels = 0;
            break;
    }
    
    if (channels == 0) {
        goto cleanup_1;
    }
    
    row_bytes = ::png_get_rowbytes(png_ptr, info_ptr);
    row_pointers = ::png_get_rows(png_ptr, info_ptr);
    
    this->data = new uint8_t[row_bytes * height];
    for (uint32_t h = 0; h < height; h++) {
        ::memcpy(data + (row_bytes * h), row_pointers[h], row_bytes);
    }
    
cleanup_1:
    ::png_destroy_info_struct(png_ptr, &info_ptr);
cleanup_2:
    ::png_destroy_read_struct(&png_ptr, NULL, NULL);
    ::fclose(fp);
}

void ktf::png::write(const std::string& path)
{
    int color_type = -1;
    size_t row_bytes;
    png_bytepp row_pointers;
    FILE *fp;
    png_structp png_ptr;
    png_infop info_ptr;
    
    if (!data) {
        return;
    }
    
    fp = ::fopen(path.c_str(), "wb");
    if(!fp) {
        return;
    }

    png_ptr = ::png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        ::fclose(fp);
        return;
    }

    info_ptr = ::png_create_info_struct(png_ptr);
    if (!info_ptr) {
        goto cleanup_2;
    }

    if (::setjmp(png_jmpbuf(png_ptr)))  {
        goto cleanup_1;
    }

    ::png_init_io(png_ptr, fp);

    switch (channels) {
        case 1:
            color_type = PNG_COLOR_TYPE_GRAY;
            break;
        case 2:
            color_type = PNG_COLOR_TYPE_GRAY_ALPHA;
            break;
        case 3:
            color_type = PNG_COLOR_TYPE_RGB;
            break;
        case 4:
            color_type = PNG_COLOR_TYPE_RGBA;
            break;
        default:
            color_type = -1;
            break;
    }
    if (color_type == -1) {
        goto cleanup_1;
    }
    
    // Output is 8bit depth.
    ::png_set_IHDR(png_ptr, info_ptr, width, height, 8, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    ::png_write_info(png_ptr, info_ptr);
    
    row_bytes = sizeof(uint8_t) * width * channels;
    row_pointers = (png_bytepp) ::malloc(sizeof(png_bytep) * height);
    for (uint32_t h = 0; h < height; h++) {
        row_pointers[h] = (png_bytep)::malloc(row_bytes);
        ::memcpy(row_pointers[h], &data[row_bytes * h], row_bytes);
    }
    ::png_write_image(png_ptr, row_pointers);
    ::png_write_end(png_ptr, NULL);

    for(uint32_t h = 0; h < height; h++) {
        ::free(row_pointers[h]);
    }
    ::free(row_pointers);
    
cleanup_1:
    ::png_destroy_info_struct(png_ptr, &info_ptr);
cleanup_2:
    ::png_destroy_write_struct(&png_ptr, NULL);
    ::fclose(fp);
}
