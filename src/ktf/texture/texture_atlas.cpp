#include <string.h>
#include <cmath>
#include <fstream>
#include <queue>
#include <set>

#include "../util/string.hpp"
#include "texture_atlas.hpp"

namespace ktf {

const uint8_t error_texture[12] = {
	255,   0, 255,
	  0,   0,   0,
	  0,   0,   0,
	255,   0, 255
};


struct tex_aabb {
	std::string id;
	int width, height;
	ktf::vec2<double> p[2];
	ktf::vec2<double> pp[2];
	const ktf::png* img;
	int padding;

	tex_aabb(const std::string& id, const ktf::png* img, int padding);
	bool intersect(const tex_aabb& o) const;
	void set_pos(double x, double y);
};

tex_aabb::tex_aabb(const std::string& id, const ktf::png* img, int padding)
: id(id)
, width(img->get_width())
, height(img->get_height())
, img(img)
, padding(padding)
{
	this->set_pos(0, 0);
}

bool ktf::tex_aabb::intersect(const ktf::tex_aabb& o) const
{
	return ((pp[0].x < o.pp[1].x) &&
			(pp[1].x > o.pp[0].x) &&
			(pp[0].y < o.pp[1].y) &&
			(pp[1].y > o.pp[0].y));
}

void ktf::tex_aabb::set_pos(double x, double y)
{
	this->p[0].x = x + padding;
	this->p[0].y = y + padding;
	this->p[1].x = x + width + padding;
	this->p[1].y = y + height + padding;
	this->pp[0].x = x;
	this->pp[0].y = y;
	this->pp[1].x = 2*padding + x + width;
	this->pp[1].y = 2*padding + y + height;
}


struct size_comp {
	bool operator()(const ktf::tex_aabb& a1, const ktf::tex_aabb& a2) const;
};

bool ktf::size_comp::operator()(const ktf::tex_aabb& a1, const ktf::tex_aabb& a2) const
{
	if (a1.height < a2.height) {
		return true;
	} else if (a1.height > a2.height) {
		return false;
	} else if (a1.width < a2.width) {
		return true;
	} else {
		return false;
	}
}

} // namespace ktf


void ktf::texture_atlas::load(const std::string& file_path)
{
	this->entries.clear();

	std::ifstream file;
	try {
		file.open(file_path);
		if (!file.fail() && file.is_open()) {
			std::string line;
			std::vector<std::string> splits;
			std::vector<std::string> sp2;
			while (std::getline(file, line)) {
				ktf::split(splits, line, " ");
				ktf::entry entry;
				entry.uv[0].x = std::stod(splits[1]);
				entry.uv[0].y = std::stod(splits[2]);
				entry.uv[1].x = std::stod(splits[3]);
				entry.uv[1].y = std::stod(splits[4]);
				entry.dim[0] = std::stoi(splits[5]);
				entry.dim[1] = std::stoi(splits[6]);
				this->entries[splits[0]] = entry;
			}
			file.close();
		}
	} catch (std::fstream::failure& f) {
		file.close();
	}
}

bool ktf::texture_atlas::contains(const std::string& identifier) const
{
	return entries.find(identifier) != entries.end();
}

const ktf::entry* ktf::texture_atlas::get_entry(const std::string& key) const
{
	auto it = entries.find(key);
	if (it != entries.end()) {
		return &it->second;
	} else {
		return nullptr;
	}
}

void ktf::texture_atlas::generate(
	std::vector<ktf::atlas_pair> const& input_textures,
	std::string const& output_path,
	int target_channels,
	int padding,
	std::function<int(int)> const& size_func
)
{
	if (input_textures.empty() || target_channels < 1 || target_channels > 4) {
		return;
	}

	// load textures
	std::vector<std::pair<std::string, ktf::png>> input;
	for (const auto& p : input_textures) {
		if (p.id == "" || p.path == "") {
			continue;
		}

		ktf::png image;
		image.load(p.path);
		if (!image.valid()) {
			image = ktf::png(2, 2, 3, ktf::error_texture);
		}
		input.push_back(std::make_pair(p.id, std::move(image)));
	}

	ktf::texture_atlas::generate(input, output_path, target_channels, padding, size_func);
}

void ktf::texture_atlas::generate(
	std::vector<std::pair<std::string, ktf::png>> const& images,
	std::string const& output_path,
	int target_channels,
	int padding,
	std::function<int(int)> const& size_func
)
{
	// create aabbs
	std::priority_queue<tex_aabb, std::vector<tex_aabb>, size_comp> sorter;
	uint32_t max_width = 1, max_height = 1;
	int total_area = 0;
	int combx = 0;
	double combratio = 0;
	for (auto& i : images) {
		sorter.push(tex_aabb(i.first, &i.second, padding));
		uint32_t iw = i.second.get_width(), ih = i.second.get_height();
		max_width = std::max(max_width, iw);
		max_height = std::max(max_height, ih);
		total_area += iw * ih;
		combx += iw;
		combratio += iw / (double)ih;
	}
	max_width += 2 * padding;
	max_height += 2 * padding;

	double average_ratio = combratio / images.size();
	unsigned int sq = std::sqrt(images.size()) / average_ratio + 1;
	double average_width = combx / images.size();
	int tiles_x = total_area / max_height;
	int tiles_y = total_area / max_width;
	uint32_t limiter = (tiles_x < tiles_y) ? tiles_x : max_width;

	// place aabbs
	uint32_t x = 0;
	int width = 0, height = 0;
	std::vector<tex_aabb> placed;
	bool pixelmode = tiles_x < (sq * average_width);
	unsigned int i = 0;
	while (i < images.size()) {
		if ((pixelmode && x >= limiter) || (!pixelmode && i >= sq)) {
			break;
		}
		tex_aabb a = sorter.top();
		sorter.pop();
		a.set_pos(x, 0);
		x += a.width + 2 * padding;
		placed.push_back(a);
		i++;
	}
	width = std::max(x, max_width);
	height = max_height;
	if (size_func) {
		width = size_func(width);
		height = size_func(height);
	}

	// sort placed textures, lower y first
	std::set<int, std::less<int>> y_coords;
	y_coords.emplace(0);
	for (tex_aabb& i : placed) {
		y_coords.emplace(i.pp[1].y);
	}

	// sort placed textures, higher x first
	std::set<int, std::greater<int>> x_coords;
	x_coords.insert(width - padding);
	for (tex_aabb& i : placed) {
		x_coords.insert(i.pp[0].x);
		x_coords.insert(i.pp[1].x);
	}

	// add remaining textures
next:
	while (!sorter.empty()) {
		// get new aabb for placement
		tex_aabb a = sorter.top();
		sorter.pop();

		// apply current rectangle to coords
		std::vector<int> xx;
		for (int x0 : x_coords) {
			int x1 = x0 - (a.width + 2*padding);
			if (x1 >= 0) {
				xx.push_back(x1);
			}
		}

		// move in y direction
		for (int y : y_coords) {
			// move in x direction
			for (int x : xx) {
				a.set_pos(x, y);
				bool not_intersecting = true;
				for (const tex_aabb& o : placed) {
					if (o.intersect(a)) {
						not_intersecting = false;
						break;
					}
				}
				// add placed texture to queue if valid / no intersection
				if (not_intersecting) {
					placed.push_back(a);
					if (height < a.pp[1].y) {
						height = a.pp[1].y;
						if (size_func) {
							height = size_func(height);
						}
					}
					y_coords.emplace(a.pp[1].y);
					x_coords.insert(a.pp[0].x);
					x_coords.insert(a.pp[1].x);
					goto next;
				}
			}
		}
	}

	size_t num_bytes = width * height * target_channels;
	uint8_t* data = new uint8_t[num_bytes];
	::memset(data, '\0', sizeof(uint8_t)*num_bytes);

	std::unordered_map<std::string, entry> entries;
	// build main texture
	for (const tex_aabb& a : placed) {
		// copy data to texture
		int x_off = a.p[0][0];
		int y_off = a.p[0][1];
		const ktf::png * i = a.img;
		uint32_t iw = i->get_width();
		uint32_t ih = i->get_height();
		uint8_t ic = i->get_channels();
		uint8_t * id = const_cast<ktf::png*>(i)->get_data();
		for (uint32_t h=0; h<ih; h++) {
			for (uint32_t w=0; w<iw; w++) {
				// copy pixel data
				size_t index = ((h + y_off) * width + (w + x_off)) * target_channels;
				uint8_t* p = &data[index];
				::memcpy(p, &id[(h * iw + w) * ic], sizeof(uint8_t) * ic);

				// fill missing alpha channel
				if (target_channels == 4 && ic != 4) {
					p[3] = 255;
				}
			}
		}
		// save entry
		ktf::entry entry;
		entry.uv[0].x = a.p[0][0] / width;
		entry.uv[0].y = a.p[0][1] / height;
		entry.uv[1].x = a.p[1][0] / width;
		entry.uv[1].y = a.p[1][1] / height;
		entry.dim[0] = a.width;
		entry.dim[1] = a.height;
		entries.emplace(a.id, entry);
	}

	// save main texture
	ktf::png image(width, height, target_channels, data);
	delete[] data;
	image.write(output_path + ".png");

	// write entries to file
	std::ofstream file;
	file.open(output_path + ".tm");
	for (auto& pair : entries) {
		entry& e = pair.second;
		std::string line = pair.first
			+ " " + std::to_string(e.uv[0].x)
			+ " " + std::to_string(e.uv[0].y)
			+ " " + std::to_string(e.uv[1].x)
			+ " " + std::to_string(e.uv[1].y)
			+ " " + std::to_string(e.dim[0])
			+ " " + std::to_string(e.dim[1])
			+ "\n";
		file << line;
	}
	file.close();
}
