#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "../math/vec2.hpp"
#include "png.hpp"

namespace ktf {

struct entry {
	ktf::vec2<double> uv[2];
	ktf::vec2<int> dim;
};


struct atlas_pair {
	std::string id;
	std::string path;
};


class texture_atlas {
	private:
		std::unordered_map<std::string, entry> entries;


	public:
		void load(const std::string& file_path);
		bool contains(const std::string& identifier) const;
		const entry* get_entry(const std::string& key) const;
		static void generate(
			std::vector<atlas_pair> const& input_textures,
			std::string const& output_path,
			int target_channels,
			int padding,
			std::function<int(int)> const& size_func = {}
		);
		static void generate(
			std::vector<std::pair<std::string, ktf::png>> const& images,
			std::string const& output_path,
			int target_channels,
			int padding,
			std::function<int(int)> const& size_func = {}
		);
};

} // namespace ktf
