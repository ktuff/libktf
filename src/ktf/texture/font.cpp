#include <freetype2/ft2build.h>
#include <freetype2/freetype/freetype.h>
#include FT_FREETYPE_H

#include "font.hpp"
#include "texture_atlas.hpp"
#include "../util/string.hpp"

/*
 * NOTE
 * advance = 1/64 pixel => 2^6 = 64
 * generated indices and vertices => triangles
 */

char const * const ktf::font::filename = "glyph_map";


bool ktf::font::load(const std::string& path, uint32_t width, uint32_t height, const std::string& temp_folder, const std::vector<uint32_t>& chars)
{
	// remove already loaded chars
	this->free();

	// begin loading
	bool success = false;
	if (chars.empty()) {
		return success;
	}

	FT_Library library;
	FT_Face face;

	double reference_height;
	double normalization_factor;
	std::vector<ktf::atlas_pair> input_textures;
	ktf::texture_atlas atlas;
	std::vector<std::pair<std::string, ktf::png>> images;

	if (::FT_Init_FreeType(&library)) {
		return success;
	}
	if (::FT_New_Face(library, path.c_str(), 0, &face)) {
		goto release_library;
	}
	if (::FT_Set_Pixel_Sizes(face, width, height)) {
		goto release_face;
	}

	// get reference height from standard A, abort if not available
	if (::FT_Load_Char(face, 'A', FT_LOAD_RENDER)) {
		goto release_face;
	}
	reference_height = face->glyph->bitmap_top;
	normalization_factor = 1.0 / reference_height;

	// load whitespace, probably not in fonts, use placeholder size
	if (::FT_Load_Char(face, ' ', FT_LOAD_RENDER)) {
		goto release_face;
	} else {
		character space = {
			{(double)face->glyph->bitmap.width,
			 (double)face->glyph->bitmap.rows},
			{(double)face->glyph->bitmap_left,
			 (double)face->glyph->bitmap_top},
			{0.0f, 0.0f, 0.0f, 0.0f},
			(double)(face->glyph->advance.x >> 6)
		};
		this->characters.emplace(' ', space);
	}

	// generate from supported unicode characters vector
	for (uint32_t code : chars) {
		if (::FT_Load_Char(face, code, FT_LOAD_RENDER)) {
			continue;
		}

		// create general info
		character ch = {
			{(double)face->glyph->bitmap.width,
			 (double)face->glyph->bitmap.rows},
			{(double)face->glyph->bitmap_left,
			 (double)face->glyph->bitmap_top},
			{0.0f, 0.0f, 0.0f, 0.0f},
			(double)(face->glyph->advance.x >> 6)
		};
		if (ch.size.x == 0 || ch.size.y == 0 || ch.advance == 0) {
			continue;
		}
		this->characters.emplace(code, ch);

		// save texture
		const std::string id(std::to_string(code));
		const std::string path(temp_folder + id + ".png");
		ktf::png image(ch.size.x, ch.size.y, 1, face->glyph->bitmap.buffer);
		images.push_back(std::make_pair(id, std::move(image)));

		input_textures.push_back({id, path});
	}

	if (input_textures.empty()) {
		goto release_face;
	}

	// generate texture containing all glyphs
	ktf::texture_atlas::generate(images, temp_folder + filename, 1, 4);
	atlas.load(temp_folder + filename + ".tm");

	// get texture coordinates
	for (auto& it : input_textures) {
		if (atlas.contains(it.id)) {
			auto const* entry = atlas.get_entry(it.id);
			uint32_t index = std::stoul(it.id);
			if (characters.find(index) == characters.end()) {
				continue;
			}
			auto& c = characters.at(index);

			// save texture coordinates, reordering needed
			c.tex = ktf::vec4<float>(
				entry->uv[0].x,
				entry->uv[1].y,
				entry->uv[1].x,
				entry->uv[0].y
			);
		}
	}

	// normalise / downscale
	for (auto& it : characters) {
		it.second.size *= normalization_factor;
		it.second.bearing *= normalization_factor;
		it.second.advance *= normalization_factor;
	}

	success = true;

release_face:
	::FT_Done_Face(face);
release_library:
	::FT_Done_FreeType(library);

	return success;
}

void ktf::font::free()
{
	this->characters.clear();
}

size_t ktf::font::generate(const std::string& string, ktf::vector<ktf::vec4<float>>& vertices, ktf::vector<uint32_t>& indices, ktf::vec2<float>& dimensions) const
{
	vertices.clear();
	indices.clear();

	std::vector<uint32_t> unicode_chars = ktf::to_utf32(string);

	double total_width = 0.0;
	double total_height = 0.0;
	double const line_height = 1.5;
	int num_lines = 1;
	double x = 0.0;
	double y = 0.0;
	for (uint32_t uc : unicode_chars) {
		if (uc == '\n') {
			total_width = std::max(total_width, x);
			num_lines++;
			y += line_height;
			x = 0.0;
			continue;
		}
		auto it = characters.find(uc);
		if (it == characters.end()) {
			continue;
		}
		auto& c = it->second;

		float px0 = x + c.bearing.x;
		float px1 = x + c.bearing.x + c.size.x;
		float py0 = y - c.bearing.y + c.size.y;
		float py1 = y - c.bearing.y;

		vertices.push({px0, py0, c.tex.x, c.tex.y});
		vertices.push({px1, py0, c.tex.z, c.tex.y});
		vertices.push({px0, py1, c.tex.x, c.tex.w});
		vertices.push({px1, py1, c.tex.z, c.tex.w});

		x += c.advance;
	}
	total_width = std::max(total_width, x);
	total_height = y + line_height;

	for (uint32_t i=0; i<vertices.size() / 4u; ++i) {
		indices.push(i*4 + 0);
		indices.push(i*4 + 1);
		indices.push(i*4 + 2);
		indices.push(i*4 + 1);
		indices.push(i*4 + 3);
		indices.push(i*4 + 2);
	}

	dimensions = ktf::vec2<float>(total_width, total_height);

	return num_lines;
}
