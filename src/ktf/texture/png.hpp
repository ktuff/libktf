#pragma once

#include <stdint.h>
#include <string>

namespace ktf {
    
class png {
    
    private:
        uint32_t width, height;
        uint8_t channels;
        uint8_t* data;
        
        
    public:
        png();
        png(uint32_t width, uint32_t height, int channels, uint8_t const * data);
        png(png&& p);
        png(const png& p);
        ~png();
        
        png& operator=(png&& p);
        png& operator=(const png& p);
        
        
    public:
        bool valid() const;
        uint32_t get_width() const;
        uint32_t get_height() const;
        uint8_t get_channels() const;
        uint8_t * get_data() const;
        void load(const std::string& path);
        void write(const std::string& path);
};

} // namespace ktf
