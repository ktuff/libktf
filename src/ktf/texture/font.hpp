#pragma once

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "../container/vector.hpp"
#include "../math/vec2.hpp"
#include "../math/vec4.hpp"

namespace ktf {

struct character {
	ktf::vec2<double> size;
	ktf::vec2<double> bearing;
	ktf::vec4<float> tex;
	double advance;
};

class font {

	public:
		static char const * const filename;

	private:
		std::unordered_map<uint32_t, character> characters;


	public:
		bool load(const std::string& path, uint32_t width, uint32_t height, const std::string& temp_folder, const std::vector<uint32_t>& chars);
		void free();
		size_t generate(const std::string& string, ktf::vector<ktf::vec4<float>>& vertices, ktf::vector<uint32_t>& indices, ktf::vec2<float>& dimensions) const;
};

} // namespace ktf


